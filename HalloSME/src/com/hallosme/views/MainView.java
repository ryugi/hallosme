package com.hallosme.views;

import javax.servlet.annotation.WebServlet;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.vaadin.teemu.VaadinIcons;

import com.hallosme.components.CreateKontextWindow;
import com.hallosme.pojos.UserAccount;
import com.hallosme.utils.KontextUIText;
import com.hallosme.utils.SQLUtil;
import com.hallosme.views.login.LoginWindow;
import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Accordion;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
@Theme("hallosme")
@PreserveOnRefresh
public class MainView extends UI {
	private VerticalLayout mainLayout;
	private HorizontalLayout splitLayout;
	private VerticalLayout contentLayout;
	private VerticalLayout navigationLayout;
	private Navigator navigator;
	private UserAccount account;
	private Accordion accordion;
	private SessionFactory factory;
	private Button loginButton;
	private Button createKontextButton;
	private Button saveButton;

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = MainView.class)
	public static class Servlet extends VaadinServlet {
	}

	@Override
	protected void init(VaadinRequest request) {
		factory = new Configuration().configure().buildSessionFactory();
		// laden der Oberflächentexte des Nutzungskontexts
		KontextUIText.loadMLTexts();
		mainLayout = new VerticalLayout();
		splitLayout = new HorizontalLayout();
		contentLayout = new VerticalLayout();
		navigationLayout = new VerticalLayout();
		accordion = new Accordion();
		navigator = new Navigator(this, contentLayout);
		navigator.addView("", new StartPage());
		loginButton = new Button("Login");
		loginButton.setIcon(VaadinIcons.SIGN_IN);
		createKontextButton = new Button("Kontext hinzufügen");
		createKontextButton.setIcon(VaadinIcons.PLUS_CIRCLE_O);
		createKontextButton.addStyleName("full-length");
		saveButton = new Button("Speichern");
		saveButton.setIcon(VaadinIcons.CLOUD_UPLOAD_O);

		// erstelle neues LoginWindow Event
		loginButton.addClickListener(new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				LoginWindow window = new LoginWindow();
				// window.setPosition(event.getClientX(), event.getClientY());
				getCurrent().addWindow(window);
			}
		});

		// // erstelle neuen Nutzungskontext Event
		createKontextButton.addClickListener(new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				getCurrent().addWindow(new CreateKontextWindow());

			}
		});

		// speichere Nutzungskontext Event
		saveButton.addClickListener(new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				account = ((UserAccount) getSession().getAttribute("user"));
				SQLUtil.save(factory, account);
			}
		});

		accordion.setVisible(false);
		saveButton.setVisible(false);
		createKontextButton.setVisible(false);

		navigationLayout.addComponent(loginButton);
		navigationLayout.addComponent(accordion);
		// navigationLayout.addComponent(kontextName);
		navigationLayout.addComponent(createKontextButton);
		navigationLayout.addComponent(saveButton);

		navigationLayout.addStyleName("navigation");
		contentLayout.addStyleName("content");
		mainLayout.addStyleName("mainview");

		splitLayout.addComponent(contentLayout);
		splitLayout.addComponent(navigationLayout);
		mainLayout.addComponent(splitLayout);
		setContent(mainLayout);
	}

	public UserAccount getAccount() {
		return account;
	}

	public void setAccount(UserAccount account) {
		this.account = account;
	}

	public Accordion getAccordion() {
		return accordion;
	}

	public void setAccordion(Accordion accordion) {
		this.accordion = accordion;
	}

	public SessionFactory getFactory() {
		return factory;
	}

	public void setFactory(SessionFactory factory) {
		this.factory = factory;
	}

	public HorizontalLayout getHorizontalLayout() {
		return splitLayout;
	}

	public void setHorizontalLayout(HorizontalLayout horizontalLayout) {
		this.splitLayout = horizontalLayout;
	}

	public Button getLoginButton() {
		return loginButton;
	}

	public void setLoginButton(Button loginButton) {
		this.loginButton = loginButton;
	}

	public Button getCreateKontextButton() {
		return createKontextButton;
	}

	public void setCreateKontextButton(Button createKontextButton) {
		this.createKontextButton = createKontextButton;
	}

	public Button getSaveButton() {
		return saveButton;
	}

	public void setSaveButton(Button saveButton) {
		this.saveButton = saveButton;
	}

	public VerticalLayout getContentLayout() {
		return contentLayout;
	}

	public void setContentLayout(VerticalLayout contentLayout) {
		this.contentLayout = contentLayout;
	}

}