package com.hallosme.views.navigation;

import java.io.File;

import com.hallosme.components.deleteKontextButton;
import com.hallosme.pojos.Nutzungskontext;
import com.hallosme.pojos.UserAccount;
import com.hallosme.utils.pdf.GeneratePDFButton;
import com.hallosme.views.MainView;
import com.hallosme.views.nutzungskontext.OrgEnviromentView;
import com.hallosme.views.nutzungskontext.PhysEnviromentView;
import com.hallosme.views.nutzungskontext.TechEnviromentView;
import com.hallosme.views.nutzungskontext.UserTypeView;
import com.hallosme.views.nutzungskontext.WorkTaskView;
import com.hallosme.views.nutzungskontext.WorkToolView;
import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.FileResource;
import com.vaadin.server.Resource;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

public class SideNavigationService {

	/**
	 * Erstellt ein neues VerticalLayout in das neue Views eines Nutzungskontext hinzugef�gt werden.
	 * Dieses VerticalLayout wird dann an das mitgegebene Accordion, als neuer Tab angeh�ngt.
	 * 
	 * @param session
	 *            aktuelle VaadinSession
	 * @param name
	 *            Name des zu erstellenden Nutzungskontexts
	 */
	public static void createNavigationTab(VaadinSession session, String name) {
		Navigator navigator = ((MainView) UI.getCurrent()).getNavigator();
		VerticalLayout accordionTab = new VerticalLayout();
		OrgEnviromentView orgEnviromentView = new OrgEnviromentView();
		TechEnviromentView techEnviromentView = new TechEnviromentView();
		PhysEnviromentView physEnviromentView = new PhysEnviromentView();
		UserTypeView userTypeView = new UserTypeView();
		WorkTaskView workTaskView = new WorkTaskView();
		WorkToolView workToolView = new WorkToolView();

		accordionTab.addStyleName("accordionTab");

		// usertypeBar
		HorizontalLayout userBarLayout = new HorizontalLayout();
		userBarLayout.addStyleName("navigationPoint");
		final ProgressBar userBar = userTypeView.getBar();
		userBar.setCaption("Benutzer Typ");
		userBar.setWidth("298");
		userBarLayout.addComponent(userBar);
		userTypeView.getEditedComponentsLabel().setValue(userTypeView.getEditedComponents() + " / " + userTypeView.getComponents());
		userBarLayout.addComponent(userTypeView.getEditedComponentsLabel());
		// clicklistener auf das layout
		userBarLayout.addLayoutClickListener(new LayoutClickListener() {
			private static final long serialVersionUID = -1822685632006055695L;

			@Override
			public void layoutClick(LayoutClickEvent event) {
				navigator.navigateTo("userType" + name.replace(" ", ""));
			}
		});
		accordionTab.addComponent(userBarLayout);

		// worktaskBar
		HorizontalLayout taskBarLayout = new HorizontalLayout();
		taskBarLayout.addStyleName("navigationPoint");
		final ProgressBar taskBar = workTaskView.getBar();
		taskBar.setCaption("Arbeitsaufgabe");
		taskBar.setWidth("298");
		taskBarLayout.addComponent(taskBar);
		workTaskView.getEditedComponentsLabel().setValue(workTaskView.getEditedComponents() + " / " + workTaskView.getComponents());
		taskBarLayout.addComponent(workTaskView.getEditedComponentsLabel());
		// clicklistener auf das layout
		taskBarLayout.addLayoutClickListener(new LayoutClickListener() {
			private static final long serialVersionUID = -4393097727718305495L;

			@Override
			public void layoutClick(LayoutClickEvent event) {
				navigator.navigateTo("workTask" + name.replace(" ", ""));
			}
		});
		accordionTab.addComponent(taskBarLayout);

		// worktoolBar
		HorizontalLayout toolBarLayout = new HorizontalLayout();
		toolBarLayout.addStyleName("navigationPoint");
		final ProgressBar toolBar = workToolView.getBar();
		toolBar.setCaption("Arbeitsmittel");
		toolBar.setWidth("298");
		toolBarLayout.addComponent(toolBar);
		workToolView.getEditedComponentsLabel().setValue(workToolView.getEditedComponents() + " / " + workToolView.getComponents());
		toolBarLayout.addComponent(workToolView.getEditedComponentsLabel());
		// clicklistener auf das layout
		toolBarLayout.addLayoutClickListener(new LayoutClickListener() {
			private static final long serialVersionUID = 5575291592424591580L;

			@Override
			public void layoutClick(LayoutClickEvent event) {
				navigator.navigateTo("workTool" + name.replace(" ", ""));
			}
		});
		accordionTab.addComponent(toolBarLayout);

		// orgEnviBar
		HorizontalLayout orgEnviBarLayout = new HorizontalLayout();
		orgEnviBarLayout.addStyleName("navigationPoint");
		final ProgressBar orgEnviBar = orgEnviromentView.getBar();
		orgEnviBar.setCaption("Organisatorische Umgebung");
		orgEnviBar.setWidth("298");
		orgEnviBarLayout.addComponent(orgEnviBar);
		orgEnviromentView.getEditedComponentsLabel().setValue(orgEnviromentView.getEditedComponents() + " / " + orgEnviromentView.getComponents());
		orgEnviBarLayout.addComponent(orgEnviromentView.getEditedComponentsLabel());
		// clicklistener auf das layout
		orgEnviBarLayout.addLayoutClickListener(new LayoutClickListener() {
			private static final long serialVersionUID = -2667100714141794351L;

			@Override
			public void layoutClick(LayoutClickEvent event) {
				navigator.navigateTo("orgEnvi" + name.replace(" ", ""));
			}
		});
		accordionTab.addComponent(orgEnviBarLayout);

		// techEnviBar
		HorizontalLayout techEnviLayout = new HorizontalLayout();
		techEnviLayout.addStyleName("navigationPoint");
		final ProgressBar techEnviBar = techEnviromentView.getBar();
		techEnviBar.setCaption("Technische Umgebung");
		techEnviBar.setWidth("298");
		techEnviLayout.addComponent(techEnviBar);
		techEnviromentView.getEditedComponentsLabel().setValue(techEnviromentView.getEditedComponents() + " / " + techEnviromentView.getComponents());
		techEnviLayout.addComponent(techEnviromentView.getEditedComponentsLabel());
		// clicklistener auf das layout
		techEnviLayout.addLayoutClickListener(new LayoutClickListener() {
			private static final long serialVersionUID = 3868714600621368915L;

			@Override
			public void layoutClick(LayoutClickEvent event) {
				navigator.navigateTo("techEnvi" + name.replace(" ", ""));
			}
		});
		accordionTab.addComponent(techEnviLayout);

		// physEnviBar
		HorizontalLayout physEnviBarLayout = new HorizontalLayout();
		physEnviBarLayout.addStyleName("navigationPoint");
		final ProgressBar physEnviBar = physEnviromentView.getBar();
		physEnviBar.setCaption("Physische Umgebung");
		physEnviBar.setWidth("298");
		physEnviBarLayout.addComponent(physEnviBar);
		physEnviromentView.getEditedComponentsLabel().setValue(physEnviromentView.getEditedComponents() + " / " + physEnviromentView.getComponents());
		physEnviBarLayout.addComponent(physEnviromentView.getEditedComponentsLabel());
		// clicklistener auf das layout
		physEnviBarLayout.addLayoutClickListener(new LayoutClickListener() {
			private static final long serialVersionUID = -3424745415468643571L;

			@Override
			public void layoutClick(LayoutClickEvent event) {
				navigator.navigateTo("physEnvi" + name.replace(" ", ""));
			}
		});
		accordionTab.addComponent(physEnviBarLayout);
		accordionTab.addComponent(new deleteKontextButton(accordionTab, name));

		navigator.addView("userType" + name.replace(" ", ""), userTypeView);
		navigator.addView("workTask" + name.replace(" ", ""), workTaskView);
		navigator.addView("workTool" + name.replace(" ", ""), workToolView);
		navigator.addView("orgEnvi" + name.replace(" ", ""), orgEnviromentView);
		navigator.addView("physEnvi" + name.replace(" ", ""), physEnviromentView);
		navigator.addView("techEnvi" + name.replace(" ", ""), techEnviromentView);

		// componets.add(orgEnviromentView);
		// componets.add(techEnviromentView);
		// componets.add(physEnviromentView);
		// componets.add(userTypeView);
		// componets.add(workTaskView);
		// componets.add(workToolView);

		// erstelle aus den einzelnen Views einen neunen Nutzungskontext und f�ge diesen dem
		// UserAccount Objekt hinzu, falls dieser existent ist
		if (((UserAccount) session.getAttribute("user")) != null) {
			Nutzungskontext nutzungskontext = new Nutzungskontext();
			nutzungskontext.setUserType(userTypeView.getUserTypeBean());
			nutzungskontext.setWorkTask(workTaskView.getWorkTaskBean());
			nutzungskontext.setWorkTool(workToolView.getWorkToolBean());
			nutzungskontext.setOrgEnviroment(orgEnviromentView.getOrgEnviromentBean());
			nutzungskontext.setPhysEnviroment(physEnviromentView.getPhysEnviromentBean());
			nutzungskontext.setTechEnviroment(techEnviromentView.getTechEnviromentBean());
			nutzungskontext.setName(name);

			/*
			 * Hier wird die PDF Generiert. Dazu werden 2 Buttons erstellt "generatePDFButton" ist
			 * in der Navigation sichtbar, downloadInvisibleButton wird mittles css siehe
			 * "Stylename" ausgebledet. Zudem wird nach dem physischen klick des Nutzers auf den
			 * "generatePDFButton" am Ende mittels Javascript ein neuer ButtonBlick auf den
			 * "downloadInvisibleButton" welcher die generierte PDF zum Download anbietet.
			 */
			String basepath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();
			Button generatePDFButton = new GeneratePDFButton().generateDownloadPDFButton(nutzungskontext);
			final Button downloadInvisibleButton = new Button();
			downloadInvisibleButton.setId("DownloadButtonId");
			downloadInvisibleButton.addStyleName("InvisibleButton");
			Resource res = new FileResource(new File(basepath + "/" + nutzungskontext.getName().replace(" ", "_") + ".pdf"));
			FileDownloader fd = new FileDownloader(res);
			fd.extend(downloadInvisibleButton);
			accordionTab.addComponent(generatePDFButton);
			accordionTab.addComponent(downloadInvisibleButton);

			((UserAccount) session.getAttribute("user")).getNutzungkontext().add(nutzungskontext);
		}

		// userTypeView.setId("usertypeID");
		// kontextMap.put("Kontext" + name, componets);
		((MainView) UI.getCurrent()).getAccordion().addTab(accordionTab, name);

	}
}
