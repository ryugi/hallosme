package com.hallosme.views.nutzungskontext;

import com.hallosme.components.listener.BarChangeListener;
import com.hallosme.components.listener.VisibilityChangeListener;
import com.hallosme.pojos.PhysEnviroment;
import com.hallosme.utils.KontextUIText;
import com.hallosme.utils.CreateComponent;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class PhysEnviromentView extends BasicKontextView implements View {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6946514544955002057L;
	final int components = 21;
	// Fragen Bl�cke
	Panel panelAtmosCondition = new Panel();
	Panel panelAcousticCondition = new Panel();
	Panel panelThermalCondition = new Panel();
	Panel panelLightCondition = new Panel();
	Panel panelEnviStability = new Panel();
	Panel panelSetup = new Panel();
	Panel panelPosture = new Panel();
	Panel panelWorkspace = new Panel();
	Panel panelHealtHezard = new Panel();
	Panel panelSafty = new Panel();

	// Layouts f�r die Fragenbl�cke
	VerticalLayout layoutAtmosCondition = new VerticalLayout();
	VerticalLayout layoutAcousticCondition = new VerticalLayout();
	VerticalLayout layoutThermalCondition = new VerticalLayout();
	VerticalLayout layoutLightCondition = new VerticalLayout();
	VerticalLayout layoutEnviStability = new VerticalLayout();
	VerticalLayout layoutSetup = new VerticalLayout();
	VerticalLayout layoutPosture = new VerticalLayout();
	VerticalLayout layoutWorkspace = new VerticalLayout();
	VerticalLayout layoutHealtHezard = new VerticalLayout();
	VerticalLayout layoutSafty = new VerticalLayout();

	// Verwendete Felder
	TextField atmosConditionAir = new TextField();
	TextField atmosConditionSpeed = new TextField();
	TextField atmosConditionHumidity = new TextField();
	ComboBox acousticCondition = new ComboBox();
	TextField thermalConditionLow = new TextField();
	TextField thermalConditionHigh = new TextField();
	TextField lightConditionLow = new TextField();
	TextField lightConditionHigh = new TextField();
	ComboBox enviStability = new ComboBox();
	TextField workspaceSize = new TextField();
	TextArea workspaceInv = new TextArea();
	TextField postureStaying = new TextField();
	TextField postureSitting = new TextField();
	TextField postureLying = new TextField();
	TextField postureKneeling = new TextField();
	TextField workspaceMoveSpace = new TextField();
	TextField workspaceAtHand = new TextField();
	ComboBox healtHezard = new ComboBox();
	ComboBox healtHezardCategory = new ComboBox();
	ComboBox safty = new ComboBox();
	ComboBox saftyLimitation = new ComboBox();

	// mapping von Feldern auf Daten in Java (Pojos)
	private PhysEnviroment physEnviromentBean = new PhysEnviroment();
	private FieldGroup physEnviromentBinder = new FieldGroup();
	BeanItem<PhysEnviroment> physEnviromentBeanItem = new BeanItem<PhysEnviroment>(physEnviromentBean);

	// SessionFactory factory = new
	// Configuration().configure().buildSessionFactory();

	@SuppressWarnings("serial")
	public PhysEnviromentView() {
		getHeadline().setValue(KontextUIText.getLabel("physCaption", "items1"));

		// ##atmosCondition
		panelAtmosCondition = new Panel(KontextUIText.getLabel("atmosCondition", "question"));
		// panel.setSizeUndefined();
		atmosConditionAir = CreateComponent.createTextField("atmosConditionAir", "items1");
		atmosConditionSpeed = CreateComponent.createTextField("atmosConditionSpeed", "items1");
		atmosConditionHumidity = CreateComponent.createTextField("atmosConditionHumidity", "items1");
		atmosConditionAir.setInputPrompt(KontextUIText.getLabel("atmosConditionAir", "items1"));
		atmosConditionSpeed.setInputPrompt(KontextUIText.getLabel("atmosConditionSpeed", "items1"));
		atmosConditionHumidity.setInputPrompt(KontextUIText.getLabel("atmosConditionHumidity", "items1"));
		layoutAtmosCondition.addComponent(atmosConditionAir);
		layoutAtmosCondition.addComponent(atmosConditionSpeed);
		layoutAtmosCondition.addComponent(atmosConditionHumidity);
		// layout.setSizeUndefined();
		layoutAtmosCondition.setMargin(true);
		panelAtmosCondition.setContent(layoutAtmosCondition);
		addComponent(panelAtmosCondition);

		// ##acousticCondition
		panelAcousticCondition = new Panel(KontextUIText.getLabel("acousticCondition", "question"));
		// panel.setSizeUndefined();
		acousticCondition = CreateComponent.createComboBox("acousticCondition", "items2", KontextUIText.getLabel("acousticCondition", "items1"));
		acousticCondition.setInputPrompt(KontextUIText.getLabel("acousticCondition", "items1"));
		layoutAcousticCondition.addComponent(acousticCondition);
		// layout.setSizeUndefined();
		layoutAcousticCondition.setMargin(true);
		panelAcousticCondition.setContent(layoutAcousticCondition);
		addComponent(panelAcousticCondition);

		// ##thermalCondition
		panelThermalCondition = new Panel(KontextUIText.getLabel("thermalCondition", "question"));
		// panel.setSizeUndefined();
		thermalConditionLow = CreateComponent.createTextField("thermalCondition", "items1");
		thermalConditionHigh = CreateComponent.createTextField("thermalCondition", "items2");
		thermalConditionLow.setInputPrompt(KontextUIText.getLabel("thermalCondition", "items1"));
		thermalConditionHigh.setInputPrompt(KontextUIText.getLabel("thermalCondition", "items2"));
		layoutThermalCondition.addComponent(thermalConditionLow);
		layoutThermalCondition.addComponent(thermalConditionHigh);
		// layout.setSizeUndefined();
		layoutThermalCondition.setMargin(true);
		panelThermalCondition.setContent(layoutThermalCondition);
		addComponent(panelThermalCondition);

		// ##lightCondition
		panelLightCondition = new Panel(KontextUIText.getLabel("lightCondition", "question"));
		// panel.setSizeUndefined();
		lightConditionLow = CreateComponent.createTextField("lightCondition", "items1");
		lightConditionHigh = CreateComponent.createTextField("lightCondition", "items2");
		lightConditionLow.setInputPrompt(KontextUIText.getLabel("lightCondition", "items1"));
		lightConditionHigh.setInputPrompt(KontextUIText.getLabel("lightCondition", "items2"));
		layoutLightCondition.addComponent(lightConditionLow);
		layoutLightCondition.addComponent(lightConditionHigh);
		// layout.setSizeUndefined();
		layoutLightCondition.setMargin(true);
		panelLightCondition.setContent(layoutLightCondition);
		addComponent(panelLightCondition);

		// ##enviStability
		panelEnviStability = new Panel(KontextUIText.getLabel("enviStability", "question"));
		// panel.setSizeUndefined();
		enviStability = CreateComponent.createComboBox("enviStability", "items1", null);
		enviStability.setInputPrompt(KontextUIText.getLabel("enviStability", "inputPrompt"));
		layoutEnviStability.addComponent(enviStability);
		// layout.setSizeUndefined();
		layoutEnviStability.setMargin(true);
		panelEnviStability.setContent(layoutEnviStability);
		addComponent(panelEnviStability);

		// ##workspaceSizeSpace
		panelSetup = new Panel(KontextUIText.getLabel("workspaceSizeSpace", "question"));
		// panel.setSizeUndefined();
		workspaceSize = CreateComponent.createTextField("workspaceSizeSpace", "items1");
		workspaceInv = CreateComponent.createTextArea("workspaceSizeSpace", "items2");
		workspaceSize.setInputPrompt(KontextUIText.getLabel("workspaceSizeSpace", "items1"));
		workspaceInv.setInputPrompt(KontextUIText.getLabel("workspaceSizeSpace", "items2"));
		layoutSetup.addComponent(workspaceSize);
		layoutSetup.addComponent(workspaceInv);
		// layout.setSizeUndefined();
		layoutSetup.setMargin(true);
		panelSetup.setContent(layoutSetup);
		addComponent(panelSetup);

		// ##posture
		panelPosture = new Panel(KontextUIText.getLabel("posture", "question"));
		// panel.setSizeUndefined();
		postureStaying = CreateComponent.createTextField("posture", "items1");
		postureSitting = CreateComponent.createTextField("posture", "items2");
		postureLying = CreateComponent.createTextField("posture", "items3");
		postureKneeling = CreateComponent.createTextField("posture", "items4");
		postureStaying.setInputPrompt(KontextUIText.getLabel("posture", "items1"));
		postureSitting.setInputPrompt(KontextUIText.getLabel("posture", "items2"));
		postureLying.setInputPrompt(KontextUIText.getLabel("posture", "items3"));
		postureKneeling.setInputPrompt(KontextUIText.getLabel("posture", "items4"));
		layoutPosture.addComponent(postureStaying);
		layoutPosture.addComponent(postureSitting);
		layoutPosture.addComponent(postureLying);
		layoutPosture.addComponent(postureKneeling);
		// layout.setSizeUndefined();
		layoutPosture.setMargin(true);
		panelPosture.setContent(layoutPosture);
		addComponent(panelPosture);

		// ##workspace
		panelWorkspace = new Panel(KontextUIText.getLabel("workspaceMove", "question"));
		// panel.setSizeUndefined();
		workspaceMoveSpace = CreateComponent.createTextField("workspaceMove", "items1");
		workspaceAtHand = CreateComponent.createTextField("workspaceMove", "items2");
		workspaceMoveSpace.setInputPrompt(KontextUIText.getLabel("workspaceMove", "items1"));
		workspaceAtHand.setInputPrompt(KontextUIText.getLabel("workspaceMove", "items2"));
		// layout.setSizeUndefined();
		layoutWorkspace.setMargin(true);
		layoutWorkspace.addComponent(workspaceMoveSpace);
		layoutWorkspace.addComponent(workspaceAtHand);
		panelWorkspace.setContent(layoutWorkspace);
		addComponent(panelWorkspace);

		// ##healtHezard
		panelHealtHezard = new Panel(KontextUIText.getLabel("healtHezard", "question"));
		// panel.setSizeUndefined();
		healtHezard = CreateComponent.createComboBoxYN(KontextUIText.getLabel("healtHezard", "items1"));
		healtHezard.addValueChangeListener(new ValueChangeListener() {

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (healtHezard.getValue() != null && healtHezard.getValue().equals("Ja")) {
					healtHezardCategory.removeStyleName("hidden");
				} else {
					healtHezardCategory.addStyleName("hidden");
				}

			}
		});

		healtHezardCategory = CreateComponent.createComboBox("healtHezard", "items3", KontextUIText.getLabel("healtHezard", "items2"));
		healtHezardCategory.setInputPrompt(KontextUIText.getLabel("healtHezard", "items2"));
		healtHezardCategory.addStyleName("hidden");
		// layout.setSizeUndefined();
		layoutHealtHezard.setMargin(true);
		layoutHealtHezard.addComponent(healtHezard);
		layoutHealtHezard.addComponent(healtHezardCategory);
		panelHealtHezard.setContent(layoutHealtHezard);
		addComponent(panelHealtHezard);

		// ##safty
		panelSafty = new Panel(KontextUIText.getLabel("safty", "question"));
		// panel.setSizeUndefined();
		safty = CreateComponent.createComboBoxYN(KontextUIText.getLabel("safty", "items1"));
		saftyLimitation = CreateComponent.createComboBox("safty", "items2", KontextUIText.getLabel("safty", "inputPrompt"));
		saftyLimitation.setInputPrompt(KontextUIText.getLabel("safty", "inputPrompt"));
		saftyLimitation.addStyleName("hidden");
		safty.addValueChangeListener(new VisibilityChangeListener(safty, saftyLimitation, null, null));
		layoutSafty.addComponent(safty);
		layoutSafty.addComponent(saftyLimitation);
		// layout.setSizeUndefined();
		layoutSafty.setMargin(true);
		panelSafty.setContent(layoutSafty);
		addComponent(panelSafty);

		physEnviromentBinder.setBuffered(false);
		physEnviromentBinder.setItemDataSource(physEnviromentBeanItem);
		physEnviromentBinder.bindMemberFields(this);

		atmosConditionAir.addValueChangeListener(new BarChangeListener(atmosConditionAir, components, this));
		atmosConditionSpeed.addValueChangeListener(new BarChangeListener(atmosConditionSpeed, components, this));
		atmosConditionHumidity.addValueChangeListener(new BarChangeListener(atmosConditionHumidity, components, this));
		acousticCondition.addValueChangeListener(new BarChangeListener(acousticCondition, components, this));
		thermalConditionLow.addValueChangeListener(new BarChangeListener(thermalConditionLow, components, this));
		thermalConditionHigh.addValueChangeListener(new BarChangeListener(thermalConditionHigh, components, this));
		lightConditionLow.addValueChangeListener(new BarChangeListener(lightConditionLow, components, this));
		lightConditionHigh.addValueChangeListener(new BarChangeListener(lightConditionHigh, components, this));
		enviStability.addValueChangeListener(new BarChangeListener(enviStability, components, this));
		workspaceSize.addValueChangeListener(new BarChangeListener(workspaceSize, components, this));
		workspaceInv.addValueChangeListener(new BarChangeListener(workspaceInv, components, this));
		postureStaying.addValueChangeListener(new BarChangeListener(postureStaying, components, this));
		postureSitting.addValueChangeListener(new BarChangeListener(postureSitting, components, this));
		postureLying.addValueChangeListener(new BarChangeListener(postureLying, components, this));
		postureKneeling.addValueChangeListener(new BarChangeListener(postureKneeling, components, this));
		workspaceMoveSpace.addValueChangeListener(new BarChangeListener(workspaceMoveSpace, components, this));
		workspaceAtHand.addValueChangeListener(new BarChangeListener(workspaceAtHand, components, this));
		healtHezard.addValueChangeListener(new BarChangeListener(healtHezard, components, this));
		healtHezardCategory.addValueChangeListener(new BarChangeListener(healtHezardCategory, components, this));
		safty.addValueChangeListener(new BarChangeListener(safty, components, this));
		saftyLimitation.addValueChangeListener(new BarChangeListener(saftyLimitation, components, this));

		panelAtmosCondition.addStyleName("panel");
		panelAcousticCondition.addStyleName("panel");
		panelThermalCondition.addStyleName("panel");
		panelLightCondition.addStyleName("panel");
		panelEnviStability.addStyleName("panel");
		panelSetup.addStyleName("panel");
		panelPosture.addStyleName("panel");
		panelWorkspace.addStyleName("panel");
		panelHealtHezard.addStyleName("panel");
		panelSafty.addStyleName("panel");

	}

	public int getComponents() {
		return components;
	}

	public PhysEnviroment getPhysEnviromentBean() {
		return physEnviromentBean;
	}

	public void setPhysEnviromentBean(PhysEnviroment physEnviromentBean) {
		this.physEnviromentBean = physEnviromentBean;
	}

	public FieldGroup getPhysEnviromentBinder() {
		return physEnviromentBinder;
	}

	public void setPhysEnviromentBinder(FieldGroup physEnviromentBinder) {
		this.physEnviromentBinder = physEnviromentBinder;
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		System.out.println("Viewchange PhysEnviroment");
	}

}
