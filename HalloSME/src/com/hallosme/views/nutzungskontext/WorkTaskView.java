package com.hallosme.views.nutzungskontext;

import com.hallosme.components.listener.BarChangeListener;
import com.hallosme.components.listener.VisibilityChangeListener;
import com.hallosme.pojos.WorkTask;
import com.hallosme.utils.KontextUIText;
import com.hallosme.utils.CreateComponent;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class WorkTaskView extends BasicKontextView implements View {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5040336547602630042L;
	final int components = 18;
	// Fragen Bl�cke
	Panel panelTaskCategory = new Panel();
	Panel panelWorkTaskName = new Panel();
	Panel panelTaskFreq = new Panel();
	Panel panelTaskDuration = new Panel();
	Panel panelIncident = new Panel();
	Panel panelScoupe = new Panel();
	Panel panelPhysMentalReq = new Panel();
	Panel panelTaskDependency = new Panel();
	Panel panelTaskResult = new Panel();
	Panel panelTaskErrorImpact = new Panel();
	Panel panelTaskDanger = new Panel();

	// Layouts f�r die Fragenbl�cke
	VerticalLayout layoutTaskCategory = new VerticalLayout();
	VerticalLayout layoutWorkTaskName = new VerticalLayout();
	VerticalLayout layoutTaskFreq = new VerticalLayout();
	VerticalLayout layoutTaskDuration = new VerticalLayout();
	VerticalLayout layoutIncident = new VerticalLayout();
	VerticalLayout layoutScoupe = new VerticalLayout();
	VerticalLayout layoutPhysMentalReq = new VerticalLayout();
	VerticalLayout layoutTaskDependency = new VerticalLayout();
	VerticalLayout layoutTaskResult = new VerticalLayout();
	VerticalLayout layoutTaskErrorImpact = new VerticalLayout();
	VerticalLayout layoutTaskDanger = new VerticalLayout();

	// Verwendete Felder
	ComboBox taskCategory1 = new ComboBox();
	ComboBox taskCategory2 = new ComboBox();
	ComboBox taskCategory3 = new ComboBox();
	TextField workTaskName = new TextField();
	TextField taskFreqValue = new TextField();
	ComboBox taskFreqEntity = new ComboBox();
	TextField taskDuration = new TextField();
	ComboBox taskDurationEntity = new ComboBox();
	ComboBox incident = new ComboBox();
	TextField incidentDuration = new TextField();
	TextField incidentFreq = new TextField();
	ComboBox scoupe = new ComboBox();
	TextField processRatio = new TextField();
	TextArea physMentalReq = new TextArea();
	TextArea taskDependency = new TextArea();
	TextArea taskResult = new TextArea();
	TextArea taskErrorImpact = new TextArea();
	ComboBox taskDanger = new ComboBox();

	// mapping von Feldern auf Daten in Java (Pojos)
	private WorkTask workTaskBean = new WorkTask();
	private FieldGroup workTaskBinder = new FieldGroup();
	BeanItem<WorkTask> workTaskItem = new BeanItem<WorkTask>(workTaskBean);

	// SessionFactory factory = new
	// Configuration().configure().buildSessionFactory();

	@SuppressWarnings("serial")
	public WorkTaskView() {
		getHeadline().setValue(KontextUIText.getLabel("worktaskCaption", "items1"));

		// ##taskCategory
		panelTaskCategory = new Panel(KontextUIText.getLabel("taskCategory", "question"));
		// panel.setSizeUndefined();
		taskCategory1 = CreateComponent.createComboBox("taskCategory", "items2", KontextUIText.getLabel("taskCategory", "items1"));
		taskCategory2 = CreateComponent.createComboBox("taskCategory", "items2", KontextUIText.getLabel("taskCategory", "items1"));
		taskCategory3 = CreateComponent.createComboBox("taskCategory", "items2", KontextUIText.getLabel("taskCategory", "items1"));
		taskCategory1.setInputPrompt(KontextUIText.getLabel("taskCategory", "inputPrompt"));
		taskCategory2.setInputPrompt(KontextUIText.getLabel("taskCategory", "inputPrompt"));
		taskCategory3.setInputPrompt(KontextUIText.getLabel("taskCategory", "inputPrompt"));
		taskCategory2.addStyleName("hidden");
		taskCategory3.addStyleName("hidden");
		taskCategory1.addValueChangeListener(new VisibilityChangeListener(taskCategory1, taskCategory2, null, null));
		taskCategory2.addValueChangeListener(new VisibilityChangeListener(taskCategory2, taskCategory3, null, null));
		layoutTaskCategory.addComponent(taskCategory1);
		layoutTaskCategory.addComponent(taskCategory2);
		layoutTaskCategory.addComponent(taskCategory3);
		// layout.setSizeUndefined();
		layoutTaskCategory.setMargin(true);
		panelTaskCategory.setContent(layoutTaskCategory);
		addComponent(panelTaskCategory);

		// ##workTaskName
		panelWorkTaskName = new Panel(KontextUIText.getLabel("workTaskName", "question"));
		// panel.setSizeUndefined();
		workTaskName = CreateComponent.createTextField("workTaskName", "items1");
		workTaskName.setInputPrompt(KontextUIText.getLabel("workTaskName", "inputPrompt"));
		layoutWorkTaskName.addComponent(workTaskName);
		// layout.setSizeUndefined();
		layoutWorkTaskName.setMargin(true);
		panelWorkTaskName.setContent(layoutWorkTaskName);
		addComponent(panelWorkTaskName);

		// // ##taskFreq
		panelTaskFreq = new Panel(KontextUIText.getLabel("taskFreq", "question"));
		// panel.setSizeUndefined();
		taskFreqValue = CreateComponent.createTextField("taskFreq", "items1");
		taskFreqEntity = CreateComponent.createComboBox("taskFreq", "items3", KontextUIText.getLabel("taskFreq", "items2"));
		taskFreqValue.setInputPrompt(KontextUIText.getLabel("taskFreq", "items1"));
		taskFreqEntity.setInputPrompt(KontextUIText.getLabel("taskFreq", "items2"));
		layoutTaskFreq.addComponent(taskFreqValue);
		layoutTaskFreq.addComponent(taskFreqEntity);
		// layout.setSizeUndefined();
		layoutTaskFreq.setMargin(true);
		panelTaskFreq.setContent(layoutTaskFreq);
		addComponent(panelTaskFreq);

		// ##taskDuration
		panelTaskDuration = new Panel(KontextUIText.getLabel("taskDuration", "question"));
		// panel.setSizeUndefined();
		taskDuration = CreateComponent.createTextField("taskDuration", "items1");
		taskDurationEntity = CreateComponent.createComboBox("taskDuration", "items3", KontextUIText.getLabel("taskDuration", "items2"));
		taskDuration.setInputPrompt(KontextUIText.getLabel("taskDuration", "items1"));
		taskDurationEntity.setInputPrompt(KontextUIText.getLabel("taskDuration", "items2"));
		layoutTaskDuration.addComponent(taskDuration);
		layoutTaskDuration.addComponent(taskDurationEntity);
		// layout.setSizeUndefined();
		layoutTaskDuration.setMargin(true);
		panelTaskDuration.setContent(layoutTaskDuration);
		addComponent(panelTaskDuration);

		// ##incident
		panelIncident = new Panel(KontextUIText.getLabel("incident", "question"));
		// panel.setSizeUndefined();
		incident = CreateComponent.createComboBoxYN(KontextUIText.getLabel("incident", "items1"));
		incidentDuration = CreateComponent.createTextField("incident", "items2");
		incidentFreq = CreateComponent.createTextField("incident", "items3");
		incidentDuration.setInputPrompt(KontextUIText.getLabel("incident", "items2"));
		incidentFreq.setInputPrompt(KontextUIText.getLabel("incident", "items3"));
		incidentDuration.addStyleName("hidden");
		incidentFreq.addStyleName("hidden");
		incident.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				if (incident.getValue() != null && incident.getValue().equals("Ja")) {
					incidentDuration.removeStyleName("hidden");
					incidentFreq.removeStyleName("hidden");
				} else {
					incidentDuration.addStyleName("hidden");
					incidentFreq.addStyleName("hidden");
				}

			}
		});
		layoutIncident.addComponent(incident);
		layoutIncident.addComponent(incidentDuration);
		layoutIncident.addComponent(incidentFreq);
		// layout.setSizeUndefined();
		layoutIncident.setMargin(true);
		panelIncident.setContent(layoutIncident);
		addComponent(panelIncident);

		// ##scoupe
		panelScoupe = new Panel(KontextUIText.getLabel("scoupe", "question"));
		// panel.setSizeUndefined();
		scoupe = CreateComponent.createComboBoxYN(KontextUIText.getLabel("scoupe", "items1"));
		processRatio = CreateComponent.createTextField("scoupe", "items2");
		processRatio.setInputPrompt(KontextUIText.getLabel("scoupe", "items2"));
		processRatio.addStyleName("hidden");
		scoupe.addValueChangeListener(new VisibilityChangeListener(scoupe, processRatio, null, null));
		layoutScoupe.addComponent(scoupe);
		layoutScoupe.addComponent(processRatio);
		// layout.setSizeUndefined();
		layoutScoupe.setMargin(true);
		panelScoupe.setContent(layoutScoupe);
		addComponent(panelScoupe);

		// ##physMentalReq
		panelPhysMentalReq = new Panel(KontextUIText.getLabel("physMentalReq", "question"));
		// panel.setSizeUndefined();
		physMentalReq = CreateComponent.createTextArea("physMentalReq", "items1");
		physMentalReq.setInputPrompt(KontextUIText.getLabel("physMentalReq", "items1"));
		layoutPhysMentalReq.addComponent(physMentalReq);
		// layout.setSizeUndefined();
		layoutPhysMentalReq.setMargin(true);
		panelPhysMentalReq.setContent(layoutPhysMentalReq);
		addComponent(panelPhysMentalReq);

		// ##taskDependency
		panelTaskDependency = new Panel(KontextUIText.getLabel("taskDependency", "question"));
		// panel.setSizeUndefined();
		taskDependency = CreateComponent.createTextArea("taskDependency", "items1");
		taskDependency.setInputPrompt(KontextUIText.getLabel("taskDependency", "items1"));
		layoutTaskDependency = new VerticalLayout();
		// layout.setSizeUndefined();
		layoutTaskDependency.addComponent(taskDependency);
		layoutTaskDependency.setMargin(true);
		panelTaskDependency.setContent(layoutTaskDependency);
		addComponent(panelTaskDependency);

		// ##taskResult
		panelTaskResult = new Panel(KontextUIText.getLabel("taskResult", "question"));
		// panel.setSizeUndefined();
		taskResult = CreateComponent.createTextArea("taskResult", "items1");
		taskResult.setInputPrompt(KontextUIText.getLabel("taskResult", "inputPrompt"));
		layoutTaskResult.addComponent(taskResult);
		// layout.setSizeUndefined();
		layoutTaskResult.setMargin(true);
		panelTaskResult.setContent(layoutTaskResult);
		addComponent(panelTaskResult);

		// ##taskErrorImpact
		panelTaskErrorImpact = new Panel(KontextUIText.getLabel("taskErrorImpact", "question"));
		// panel.setSizeUndefined();
		taskErrorImpact = CreateComponent.createTextArea("taskErrorImpact", "items1");
		taskErrorImpact.setInputPrompt(KontextUIText.getLabel("taskErrorImpact", "items1"));
		layoutTaskErrorImpact.addComponent(taskErrorImpact);
		// layout.setSizeUndefined();
		layoutTaskErrorImpact.setMargin(true);
		panelTaskErrorImpact.setContent(layoutTaskErrorImpact);
		addComponent(panelTaskErrorImpact);

		// ##taskDanger
		panelTaskDanger = new Panel(KontextUIText.getLabel("taskDanger", "question"));
		// panel.setSizeUndefined();
		taskDanger = CreateComponent.createComboBox("taskDanger", "items2", KontextUIText.getLabel("taskDanger", "items1"));
		taskDanger.setInputPrompt(KontextUIText.getLabel("taskDanger", "items1"));
		layoutTaskDanger.addComponent(taskDanger);
		// layout.setSizeUndefined();
		layoutTaskDanger.setMargin(true);
		panelTaskDanger.setContent(layoutTaskDanger);
		addComponent(panelTaskDanger);

		workTaskBinder.setBuffered(false);
		workTaskBinder.setItemDataSource(workTaskItem);
		workTaskBinder.bindMemberFields(this);

		taskCategory1.addValueChangeListener(new BarChangeListener(taskCategory1, components, this));
		taskCategory2.addValueChangeListener(new BarChangeListener(taskCategory2, components, this));
		taskCategory3.addValueChangeListener(new BarChangeListener(taskCategory3, components, this));
		workTaskName.addValueChangeListener(new BarChangeListener(workTaskName, components, this));
		taskFreqValue.addValueChangeListener(new BarChangeListener(taskFreqValue, components, this));
		taskFreqEntity.addValueChangeListener(new BarChangeListener(taskFreqEntity, components, this));
		taskDuration.addValueChangeListener(new BarChangeListener(taskDuration, components, this));
		taskDurationEntity.addValueChangeListener(new BarChangeListener(taskDurationEntity, components, this));
		incident.addValueChangeListener(new BarChangeListener(incident, components, this));
		incidentDuration.addValueChangeListener(new BarChangeListener(incidentDuration, components, this));
		incidentFreq.addValueChangeListener(new BarChangeListener(incidentFreq, components, this));
		scoupe.addValueChangeListener(new BarChangeListener(scoupe, components, this));
		processRatio.addValueChangeListener(new BarChangeListener(processRatio, components, this));
		physMentalReq.addValueChangeListener(new BarChangeListener(physMentalReq, components, this));
		taskDependency.addValueChangeListener(new BarChangeListener(taskDependency, components, this));
		taskResult.addValueChangeListener(new BarChangeListener(taskResult, components, this));
		taskErrorImpact.addValueChangeListener(new BarChangeListener(taskErrorImpact, components, this));
		taskDanger.addValueChangeListener(new BarChangeListener(taskDanger, components, this));

		panelTaskCategory.addStyleName("panel");
		panelWorkTaskName.addStyleName("panel");
		panelTaskFreq.addStyleName("panel");
		panelTaskDuration.addStyleName("panel");
		panelIncident.addStyleName("panel");
		panelScoupe.addStyleName("panel");
		panelPhysMentalReq.addStyleName("panel");
		panelTaskDependency.addStyleName("panel");
		panelTaskResult.addStyleName("panel");
		panelTaskErrorImpact.addStyleName("panel");
		panelTaskDanger.addStyleName("panel");

	}

	public int getComponents() {
		return components;
	}

	public WorkTask getWorkTaskBean() {
		return workTaskBean;
	}

	public void setWorkTaskBean(WorkTask workTaskBean) {
		this.workTaskBean = workTaskBean;
	}

	public FieldGroup getWorkTaskBinder() {
		return workTaskBinder;
	}

	public void setWorkTaskBinder(FieldGroup workTaskBinder) {
		this.workTaskBinder = workTaskBinder;
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub

	}

}
