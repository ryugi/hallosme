package com.hallosme.views.nutzungskontext;

import com.hallosme.components.listener.BarChangeListener;
import com.hallosme.components.listener.VisibilityChangeListener;
import com.hallosme.pojos.UserType;
import com.hallosme.utils.CreateComponent;
import com.hallosme.utils.KontextUIText;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class UserTypeView extends BasicKontextView implements View {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4378202070560270257L;

	final int components = 28;

	// Fragen Bl�cke
	Panel panelUserType = new Panel();
	Panel panelExpMobAss = new Panel();
	Panel panelExpWorkTask = new Panel();
	Panel panelExpOrg = new Panel();
	Panel panelUebungsgrad = new Panel();
	Panel panelSkillInputDiv = new Panel();
	Panel panelQuallification = new Panel();
	Panel panelLanguages = new Panel();
	Panel panelGeneralKnowledge = new Panel();
	Panel panelAge = new Panel();
	Panel panelSex = new Panel();
	Panel panelMentalSkill = new Panel();
	Panel panelAttitude = new Panel();

	// Layouts f�r die Fragenbl�cke
	VerticalLayout layoutUserType = new VerticalLayout();
	VerticalLayout layoutExpMobAss = new VerticalLayout();
	VerticalLayout layoutExpWorkTask = new VerticalLayout();
	VerticalLayout layoutExpOrg = new VerticalLayout();
	VerticalLayout layoutUebungsgrad = new VerticalLayout();
	VerticalLayout layoutSkillInputDiv = new VerticalLayout();
	VerticalLayout layoutQuallification = new VerticalLayout();
	VerticalLayout layoutLanguages = new VerticalLayout();
	VerticalLayout layoutGeneralKnowledge = new VerticalLayout();
	VerticalLayout layoutAge = new VerticalLayout();
	VerticalLayout layoutSex = new VerticalLayout();
	VerticalLayout layoutMentalSkill = new VerticalLayout();
	VerticalLayout layoutAttitude = new VerticalLayout();

	// Verwendete Felder
	ComboBox userType = new ComboBox();
	TextField expMobAss = new TextField();
	TextField expWorkTask = new TextField();
	TextField expOrg = new TextField();
	ComboBox uebungsgrad = new ComboBox();
	CheckBox mouseUsed = new CheckBox();
	ComboBox skillMouse = new ComboBox();
	CheckBox keyboardUsed = new CheckBox();
	TextField skillKeyboard = new TextField();
	CheckBox touchscreenUsed = new CheckBox();
	ComboBox skillTouchscreen = new ComboBox();
	ComboBox quallificationField1 = new ComboBox();
	TextField quallificationFieldStake1 = new TextField();
	ComboBox quallificationField2 = new ComboBox();
	TextField quallificationFieldStake2 = new TextField();
	ComboBox quallificationField3 = new ComboBox();
	TextField quallificationFieldStake3 = new TextField();
	ComboBox language1 = new ComboBox();
	ComboBox languageLevel1 = new ComboBox();
	ComboBox language2 = new ComboBox();
	ComboBox languageLevel2 = new ComboBox();
	ComboBox language3 = new ComboBox();
	ComboBox languageLevel3 = new ComboBox();
	TextArea generalKnowledge = new TextArea();
	ComboBox age = new ComboBox();
	ComboBox sex = new ComboBox();
	ComboBox mentalSkill = new ComboBox();
	TextArea attitude = new TextArea();

	private UserType userTypeBean = new UserType();
	private FieldGroup userTypeBinder = new FieldGroup();
	BeanItem<UserType> userTypeItem = new BeanItem<UserType>(userTypeBean);

	// SessionFactory factory = new
	// Configuration().configure().buildSessionFactory();

	public UserTypeView() {
		getHeadline().setValue(KontextUIText.getLabel("userCaption", "items1"));
		// ##UserType
		panelUserType = new Panel(KontextUIText.getLabel("userType", "question"));
		// panel.setSizeUndefined();
		userType = CreateComponent.createComboBox("userType", "items1", "Benutzertyp");
		userType.setInputPrompt(KontextUIText.getLabel("userType", "inputPrompt"));
		layoutUserType.addComponent(userType);
		// layout.setSizeUndefined();
		layoutUserType.setMargin(true);
		panelUserType.setContent(layoutUserType);
		addComponent(panelUserType);

		// ##expMobAss
		panelExpMobAss = new Panel(KontextUIText.getLabel("expMobAss", "question"));
		// panel.setSizeUndefined();
		expMobAss = CreateComponent.createTextField("expMobAss", "items1");
		expMobAss.setInputPrompt(KontextUIText.getLabel("expMobAss", "items1"));
		layoutExpMobAss.addComponent(expMobAss);
		// layout.setSizeUndefined();
		layoutExpMobAss.setMargin(true);
		panelExpMobAss.setContent(layoutExpMobAss);
		addComponent(panelExpMobAss);

		// // ##expWorkTask
		panelExpWorkTask = new Panel(KontextUIText.getLabel("expWorkTask", "question"));
		// panel.setSizeUndefined();
		expWorkTask = CreateComponent.createTextField("expWorkTask", "items1");
		expWorkTask.setInputPrompt(KontextUIText.getLabel("expWorkTask", "items1"));
		layoutExpWorkTask.addComponent(expWorkTask);
		// layout.setSizeUndefined();
		layoutExpWorkTask.setMargin(true);
		panelExpWorkTask.setContent(layoutExpWorkTask);
		addComponent(panelExpWorkTask);

		// ##expOrg
		panelExpOrg = new Panel(KontextUIText.getLabel("expOrg", "question"));
		// panel.setSizeUndefined();
		expOrg = CreateComponent.createTextField("expOrg", "items1");
		expOrg.setInputPrompt(KontextUIText.getLabel("expOrg", "items1"));
		layoutExpOrg.addComponent(expOrg);
		// layout.setSizeUndefined();
		layoutExpOrg.setMargin(true);
		panelExpOrg.setContent(layoutExpOrg);
		addComponent(panelExpOrg);

		// ##uebungsgrad
		panelUebungsgrad = new Panel(KontextUIText.getLabel("uebungsgrad", "question"));
		// panel.setSizeUndefined();
		uebungsgrad = CreateComponent.createComboBox("uebungsgrad", "items1", null);
		uebungsgrad.setInputPrompt(KontextUIText.getLabel("uebungsgrad", "inputPrompt"));
		layoutUebungsgrad.addComponent(uebungsgrad);
		// layout.setSizeUndefined();
		layoutUebungsgrad.setMargin(true);
		panelUebungsgrad.setContent(layoutUebungsgrad);
		addComponent(panelUebungsgrad);

		// ##skillInputDevice
		panelSkillInputDiv = new Panel(KontextUIText.getLabel("skillInputDevice", "question"));
		// panel.setSizeUndefined();
		mouseUsed = CreateComponent.createCheckBox("skillInputDevice", "items1", 0);
		keyboardUsed = CreateComponent.createCheckBox("skillInputDevice", "items1", 1);
		touchscreenUsed = CreateComponent.createCheckBox("skillInputDevice", "items1", 2);
		skillMouse = CreateComponent.createComboBoxYN(KontextUIText.getLabel("skillInputDevice", "items2"));
		skillKeyboard = CreateComponent.createTextField("skillInputDevice", "items3");
		skillTouchscreen = CreateComponent.createComboBoxYN(KontextUIText.getLabel("skillInputDevice", "items2"));

		mouseUsed.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = -1119581893119481741L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (mouseUsed.getValue() == true) {
					// skillMouse.removeStyleName("hidden");
					skillMouse.removeStyleName("hidden");
				} else {
					skillMouse.select(null);
					skillMouse.addStyleName("hidden");
					// skillMouse.addStyleName("hidden");
				}
			}
		});

		keyboardUsed.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = -1119581893119481741L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (keyboardUsed.getValue() == true) {
					// skillKeyboard.removeStyleName("hidden");
					skillKeyboard.removeStyleName("hidden");
				} else {
					skillKeyboard.setValue("0");
					// skillKeyboard.addStyleName("hidden");
					skillKeyboard.addStyleName("hidden");
				}
			}
		});

		touchscreenUsed.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = -1119581893119481741L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (touchscreenUsed.getValue() == true) {
					// skillTouchscreen.removeStyleName("hidden");
					skillTouchscreen.removeStyleName("hidden");

				} else {
					skillTouchscreen.select(null);
					// skillTouchscreen.addStyleName("hidden");
					skillTouchscreen.addStyleName("hidden");
				}
			}
		});

		mouseUsed.addStyleName("checkBox");
		keyboardUsed.addStyleName("checkBox");
		touchscreenUsed.addStyleName("checkBox");
		skillMouse.addStyleName("hidden");
		skillKeyboard.addStyleName("hidden");
		skillTouchscreen.addStyleName("hidden");

		HorizontalLayout horizontalLayoutMouse = new HorizontalLayout();
		HorizontalLayout horizontalLayoutKeyboard = new HorizontalLayout();
		HorizontalLayout horizontalLayoutTouchscreen = new HorizontalLayout();

		horizontalLayoutMouse.addComponent(mouseUsed);
		horizontalLayoutMouse.addComponent(skillMouse);
		horizontalLayoutKeyboard.addComponent(keyboardUsed);
		horizontalLayoutKeyboard.addComponent(skillKeyboard);
		horizontalLayoutTouchscreen.addComponent(touchscreenUsed);
		horizontalLayoutTouchscreen.addComponent(skillTouchscreen);

		layoutSkillInputDiv.addComponent(horizontalLayoutMouse);
		layoutSkillInputDiv.addComponent(horizontalLayoutKeyboard);
		layoutSkillInputDiv.addComponent(horizontalLayoutTouchscreen);

		// layout.setSizeUndefined();
		layoutSkillInputDiv.setMargin(true);
		panelSkillInputDiv.setContent(layoutSkillInputDiv);
		addComponent(panelSkillInputDiv);

		// ##quallification
		panelQuallification = new Panel(KontextUIText.getLabel("quallification", "question"));
		// panel.setSizeUndefined();
		quallificationField1 = CreateComponent.createComboBox("quallification", "items1", null);
		quallificationFieldStake1 = CreateComponent.createTextField("quallification", "items2");
		quallificationField2 = CreateComponent.createComboBox("quallification", "items1", null);
		quallificationFieldStake2 = CreateComponent.createTextField("quallification", "items2");
		quallificationField3 = CreateComponent.createComboBox("quallification", "items1", null);
		quallificationFieldStake3 = CreateComponent.createTextField("quallification", "items2");
		quallificationField1.setInputPrompt(KontextUIText.getLabel("quallification", "inputPrompt"));
		quallificationField2.setInputPrompt(KontextUIText.getLabel("quallification", "inputPrompt"));
		quallificationField3.setInputPrompt(KontextUIText.getLabel("quallification", "inputPrompt"));
		quallificationFieldStake1.setInputPrompt(KontextUIText.getLabel("quallification", "items2"));
		quallificationFieldStake2.setInputPrompt(KontextUIText.getLabel("quallification", "items2"));
		quallificationFieldStake3.setInputPrompt(KontextUIText.getLabel("quallification", "items2"));

		quallificationField1.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = -1119581893119481741L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (quallificationField1.getValue() != null && !quallificationField1.getValue().equals("")) {
					quallificationFieldStake1.removeStyleName("hidden");
				} else {
					quallificationField2.select(null);
					quallificationField3.select(null);
					quallificationFieldStake1.setValue("0");
					quallificationFieldStake2.setValue("0");
					quallificationFieldStake3.setValue("0");
					quallificationField2.addStyleName("hidden");
					quallificationField3.addStyleName("hidden");
					quallificationFieldStake1.addStyleName("hidden");
					quallificationFieldStake2.addStyleName("hidden");
					quallificationFieldStake3.addStyleName("hidden");
				}
			}
		});
		quallificationFieldStake1.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = -1119581893119481741L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (quallificationFieldStake1.getValue() != null && !quallificationFieldStake1.getValue().equals("")
						&& !quallificationFieldStake1.getValue().equals("0")) {
					quallificationField2.removeStyleName("hidden");
				} else {
					quallificationField2.select(null);
					quallificationField2.select(null);
					quallificationFieldStake2.setValue("0");
					quallificationFieldStake3.setValue("0");
					quallificationField2.addStyleName("hidden");
					quallificationField3.addStyleName("hidden");
					quallificationFieldStake2.addStyleName("hidden");
					quallificationFieldStake3.addStyleName("hidden");
				}
			}
		});
		quallificationField2.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = -1119581893119481741L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (quallificationField2.getValue() != null && !quallificationField2.getValue().equals("")) {
					quallificationFieldStake2.removeStyleName("hidden");
				} else {
					quallificationField3.select(null);
					quallificationFieldStake3.setValue("0");
					quallificationField3.addStyleName("hidden");
					quallificationFieldStake3.addStyleName("hidden");

				}
			}
		});
		quallificationFieldStake2.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = -1119581893119481741L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (quallificationFieldStake2.getValue() != null && !quallificationFieldStake2.getValue().equals("")
						&& !quallificationFieldStake2.getValue().equals("0")) {
					quallificationField3.removeStyleName("hidden");
				} else {
					quallificationField3.select(null);
					quallificationFieldStake3.setValue("0");
					quallificationField3.addStyleName("hidden");
					quallificationFieldStake3.addStyleName("hidden");

				}
			}
		});
		quallificationField3.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = -1119581893119481741L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (quallificationField3.getValue() != null && !quallificationField3.getValue().equals("")) {
					quallificationFieldStake3.removeStyleName("hidden");
				} else {
					quallificationFieldStake3.setValue("0");
					quallificationFieldStake3.addStyleName("hidden");
				}
			}
		});

		quallificationField1.addValueChangeListener(new VisibilityChangeListener(quallificationField1, quallificationFieldStake1, null, null));
		quallificationField2.addValueChangeListener(new VisibilityChangeListener(quallificationField2, quallificationFieldStake2, null, null));
		quallificationField3.addValueChangeListener(new VisibilityChangeListener(quallificationField3, quallificationFieldStake3, null, null));
		quallificationFieldStake1.addValueChangeListener(new VisibilityChangeListener(quallificationFieldStake1, quallificationField2, null, null));
		quallificationFieldStake2.addValueChangeListener(new VisibilityChangeListener(quallificationFieldStake2, quallificationField3, null, null));
		quallificationField2.addStyleName("hidden");
		quallificationField3.addStyleName("hidden");
		quallificationFieldStake1.addStyleName("hidden");
		quallificationFieldStake2.addStyleName("hidden");
		quallificationFieldStake3.addStyleName("hidden");
		layoutQuallification.addComponent(quallificationField1);
		layoutQuallification.addComponent(quallificationFieldStake1);
		layoutQuallification.addComponent(quallificationField2);
		layoutQuallification.addComponent(quallificationFieldStake2);
		layoutQuallification.addComponent(quallificationField3);
		layoutQuallification.addComponent(quallificationFieldStake3);
		// layout.setSizeUndefined();
		layoutQuallification.setMargin(true);
		panelQuallification.setContent(layoutQuallification);
		addComponent(panelQuallification);

		// ##languages
		panelLanguages = new Panel(KontextUIText.getLabel("languages", "question"));
		// panel.setSizeUndefined();
		layoutLanguages = new VerticalLayout();
		// layout.setSizeUndefined();
		layoutLanguages.setMargin(true);
		language1 = CreateComponent.createComboBox("languages", "items1", "Sprache ");
		languageLevel1 = CreateComponent.createComboBox("languages", "items2", "Niveau ");
		language2 = CreateComponent.createComboBox("languages", "items1", "Sprache ");
		languageLevel2 = CreateComponent.createComboBox("languages", "items2", "Niveau ");
		language3 = CreateComponent.createComboBox("languages", "items1", "Sprache ");
		languageLevel3 = CreateComponent.createComboBox("languages", "items2", "Niveau ");
		language1.setInputPrompt(KontextUIText.getLabel("languages", "inputPrompt"));
		language2.setInputPrompt(KontextUIText.getLabel("languages", "inputPrompt"));
		language3.setInputPrompt(KontextUIText.getLabel("languages", "inputPrompt"));
		languageLevel1.setInputPrompt(KontextUIText.getLabel("languages", "items3"));
		languageLevel2.setInputPrompt(KontextUIText.getLabel("languages", "items3"));
		languageLevel3.setInputPrompt(KontextUIText.getLabel("languages", "items3"));

		language2.addStyleName("hidden");
		language3.addStyleName("hidden");
		languageLevel1.addStyleName("hidden");
		languageLevel2.addStyleName("hidden");
		languageLevel3.addStyleName("hidden");

		language1.addValueChangeListener(new ValueChangeListener() {

			private static final long serialVersionUID = -1800187814013196181L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (language1.getValue() != null && !language1.getValue().equals("")) {
					languageLevel1.removeStyleName("hidden");
				} else {
					language2.select(null);
					language3.select(null);
					languageLevel1.select(null);
					languageLevel2.select(null);
					languageLevel3.select(null);

					language2.addStyleName("hidden");
					language3.addStyleName("hidden");
					languageLevel1.addStyleName("hidden");
					languageLevel2.addStyleName("hidden");
					languageLevel3.addStyleName("hidden");
				}

			}
		});

		languageLevel1.addValueChangeListener(new ValueChangeListener() {

			private static final long serialVersionUID = 1796685585574804120L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (languageLevel1.getValue() != null && !languageLevel1.getValue().equals("")) {
					language2.removeStyleName("hidden");
				} else {
					language2.select(null);
					language3.select(null);
					languageLevel2.select(null);
					languageLevel3.select(null);

					language2.addStyleName("hidden");
					language3.addStyleName("hidden");
					languageLevel2.addStyleName("hidden");
					languageLevel3.addStyleName("hidden");
				}

			}
		});

		language2.addValueChangeListener(new ValueChangeListener() {

			private static final long serialVersionUID = 194854753684203313L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (language2.getValue() != null && !language2.getValue().equals("")) {
					languageLevel2.removeStyleName("hidden");
				} else {
					language3.select(null);
					languageLevel2.select(null);
					languageLevel3.select(null);

					language3.addStyleName("hidden");
					languageLevel2.addStyleName("hidden");
					languageLevel3.addStyleName("hidden");
				}

			}
		});

		languageLevel2.addValueChangeListener(new ValueChangeListener() {

			private static final long serialVersionUID = -8552776713808214794L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (languageLevel2.getValue() != null && !languageLevel2.getValue().equals("")) {
					language3.removeStyleName("hidden");
				} else {
					language3.select(null);
					languageLevel3.select(null);

					language3.addStyleName("hidden");
					languageLevel3.addStyleName("hidden");
				}

			}
		});

		language3.addValueChangeListener(new ValueChangeListener() {

			private static final long serialVersionUID = 2926990382028868361L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				if (language3.getValue() != null && !language3.getValue().equals("")) {
					languageLevel3.removeStyleName("hidden");
				} else {
					languageLevel3.select(null);

					languageLevel3.addStyleName("hidden");
				}

			}
		});

		HorizontalLayout languageLayout1 = new HorizontalLayout();
		HorizontalLayout languageLayout2 = new HorizontalLayout();
		HorizontalLayout languageLayout3 = new HorizontalLayout();

		languageLayout1.addComponent(language1);
		languageLayout1.addComponent(languageLevel1);
		languageLayout2.addComponent(language2);
		languageLayout2.addComponent(languageLevel2);
		languageLayout3.addComponent(language3);
		languageLayout3.addComponent(languageLevel3);
		layoutLanguages.addComponent(languageLayout1);
		layoutLanguages.addComponent(languageLayout2);
		layoutLanguages.addComponent(languageLayout3);

		panelLanguages.setContent(layoutLanguages);
		addComponent(panelLanguages);

		// ##generalKnowledge
		panelGeneralKnowledge = new Panel(KontextUIText.getLabel("generalKnowledge", "question"));
		// panel.setSizeUndefined();
		generalKnowledge = CreateComponent.createTextArea("generalKnowledge", "items1");
		generalKnowledge.setInputPrompt(KontextUIText.getLabel("generalKnowledge", "items1"));
		layoutGeneralKnowledge.addComponent(generalKnowledge);
		// layout.setSizeUndefined();
		layoutGeneralKnowledge.setMargin(true);
		panelGeneralKnowledge.setContent(layoutGeneralKnowledge);
		addComponent(panelGeneralKnowledge);

		// ##age
		panelAge = new Panel(KontextUIText.getLabel("age", "question"));
		// panel.setSizeUndefined();
		age = CreateComponent.createComboBox("age", "items1", null);
		age.setInputPrompt(KontextUIText.getLabel("age", "inputPrompt"));
		layoutAge.addComponent(age);
		// layout.setSizeUndefined();
		layoutAge.setMargin(true);
		panelAge.setContent(layoutAge);
		addComponent(panelAge);

		// ##sex
		panelSex = new Panel(KontextUIText.getLabel("sex", "question"));
		// panel.setSizeUndefined();
		sex = CreateComponent.createComboBox("sex", "items1", null);
		sex.setInputPrompt(KontextUIText.getLabel("sex", "inputPrompt"));
		layoutSex.addComponent(sex);
		// layout.setSizeUndefined();
		layoutSex.setMargin(true);
		panelSex.setContent(layoutSex);
		addComponent(panelSex);

		// ##mentalSkill
		panelMentalSkill = new Panel(KontextUIText.getLabel("mentalSkill", "question"));
		// panel.setSizeUndefined();
		mentalSkill = CreateComponent.createComboBox("mentalSkill", "items1", null);
		mentalSkill.setInputPrompt(KontextUIText.getLabel("mentalSkill", "inputPrompt"));
		layoutMentalSkill.addComponent(mentalSkill);
		// layout.setSizeUndefined();
		layoutMentalSkill.setMargin(true);
		panelMentalSkill.setContent(layoutMentalSkill);
		addComponent(panelMentalSkill);

		// ##attitude
		panelAttitude = new Panel(KontextUIText.getLabel("attitude", "question"));
		// panel.setSizeUndefined();
		attitude = CreateComponent.createTextArea("attitude", "items1");
		attitude.setInputPrompt(KontextUIText.getLabel("attitude", "items1"));
		layoutAttitude.addComponent(attitude);
		// layout.setSizeUndefined();
		layoutAttitude.setMargin(true);
		panelAttitude.setContent(layoutAttitude);
		addComponent(panelAttitude);

		userTypeBinder.setBuffered(false);
		userTypeBinder.setItemDataSource(userTypeItem);
		userTypeBinder.bindMemberFields(this);

		userType.addValueChangeListener(new BarChangeListener(userType, components, this));
		expMobAss.addValueChangeListener(new BarChangeListener(expMobAss, components, this));
		expWorkTask.addValueChangeListener(new BarChangeListener(expWorkTask, components, this));
		expOrg.addValueChangeListener(new BarChangeListener(expOrg, components, this));
		uebungsgrad.addValueChangeListener(new BarChangeListener(uebungsgrad, components, this));
		mouseUsed.addValueChangeListener(new BarChangeListener(mouseUsed, components, this));
		skillMouse.addValueChangeListener(new BarChangeListener(skillMouse, components, this));
		keyboardUsed.addValueChangeListener(new BarChangeListener(keyboardUsed, components, this));
		skillKeyboard.addValueChangeListener(new BarChangeListener(skillKeyboard, components, this));
		touchscreenUsed.addValueChangeListener(new BarChangeListener(touchscreenUsed, components, this));
		skillTouchscreen.addValueChangeListener(new BarChangeListener(skillTouchscreen, components, this));
		quallificationField1.addValueChangeListener(new BarChangeListener(quallificationField1, components, this));
		quallificationFieldStake1.addValueChangeListener(new BarChangeListener(quallificationFieldStake1, components, this));
		quallificationField2.addValueChangeListener(new BarChangeListener(quallificationField2, components, this));
		quallificationFieldStake2.addValueChangeListener(new BarChangeListener(quallificationFieldStake2, components, this));
		quallificationField3.addValueChangeListener(new BarChangeListener(quallificationField3, components, this));
		quallificationFieldStake3.addValueChangeListener(new BarChangeListener(quallificationFieldStake3, components, this));
		language1.addValueChangeListener(new BarChangeListener(language1, components, this));
		languageLevel1.addValueChangeListener(new BarChangeListener(languageLevel1, components, this));
		language2.addValueChangeListener(new BarChangeListener(language2, components, this));
		languageLevel2.addValueChangeListener(new BarChangeListener(languageLevel2, components, this));
		language3.addValueChangeListener(new BarChangeListener(language3, components, this));
		languageLevel3.addValueChangeListener(new BarChangeListener(languageLevel3, components, this));
		generalKnowledge.addValueChangeListener(new BarChangeListener(generalKnowledge, components, this));
		age.addValueChangeListener(new BarChangeListener(age, components, this));
		sex.addValueChangeListener(new BarChangeListener(sex, components, this));
		mentalSkill.addValueChangeListener(new BarChangeListener(mentalSkill, components, this));
		attitude.addValueChangeListener(new BarChangeListener(attitude, components, this));

		panelUserType.addStyleName("panel");
		panelExpMobAss.addStyleName("panel");
		panelExpWorkTask.addStyleName("panel");
		panelExpOrg.addStyleName("panel");
		panelUebungsgrad.addStyleName("panel");
		panelSkillInputDiv.addStyleName("panel");
		panelQuallification.addStyleName("panel");
		panelLanguages.addStyleName("panel");
		panelGeneralKnowledge.addStyleName("panel");
		panelAge.addStyleName("panel");
		panelSex.addStyleName("panel");
		panelMentalSkill.addStyleName("panel");
		panelAttitude.addStyleName("panel");

	}

	public int getComponents() {
		return components;
	}

	public UserType getUserTypeBean() {
		return userTypeBean;
	}

	public void setUserTypeBean(UserType userTypeBean) {
		this.userTypeBean = userTypeBean;
	}

	public FieldGroup getUserTypeBinder() {
		return userTypeBinder;
	}

	public void setUserTypeBinder(FieldGroup userTypeBinder) {
		this.userTypeBinder = userTypeBinder;
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub

	}

}
