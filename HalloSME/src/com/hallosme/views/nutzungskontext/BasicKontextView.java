package com.hallosme.views.nutzungskontext;

import com.vaadin.ui.Label;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.VerticalLayout;

public class BasicKontextView extends VerticalLayout {
	private static final long serialVersionUID = 6557183108256265691L;

	private ProgressBar bar = new ProgressBar(0.0f);
	// Anzahl bereits bearbeiteter Punkte
	private Integer editedComponents = 0;
	private Label editedComponentsLabel = new Label();
	private Label headline = new Label();

	public BasicKontextView() {
		super();
		addStyleName("contentView");
		bar.addStyleName("progressbar");
		bar.addStyleName("redBar");
		editedComponentsLabel.addStyleName("editedComponentsLabel");
		headline.addStyleName("headlineStyle");
		addComponent(headline);
	}

	public ProgressBar getBar() {
		return bar;
	}

	public void setBar(ProgressBar bar) {
		this.bar = bar;
	}

	/**
	 * @return Anzahl bereits bearbeiteter Punkte
	 */
	public Integer getEditedComponents() {
		return editedComponents;
	}

	public void setEditedComponents(Integer editedComponents) {
		this.editedComponents = editedComponents;
	}

	/**
	 * @return Anzahl bereits bearbeiteter Punkte Label
	 */
	public Label getEditedComponentsLabel() {
		return editedComponentsLabel;
	}

	public void setEditedComponentsLabel(Label editedComponentsLabel) {
		this.editedComponentsLabel = editedComponentsLabel;
	}

	public Label getHeadline() {
		return headline;
	}

	public void setHeadline(Label headline) {
		this.headline = headline;
	}
}
