package com.hallosme.views.nutzungskontext;

import com.hallosme.components.listener.BarChangeListener;
import com.hallosme.components.listener.VisibilityChangeListener;
import com.hallosme.pojos.WorkTool;
import com.hallosme.utils.KontextUIText;
import com.hallosme.utils.CreateComponent;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class WorkToolView extends BasicKontextView implements View {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7111986810385186874L;
	final int components = 13;
	// Fragen Bl�cke
	Panel panelProductName = new Panel();
	Panel panelProductScoupe = new Panel();
	Panel panelImportantFunctions = new Panel();
	Panel panelHardware = new Panel();
	Panel panelSoftware = new Panel();
	Panel panelExtService = new Panel();
	Panel panelMisc = new Panel();

	// Layouts f�r die Fragenbl�cke
	VerticalLayout layoutProductName = new VerticalLayout();
	VerticalLayout layoutProductScoupe = new VerticalLayout();
	VerticalLayout layoutImportantFunctions = new VerticalLayout();
	VerticalLayout layoutHardware = new VerticalLayout();
	VerticalLayout layoutSoftware = new VerticalLayout();
	VerticalLayout layoutExtService = new VerticalLayout();
	VerticalLayout layoutMisc = new VerticalLayout();

	// Verwendete Felder
	ComboBox productName = new ComboBox();
	ComboBox productScoupe1 = new ComboBox();
	ComboBox productScoupe2 = new ComboBox();
	ComboBox productScoupe3 = new ComboBox();
	TextArea importantFunctions = new TextArea();
	TextField hardwareScreenSize = new TextField();
	CheckBox touchscreenNeed = new CheckBox();
	CheckBox mouseNeed = new CheckBox();
	CheckBox keyboardNeed = new CheckBox();
	ComboBox softwareCategory = new ComboBox();
	ComboBox extServiceNeed = new ComboBox();
	TextArea extServiceCategorys = new TextArea();
	TextArea misc = new TextArea();

	// mapping von Feldern auf Daten in Java (Pojos)
	private WorkTool workToolBean = new WorkTool();
	private FieldGroup workToolBinder = new FieldGroup();
	BeanItem<WorkTool> workToolItem = new BeanItem<WorkTool>(workToolBean);

	// SessionFactory factory = new
	// Configuration().configure().buildSessionFactory();

	public WorkToolView() {
		getHeadline().setValue(KontextUIText.getLabel("worktoolCaption", "items1"));

		// ##productName
		panelProductName = new Panel(KontextUIText.getLabel("productName", "question"));
		// panel.setSizeUndefined();
		productName = CreateComponent.createComboBox("productName", "items2", KontextUIText.getLabel("productName", "items1"));
		productName.setInputPrompt(KontextUIText.getLabel("productName", "items1"));
		layoutProductName.addComponent(productName);
		// layout.setSizeUndefined();
		layoutProductName.setMargin(true);
		panelProductName.setContent(layoutProductName);
		addComponent(panelProductName);

		// ##productScoupe
		panelProductScoupe = new Panel(KontextUIText.getLabel("productScoupe", "question"));
		// panel.setSizeUndefined();
		productScoupe1 = CreateComponent.createComboBox("productScoupe", "items2", KontextUIText.getLabel("productScoupe", "items1"));
		productScoupe2 = CreateComponent.createComboBox("productScoupe", "items2", KontextUIText.getLabel("productScoupe", "items1"));
		productScoupe3 = CreateComponent.createComboBox("productScoupe", "items2", KontextUIText.getLabel("productScoupe", "items1"));
		productScoupe1.setInputPrompt(KontextUIText.getLabel("productScoupe", "items1"));
		productScoupe2.setInputPrompt(KontextUIText.getLabel("productScoupe", "items1"));
		productScoupe3.setInputPrompt(KontextUIText.getLabel("productScoupe", "items1"));
		productScoupe2.addStyleName("hidden");
		productScoupe3.addStyleName("hidden");
		productScoupe1.addValueChangeListener(new VisibilityChangeListener(productScoupe1, productScoupe2, null, null));
		productScoupe2.addValueChangeListener(new VisibilityChangeListener(productScoupe2, productScoupe3, null, null));
		layoutProductScoupe.addComponent(productScoupe1);
		layoutProductScoupe.addComponent(productScoupe2);
		layoutProductScoupe.addComponent(productScoupe3);
		// layout.setSizeUndefined();
		layoutProductScoupe.setMargin(true);
		panelProductScoupe.setContent(layoutProductScoupe);
		addComponent(panelProductScoupe);

		// ##importantFunctions
		panelImportantFunctions = new Panel(KontextUIText.getLabel("importantFunctions", "question"));
		// panel.setSizeUndefined();
		importantFunctions = CreateComponent.createTextArea("importantFunctions", "items1");
		importantFunctions.setInputPrompt(KontextUIText.getLabel("importantFunctions", "items1"));
		layoutImportantFunctions.addComponent(importantFunctions);
		// layout.setSizeUndefined();
		layoutImportantFunctions.setMargin(true);
		panelImportantFunctions.setContent(layoutImportantFunctions);
		addComponent(panelImportantFunctions);

		// ##hardwareWorkTool
		panelHardware = new Panel(KontextUIText.getLabel("hardwareWorkTool", "question"));
		// panel.setSizeUndefined();
		hardwareScreenSize = CreateComponent.createTextField("hardwareWorkTool", "items1");
		hardwareScreenSize.setInputPrompt(KontextUIText.getLabel("hardwareWorkTool", "items1"));
		touchscreenNeed = CreateComponent.createCheckBox("hardwareWorkTool", "items2", 0);
		mouseNeed = CreateComponent.createCheckBox("hardwareWorkTool", "items2", 1);
		keyboardNeed = CreateComponent.createCheckBox("hardwareWorkTool", "items2", 2);
		layoutHardware.addComponent(hardwareScreenSize);
		layoutHardware.addComponent(touchscreenNeed);
		layoutHardware.addComponent(mouseNeed);
		layoutHardware.addComponent(keyboardNeed);
		// layout.setSizeUndefined();
		layoutHardware.setMargin(true);
		panelHardware.setContent(layoutHardware);
		addComponent(panelHardware);

		// ##softwareWorkTool
		panelSoftware = new Panel(KontextUIText.getLabel("softwareWorkTool", "question"));
		// panel.setSizeUndefined();
		softwareCategory = CreateComponent.createComboBox("softwareWorkTool", "items2", KontextUIText.getLabel("softwareWorkTool", "items1"));
		softwareCategory.setInputPrompt(KontextUIText.getLabel("softwareWorkTool", "items1"));
		layoutSoftware.addComponent(softwareCategory);
		// layout.setSizeUndefined();
		layoutSoftware.setMargin(true);
		panelSoftware.setContent(layoutSoftware);
		addComponent(panelSoftware);

		// ##extService
		panelExtService = new Panel(KontextUIText.getLabel("extService", "question"));
		// panel.setSizeUndefined();
		extServiceNeed = CreateComponent.createComboBoxYN(KontextUIText.getLabel("extService", "items1"));
		extServiceCategorys = CreateComponent.createTextArea("extService", "items2");
		extServiceCategorys.setInputPrompt(KontextUIText.getLabel("extService", "items2"));
		extServiceCategorys.addStyleName("hidden");
		extServiceNeed.addValueChangeListener(new VisibilityChangeListener(extServiceNeed, extServiceCategorys, null, null));
		layoutExtService.addComponent(extServiceNeed);
		layoutExtService.addComponent(extServiceCategorys);
		// layout.setSizeUndefined();
		layoutExtService.setMargin(true);
		panelExtService.setContent(layoutExtService);
		addComponent(panelExtService);

		// ##miscWorkTool
		panelMisc = new Panel(KontextUIText.getLabel("miscWorkTool", "question"));
		// panel.setSizeUndefined();
		misc = CreateComponent.createTextArea("miscWorkTool", "items1");
		misc.setInputPrompt(KontextUIText.getLabel("miscWorkTool", "items1"));
		layoutMisc.addComponent(misc);
		// layout.setSizeUndefined();
		layoutMisc.setMargin(true);
		panelMisc.setContent(layoutMisc);
		addComponent(panelMisc);

		workToolBinder.setBuffered(false);
		workToolBinder.setItemDataSource(workToolItem);
		workToolBinder.bindMemberFields(this);

		// Fortschrittsbalken
		productName.addValueChangeListener(new BarChangeListener(productName, components, this));
		productScoupe1.addValueChangeListener(new BarChangeListener(productScoupe1, components, this));
		productScoupe2.addValueChangeListener(new BarChangeListener(productScoupe2, components, this));
		productScoupe3.addValueChangeListener(new BarChangeListener(productScoupe3, components, this));
		importantFunctions.addValueChangeListener(new BarChangeListener(importantFunctions, components, this));
		hardwareScreenSize.addValueChangeListener(new BarChangeListener(hardwareScreenSize, components, this));
		touchscreenNeed.addValueChangeListener(new BarChangeListener(touchscreenNeed, components, this));
		mouseNeed.addValueChangeListener(new BarChangeListener(mouseNeed, components, this));
		keyboardNeed.addValueChangeListener(new BarChangeListener(keyboardNeed, components, this));
		softwareCategory.addValueChangeListener(new BarChangeListener(softwareCategory, components, this));
		extServiceNeed.addValueChangeListener(new BarChangeListener(extServiceNeed, components, this));
		extServiceCategorys.addValueChangeListener(new BarChangeListener(extServiceCategorys, components, this));
		misc.addValueChangeListener(new BarChangeListener(misc, components, this));

		panelProductName.addStyleName("panel");
		panelProductScoupe.addStyleName("panel");
		panelImportantFunctions.addStyleName("panel");
		panelHardware.addStyleName("panel");
		panelSoftware.addStyleName("panel");
		panelExtService.addStyleName("panel");
		panelMisc.addStyleName("panel");

	}

	public int getComponents() {
		return components;
	}

	public WorkTool getWorkToolBean() {
		return workToolBean;
	}

	public void setWorkToolBean(WorkTool workToolBean) {
		this.workToolBean = workToolBean;
	}

	public FieldGroup getWorkToolBinder() {
		return workToolBinder;
	}

	public void setWorkToolBinder(FieldGroup workToolBinder) {
		this.workToolBinder = workToolBinder;
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub

	}

}
