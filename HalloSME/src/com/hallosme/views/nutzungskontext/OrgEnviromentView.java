package com.hallosme.views.nutzungskontext;

import com.hallosme.components.listener.BarChangeListener;
import com.hallosme.components.listener.VisibilityChangeListener;
import com.hallosme.pojos.OrgEnviroment;
import com.hallosme.utils.KontextUIText;
import com.hallosme.utils.CreateComponent;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class OrgEnviromentView extends BasicKontextView implements View {

	private static final long serialVersionUID = -8699394999580170561L;
	final int components = 25;
	// Fragen Bl�cke
	Panel panelWorkHours = new Panel();
	Panel panelGroupwork = new Panel();
	Panel panelGoalOrg = new Panel();
	Panel panelAssistence = new Panel();
	Panel panelBreaks = new Panel();
	Panel panelManageStructure = new Panel();
	Panel panelCommunicationStructure = new Panel();
	Panel panelRegulationComputer = new Panel();
	Panel panelOrgGoals = new Panel();
	Panel panelWorkMix = new Panel();
	Panel panelPerformaceMessure = new Panel();
	Panel panelResultResponse = new Panel();
	Panel panelWorkSpeed = new Panel();
	Panel panelAutonomousWork = new Panel();
	Panel panelFreedomChoice = new Panel();

	// Layouts f�r die Fragenbl�cke
	VerticalLayout layoutWorkHours = new VerticalLayout();
	VerticalLayout layoutGroupwork = new VerticalLayout();
	VerticalLayout layoutGoalOrg = new VerticalLayout();
	VerticalLayout layoutAssistence = new VerticalLayout();
	VerticalLayout layoutBreaks = new VerticalLayout();
	VerticalLayout layoutManageStructure = new VerticalLayout();
	VerticalLayout layoutCommunicationStructure = new VerticalLayout();
	VerticalLayout layoutRegulationComputer = new VerticalLayout();
	VerticalLayout layoutOrgGoals = new VerticalLayout();
	VerticalLayout layoutWorkMix = new VerticalLayout();
	VerticalLayout layoutPerformaceMessure = new VerticalLayout();
	VerticalLayout layoutResultResponse = new VerticalLayout();
	VerticalLayout layoutWorkSpeed = new VerticalLayout();
	VerticalLayout layoutAutonomousWork = new VerticalLayout();
	VerticalLayout layoutFreedomChoice = new VerticalLayout();

	// Verwendete Felder
	TextField workHours = new TextField();
	ComboBox groupwork = new ComboBox();
	TextField groupworkSize = new TextField();
	TextArea goalOrg = new TextArea();
	ComboBox assistence = new ComboBox();
	ComboBox assistenceCategory1 = new ComboBox();
	ComboBox assistenceCategory2 = new ComboBox();
	ComboBox assistenceCategory3 = new ComboBox();
	TextField breaks = new TextField();
	ComboBox manageStructure = new ComboBox();
	ComboBox communicationStructure = new ComboBox();
	ComboBox regulationComputer1 = new ComboBox();
	ComboBox regulationComputer2 = new ComboBox();
	ComboBox regulationComputer3 = new ComboBox();
	TextArea orgGoals = new TextArea();
	ComboBox workMix1 = new ComboBox();
	ComboBox workMix2 = new ComboBox();
	ComboBox workMix3 = new ComboBox();
	ComboBox performaceMessure = new ComboBox();
	ComboBox performaceMessureCategory = new ComboBox();
	ComboBox resultResponseCat = new ComboBox();
	ComboBox resultResponsePeriode = new ComboBox();
	ComboBox workSpeed = new ComboBox();
	ComboBox autonomousWork = new ComboBox();
	ComboBox freedomChoice = new ComboBox();

	// mapping von Feldern auf Daten in Java (Pojos)
	private OrgEnviroment orgEnviromentBean = new OrgEnviroment();
	private FieldGroup orgEnviromentBinder = new FieldGroup();
	BeanItem<OrgEnviroment> orgEnviromentItem = new BeanItem<OrgEnviroment>(orgEnviromentBean);

	// SessionFactory factory = new
	// Configuration().configure().buildSessionFactory();

	public OrgEnviromentView() {
		getHeadline().setValue(KontextUIText.getLabel("orgCaption", "items1"));

		// ##workHours
		panelWorkHours = new Panel(KontextUIText.getLabel("workHours", "question"));
		// panel.setSizeUndefined();
		workHours = CreateComponent.createTextField("workHours", "items1");
		workHours.setInputPrompt(KontextUIText.getLabel("workHours", "items1"));
		layoutWorkHours.addComponent(workHours);
		// layout.setSizeUndefined();
		layoutWorkHours.setMargin(true);
		panelWorkHours.setContent(layoutWorkHours);
		addComponent(panelWorkHours);

		// ##groupwork
		panelGroupwork = new Panel(KontextUIText.getLabel("groupwork", "question"));
		// panel.setSizeUndefined();
		groupwork = CreateComponent.createComboBoxYN(KontextUIText.getLabel("groupwork", "items1"));
		groupworkSize = CreateComponent.createTextField("groupwork", "items2");
		groupworkSize.setInputPrompt(KontextUIText.getLabel("groupwork", "items2"));
		groupworkSize.addStyleName("hidden");
		groupwork.addValueChangeListener(new VisibilityChangeListener(groupwork, groupworkSize, null, null));
		layoutGroupwork.addComponent(groupwork);
		layoutGroupwork.addComponent(groupworkSize);
		// layout.setSizeUndefined();
		layoutGroupwork.setMargin(true);
		panelGroupwork.setContent(layoutGroupwork);
		addComponent(panelGroupwork);

		// // ##goalOrg
		panelGoalOrg = new Panel(KontextUIText.getLabel("goalOrg", "question"));
		// panel.setSizeUndefined();
		goalOrg.setInputPrompt(KontextUIText.getLabel("goalOrg", "inputPrompt"));
		layoutGoalOrg.addComponent(goalOrg);
		// layout.setSizeUndefined();
		layoutGoalOrg.setMargin(true);
		panelGoalOrg.setContent(layoutGoalOrg);
		addComponent(panelGoalOrg);

		// ##assistenceOrg
		panelAssistence = new Panel(KontextUIText.getLabel("assistenceOrg", "question"));
		// panel.setSizeUndefined();
		assistence = CreateComponent.createComboBoxYN(KontextUIText.getLabel("assistenceOrg", "items1"));
		assistenceCategory1 = CreateComponent.createComboBox("assistenceOrg", "items3", KontextUIText.getLabel("assistenceOrg", "items2"));
		assistenceCategory2 = CreateComponent.createComboBox("assistenceOrg", "items3", KontextUIText.getLabel("assistenceOrg", "items2"));
		assistenceCategory3 = CreateComponent.createComboBox("assistenceOrg", "items3", KontextUIText.getLabel("assistenceOrg", "items2"));
		assistenceCategory1.setInputPrompt(KontextUIText.getLabel("assistenceOrg", "items2"));
		assistenceCategory2.setInputPrompt(KontextUIText.getLabel("assistenceOrg", "items2"));
		assistenceCategory3.setInputPrompt(KontextUIText.getLabel("assistenceOrg", "items2"));
		assistenceCategory1.addStyleName("hidden");
		assistenceCategory2.addStyleName("hidden");
		assistenceCategory3.addStyleName("hidden");
		assistence.addValueChangeListener(new VisibilityChangeListener(assistence, assistenceCategory1, assistenceCategory2, assistenceCategory3));
		assistenceCategory1.addValueChangeListener(new VisibilityChangeListener(assistenceCategory1, assistenceCategory2, null, null));
		assistenceCategory2.addValueChangeListener(new VisibilityChangeListener(assistenceCategory2, assistenceCategory3, null, null));
		layoutAssistence.addComponent(assistence);
		layoutAssistence.addComponent(assistenceCategory1);
		layoutAssistence.addComponent(assistenceCategory2);
		layoutAssistence.addComponent(assistenceCategory3);
		// layout.setSizeUndefined();
		layoutAssistence.setMargin(true);
		panelAssistence.setContent(layoutAssistence);
		addComponent(panelAssistence);

		// ##breaks
		panelBreaks = new Panel(KontextUIText.getLabel("breaks", "question"));
		// panel.setSizeUndefined();
		breaks = CreateComponent.createTextField("breaks", "items1");
		breaks.setInputPrompt(KontextUIText.getLabel("breaks", "items1"));
		layoutBreaks.addComponent(breaks);
		// layout.setSizeUndefined();
		layoutBreaks.setMargin(true);
		panelBreaks.setContent(layoutBreaks);
		addComponent(panelBreaks);

		// ##manageStructure
		panelManageStructure = new Panel(KontextUIText.getLabel("manageStructure", "question"));
		// panel.setSizeUndefined();
		manageStructure = CreateComponent.createComboBox("manageStructure", "items2", KontextUIText.getLabel("manageStructure", "items1"));
		manageStructure.setInputPrompt(KontextUIText.getLabel("manageStructure", "items1"));
		layoutManageStructure.addComponent(manageStructure);
		// layout.setSizeUndefined();
		layoutManageStructure.setMargin(true);
		panelManageStructure.setContent(layoutManageStructure);
		addComponent(panelManageStructure);

		// ##communicationStructure
		panelCommunicationStructure = new Panel(KontextUIText.getLabel("communicationStructure", "question"));
		// panel.setSizeUndefined();
		communicationStructure = CreateComponent.createComboBox("communicationStructure", "items2", KontextUIText.getLabel("communicationStructure", "items1"));
		communicationStructure.setInputPrompt(KontextUIText.getLabel("communicationStructure", "items1"));
		layoutCommunicationStructure.addComponent(communicationStructure);
		// layout.setSizeUndefined();
		layoutCommunicationStructure.setMargin(true);
		panelCommunicationStructure.setContent(layoutCommunicationStructure);
		addComponent(panelCommunicationStructure);

		// ##regulationComputer
		panelRegulationComputer = new Panel(KontextUIText.getLabel("regulationComputer", "question"));
		// panel.setSizeUndefined();
		layoutRegulationComputer = new VerticalLayout();
		// layout.setSizeUndefined();
		layoutRegulationComputer.setMargin(true);
		regulationComputer1 = CreateComponent.createComboBox("regulationComputer", "items1", "Unternehmensvorschrift ");
		regulationComputer2 = CreateComponent.createComboBox("regulationComputer", "items1", "Unternehmensvorschrift ");
		regulationComputer3 = CreateComponent.createComboBox("regulationComputer", "items1", "Unternehmensvorschrift ");
		regulationComputer1.setInputPrompt(KontextUIText.getLabel("regulationComputer", "inputPrompt"));
		regulationComputer2.setInputPrompt(KontextUIText.getLabel("regulationComputer", "inputPrompt"));
		regulationComputer3.setInputPrompt(KontextUIText.getLabel("regulationComputer", "inputPrompt"));
		regulationComputer2.addStyleName("hidden");
		regulationComputer3.addStyleName("hidden");
		regulationComputer1.addValueChangeListener(new VisibilityChangeListener(regulationComputer1, regulationComputer2, null, null));
		regulationComputer2.addValueChangeListener(new VisibilityChangeListener(regulationComputer2, regulationComputer3, null, null));
		layoutRegulationComputer.addComponent(regulationComputer1);
		layoutRegulationComputer.addComponent(regulationComputer2);
		layoutRegulationComputer.addComponent(regulationComputer3);
		panelRegulationComputer.setContent(layoutRegulationComputer);
		addComponent(panelRegulationComputer);

		// ##orgGoals
		panelOrgGoals = new Panel(KontextUIText.getLabel("orgGoals", "question"));
		// panel.setSizeUndefined();
		orgGoals = CreateComponent.createTextArea("orgGoals", "items1");
		orgGoals.setInputPrompt(KontextUIText.getLabel("orgGoals", "items1"));
		layoutOrgGoals.addComponent(orgGoals);
		// layout.setSizeUndefined();
		layoutOrgGoals.setMargin(true);
		panelOrgGoals.setContent(layoutOrgGoals);
		addComponent(panelOrgGoals);

		// ##workMix
		panelWorkMix = new Panel(KontextUIText.getLabel("workMix", "question"));
		// panel.setSizeUndefined();
		workMix1 = CreateComponent.createComboBox("workMix", "items1", null);
		workMix2 = CreateComponent.createComboBox("workMix", "items1", null);
		workMix3 = CreateComponent.createComboBox("workMix", "items1", null);
		workMix1.setInputPrompt(KontextUIText.getLabel("workMix", "inputPrompt"));
		workMix2.setInputPrompt(KontextUIText.getLabel("workMix", "inputPrompt"));
		workMix3.setInputPrompt(KontextUIText.getLabel("workMix", "inputPrompt"));
		workMix2.addStyleName("hidden");
		workMix3.addStyleName("hidden");
		workMix1.addValueChangeListener(new VisibilityChangeListener(workMix1, workMix2, null, null));
		workMix2.addValueChangeListener(new VisibilityChangeListener(workMix2, workMix3, null, null));
		layoutWorkMix.addComponent(workMix1);
		layoutWorkMix.addComponent(workMix2);
		layoutWorkMix.addComponent(workMix3);
		// layout.setSizeUndefined();
		layoutWorkMix.setMargin(true);
		panelWorkMix.setContent(layoutWorkMix);
		addComponent(panelWorkMix);

		// ##performaceMessure
		panelPerformaceMessure = new Panel(KontextUIText.getLabel("performaceMessure", "question"));
		// panel.setSizeUndefined();
		performaceMessure = CreateComponent.createComboBoxYN(KontextUIText.getLabel("performaceMessure", "items1"));
		performaceMessureCategory = CreateComponent.createComboBox("performaceMessure", "items3", KontextUIText.getLabel("performaceMessure", "items2"));
		performaceMessureCategory.setInputPrompt(KontextUIText.getLabel("performaceMessure", "items3"));
		performaceMessureCategory.addStyleName("hidden");
		performaceMessure.addValueChangeListener(new VisibilityChangeListener(performaceMessure, performaceMessureCategory, null, null));
		layoutPerformaceMessure.addComponent(performaceMessure);
		layoutPerformaceMessure.addComponent(performaceMessureCategory);
		// layout.setSizeUndefined();
		layoutPerformaceMessure.setMargin(true);
		panelPerformaceMessure.setContent(layoutPerformaceMessure);
		addComponent(panelPerformaceMessure);

		// ##resultResponseCat
		panelResultResponse = new Panel(KontextUIText.getLabel("resultResponseCat", "question"));
		// panel.setSizeUndefined();
		resultResponseCat = CreateComponent.createComboBox("resultResponseCat", "items2", KontextUIText.getLabel("resultResponsePeriode", "items1"));
		resultResponsePeriode = CreateComponent.createComboBox("resultResponsePeriode", "items2", KontextUIText.getLabel("resultResponsePeriode", "items1"));
		resultResponseCat.setInputPrompt(KontextUIText.getLabel("resultResponseCat", "items1"));
		resultResponsePeriode.setInputPrompt(KontextUIText.getLabel("resultResponsePeriode", "items1"));
		layoutResultResponse.addComponent(resultResponseCat);
		layoutResultResponse.addComponent(resultResponsePeriode);
		// layout.setSizeUndefined();
		layoutResultResponse.setMargin(true);
		panelResultResponse.setContent(layoutResultResponse);
		addComponent(panelResultResponse);

		// ##workSpeed
		panelWorkSpeed = new Panel(KontextUIText.getLabel("workSpeed", "question"));
		// panel.setSizeUndefined();
		workSpeed = CreateComponent.createComboBox("workSpeed", "items2", KontextUIText.getLabel("workSpeed", "items1"));
		workSpeed.setInputPrompt(KontextUIText.getLabel("workSpeed", "items1"));
		layoutWorkSpeed.addComponent(workSpeed);
		// layout.setSizeUndefined();
		layoutWorkSpeed.setMargin(true);
		panelWorkSpeed.setContent(layoutWorkSpeed);
		addComponent(panelWorkSpeed);

		// ##autonomousWork
		panelAutonomousWork = new Panel(KontextUIText.getLabel("autonomousWork", "question"));
		// panel.setSizeUndefined();
		autonomousWork = CreateComponent.createComboBox("autonomousWork", "items1", KontextUIText.getLabel("autonomousWork", "inputPrompt"));
		autonomousWork.setInputPrompt(KontextUIText.getLabel("autonomousWork", "inputPrompt"));
		layoutAutonomousWork.addComponent(autonomousWork);
		// layout.setSizeUndefined();
		layoutAutonomousWork.setMargin(true);
		panelAutonomousWork.setContent(layoutAutonomousWork);
		addComponent(panelAutonomousWork);

		// ##freedomChoice
		panelFreedomChoice = new Panel(KontextUIText.getLabel("freedomChoice", "question"));
		// panel.setSizeUndefined();
		freedomChoice = CreateComponent.createComboBox("freedomChoice", "items1", KontextUIText.getLabel("freedomChoice", "inputPrompt"));
		freedomChoice.setInputPrompt(KontextUIText.getLabel("freedomChoice", "inputPrompt"));
		layoutFreedomChoice.addComponent(freedomChoice);
		// layout.setSizeUndefined();
		layoutFreedomChoice.setMargin(true);
		panelFreedomChoice.setContent(layoutFreedomChoice);
		addComponent(panelFreedomChoice);

		orgEnviromentBinder.setBuffered(false);
		orgEnviromentBinder.setItemDataSource(orgEnviromentItem);
		orgEnviromentBinder.bindMemberFields(this);

		workHours.addValueChangeListener(new BarChangeListener(workHours, components, this));
		groupwork.addValueChangeListener(new BarChangeListener(groupwork, components, this));
		groupworkSize.addValueChangeListener(new BarChangeListener(groupworkSize, components, this));
		goalOrg.addValueChangeListener(new BarChangeListener(goalOrg, components, this));
		assistence.addValueChangeListener(new BarChangeListener(assistence, components, this));
		assistenceCategory1.addValueChangeListener(new BarChangeListener(assistenceCategory1, components, this));
		assistenceCategory2.addValueChangeListener(new BarChangeListener(assistenceCategory2, components, this));
		assistenceCategory3.addValueChangeListener(new BarChangeListener(assistenceCategory3, components, this));
		breaks.addValueChangeListener(new BarChangeListener(breaks, components, this));
		manageStructure.addValueChangeListener(new BarChangeListener(manageStructure, components, this));
		communicationStructure.addValueChangeListener(new BarChangeListener(communicationStructure, components, this));
		regulationComputer1.addValueChangeListener(new BarChangeListener(regulationComputer1, components, this));
		regulationComputer2.addValueChangeListener(new BarChangeListener(regulationComputer2, components, this));
		regulationComputer3.addValueChangeListener(new BarChangeListener(regulationComputer3, components, this));
		orgGoals.addValueChangeListener(new BarChangeListener(orgGoals, components, this));
		workMix1.addValueChangeListener(new BarChangeListener(workMix1, components, this));
		workMix2.addValueChangeListener(new BarChangeListener(workMix2, components, this));
		workMix3.addValueChangeListener(new BarChangeListener(workMix3, components, this));
		performaceMessure.addValueChangeListener(new BarChangeListener(performaceMessure, components, this));
		performaceMessureCategory.addValueChangeListener(new BarChangeListener(performaceMessureCategory, components, this));
		resultResponseCat.addValueChangeListener(new BarChangeListener(resultResponseCat, components, this));
		resultResponsePeriode.addValueChangeListener(new BarChangeListener(resultResponsePeriode, components, this));
		workSpeed.addValueChangeListener(new BarChangeListener(workSpeed, components, this));
		autonomousWork.addValueChangeListener(new BarChangeListener(autonomousWork, components, this));
		freedomChoice.addValueChangeListener(new BarChangeListener(freedomChoice, components, this));

		panelWorkHours.addStyleName("panel");
		panelGroupwork.addStyleName("panel");
		panelGoalOrg.addStyleName("panel");
		panelAssistence.addStyleName("panel");
		panelBreaks.addStyleName("panel");
		panelManageStructure.addStyleName("panel");
		panelCommunicationStructure.addStyleName("panel");
		panelRegulationComputer.addStyleName("panel");
		panelOrgGoals.addStyleName("panel");
		panelWorkMix.addStyleName("panel");
		panelPerformaceMessure.addStyleName("panel");
		panelResultResponse.addStyleName("panel");
		panelWorkSpeed.addStyleName("panel");
		panelAutonomousWork.addStyleName("panel");
		panelFreedomChoice.addStyleName("panel");

		// wird nicht l�nger gebraucht, felder wernden sofort commited
		// addComponent(new Button("commitButton", new ClickListener() {
		// @Override
		// public void buttonClick(ClickEvent event) {
		// try {
		// userTypeBinder.commit();
		// } catch (CommitException e) {
		// e.printStackTrace();
		// }
		// System.out.println("commit erflogreich");
		// }
		// }));

		// addComponent(new Button("saveDB", new ClickListener() {
		// @Override
		// public void buttonClick(ClickEvent event) {
		// getUserTypeBean().setId(
		// // speichern in die DB und ersetzen des UserTypebean mit dem
		// // neuen peristieren Usertype
		// SQLUtil.save(getUserTypeBean(), factory));
		// }
		// }));
	}

	public int getComponents() {
		return components;
	}

	public OrgEnviroment getOrgEnviromentBean() {
		return orgEnviromentBean;
	}

	public void setOrgEnviromentBean(OrgEnviroment orgEnviromentBean) {
		this.orgEnviromentBean = orgEnviromentBean;
	}

	public FieldGroup getOrgEnviromentBinder() {
		return orgEnviromentBinder;
	}

	public void setOrgEnviromentBinder(FieldGroup orgEnviromentBinder) {
		this.orgEnviromentBinder = orgEnviromentBinder;
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub

	}

}
