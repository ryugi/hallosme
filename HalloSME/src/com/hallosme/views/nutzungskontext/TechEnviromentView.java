package com.hallosme.views.nutzungskontext;

import com.hallosme.components.listener.BarChangeListener;
import com.hallosme.components.listener.VisibilityChangeListener;
import com.hallosme.pojos.TechEnviroment;
import com.hallosme.utils.KontextUIText;
import com.hallosme.utils.CreateComponent;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class TechEnviromentView extends BasicKontextView implements View {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4008718709479446307L;
	final int components = 6;
	// Fragen Bl�cke
	Panel panelHardware = new Panel();
	Panel panelSoftware = new Panel();

	// Layouts f�r die Fragenbl�cke
	VerticalLayout layoutHardware = new VerticalLayout();
	VerticalLayout layoutSoftware = new VerticalLayout();

	// Verwendete Felder
	ComboBox hardware1 = new ComboBox();
	ComboBox hardware2 = new ComboBox();
	ComboBox hardware3 = new ComboBox();
	ComboBox software1 = new ComboBox();
	ComboBox software2 = new ComboBox();
	ComboBox software3 = new ComboBox();

	// mapping von Feldern auf Daten in Java (Pojos)
	private TechEnviroment techEnviromentBean = new TechEnviroment();
	private FieldGroup techEnviromentBinder = new FieldGroup();
	BeanItem<TechEnviroment> techEnviromentBeanItem = new BeanItem<TechEnviroment>(techEnviromentBean);

	// SessionFactory factory = new
	// Configuration().configure().buildSessionFactory();

	public TechEnviromentView() {
		getHeadline().setValue(KontextUIText.getLabel("techCaption", "items1"));

		// ##hardwarePhys
		panelHardware = new Panel(KontextUIText.getLabel("hardwarePhys", "question"));
		// panel.setSizeUndefined();
		hardware1 = CreateComponent.createComboBox("hardwarePhys", "items2", KontextUIText.getLabel("hardwarePhys", "items1"));
		hardware2 = CreateComponent.createComboBox("hardwarePhys", "items2", KontextUIText.getLabel("hardwarePhys", "items1"));
		hardware3 = CreateComponent.createComboBox("hardwarePhys", "items2", KontextUIText.getLabel("hardwarePhys", "items1"));
		hardware1.setInputPrompt(KontextUIText.getLabel("hardwarePhys", "items1"));
		hardware2.setInputPrompt(KontextUIText.getLabel("hardwarePhys", "items1"));
		hardware3.setInputPrompt(KontextUIText.getLabel("hardwarePhys", "items1"));
		hardware2.setVisible(false);
		hardware3.setVisible(false);
		hardware1.addValueChangeListener(new VisibilityChangeListener(hardware1, hardware2, hardware3, null));
		hardware2.addValueChangeListener(new VisibilityChangeListener(hardware2, hardware3, null, null));
		layoutHardware.addComponent(hardware1);
		layoutHardware.addComponent(hardware2);
		layoutHardware.addComponent(hardware3);
		// layout.setSizeUndefined();
		layoutHardware.setMargin(true);
		panelHardware.setContent(layoutHardware);
		addComponent(panelHardware);

		// ##softwarePhys
		panelSoftware = new Panel(KontextUIText.getLabel("softwarePhys", "question"));
		// panel.setSizeUndefined();
		software1 = CreateComponent.createComboBox("softwarePhys", "items2", KontextUIText.getLabel("softwarePhys", "items1"));
		software2 = CreateComponent.createComboBox("softwarePhys", "items2", KontextUIText.getLabel("softwarePhys", "items1"));
		software3 = CreateComponent.createComboBox("softwarePhys", "items2", KontextUIText.getLabel("softwarePhys", "items1"));
		software1.setInputPrompt(KontextUIText.getLabel("softwarePhys", "items1"));
		software2.setInputPrompt(KontextUIText.getLabel("softwarePhys", "items1"));
		software3.setInputPrompt(KontextUIText.getLabel("hardwarePhys", "items1"));
		software2.setVisible(false);
		software3.setVisible(false);
		software1.addValueChangeListener(new VisibilityChangeListener(software1, software2, software3, null));
		software2.addValueChangeListener(new VisibilityChangeListener(software2, software3, null, null));
		layoutSoftware.addComponent(software1);
		layoutSoftware.addComponent(software2);
		layoutSoftware.addComponent(software3);
		// layout.setSizeUndefined();
		layoutSoftware.setMargin(true);
		panelSoftware.setContent(layoutSoftware);
		addComponent(panelSoftware);

		techEnviromentBinder.setBuffered(false);
		techEnviromentBinder.setItemDataSource(techEnviromentBeanItem);
		techEnviromentBinder.bindMemberFields(this);

		// Fortschrittsbalken
		hardware1.addValueChangeListener(new BarChangeListener(hardware1, components, this));
		hardware2.addValueChangeListener(new BarChangeListener(hardware2, components, this));
		hardware3.addValueChangeListener(new BarChangeListener(hardware3, components, this));
		software1.addValueChangeListener(new BarChangeListener(software1, components, this));
		software2.addValueChangeListener(new BarChangeListener(software2, components, this));
		software3.addValueChangeListener(new BarChangeListener(software3, components, this));

		panelHardware.addStyleName("panel");
		panelSoftware.addStyleName("panel");

	}

	public int getComponents() {
		return components;
	}

	public TechEnviroment getTechEnviromentBean() {
		return techEnviromentBean;
	}

	public void setTechEnviromentBean(TechEnviroment techEnviromentBean) {
		this.techEnviromentBean = techEnviromentBean;
	}

	public FieldGroup getTechEnviromentBinder() {
		return techEnviromentBinder;
	}

	public void setTechEnviromentBinder(FieldGroup techEnviromentBinder) {
		this.techEnviromentBinder = techEnviromentBinder;
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO
	}

}
