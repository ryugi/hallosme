package com.hallosme.views.login;

import java.util.Arrays;

import org.vaadin.dialogs.ConfirmDialog;

import com.hallosme.components.listener.ValidationListener;
import com.hallosme.pojos.UserAccount;
import com.hallosme.utils.SQLUtil;
import com.hallosme.views.MainView;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;
import com.vaadin.server.Page;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class RegistryWindow extends Window {

	private static final long serialVersionUID = 1799127729671511169L;

	VerticalLayout layout = new VerticalLayout();
	ComboBox sex = new ComboBox("Anrede", Arrays.asList("Herr", "Frau"));
	TextField firstName = new TextField("Vorname");
	TextField lastName = new TextField("Nachname");
	TextField company = new TextField("Unternehmen");
	TextField street = new TextField("Stra�e");
	TextField zip = new TextField("Postleitzahl");
	TextField city = new TextField("Stadt");
	TextField country = new TextField("Land");
	TextField email = new TextField("Email");
	PasswordField accPassword = new PasswordField("Passwort");
	PasswordField passwordValidation = new PasswordField("Passwort best�tigen");
	Button createAccountButton = new Button("Account anlegen");
	Button cancleButton = new Button("Abbrechen");
	HorizontalLayout horizontalLayoutName = new HorizontalLayout();
	HorizontalLayout horizontalLayoutCity = new HorizontalLayout();
	HorizontalLayout horizontalLayoutPassword = new HorizontalLayout();
	HorizontalLayout horizontalLayoutButtons = new HorizontalLayout();

	private UserAccount userAccountBean = new UserAccount();
	final FieldGroup userAccountBinder = new FieldGroup();
	BeanItem<UserAccount> userAccountItem = new BeanItem<UserAccount>(userAccountBean);

	public RegistryWindow() {
		super();
		addStyleName("registryWindow");

		sex.setInputPrompt("Anrede");
		firstName.setInputPrompt("Vorname");
		lastName.setInputPrompt("Nachname");
		company.setInputPrompt("Unternehmen");
		street.setInputPrompt("Stra�e");
		zip.setInputPrompt("Postleitzahl");
		city.setInputPrompt("Stadt");
		country.setInputPrompt("Land");
		email.setInputPrompt("Email");
		accPassword.setInputPrompt("Passwort");
		passwordValidation.setInputPrompt("Passwort");

		sex.addBlurListener(new BlurListener() {

			private static final long serialVersionUID = 108125672667612749L;

			@Override
			public void blur(BlurEvent event) {
				sex.commit();
			}
		});
		firstName.addBlurListener(new BlurListener() {

			private static final long serialVersionUID = -2716445822599810098L;

			@Override
			public void blur(BlurEvent event) {
				firstName.commit();
			}
		});
		lastName.addBlurListener(new BlurListener() {

			private static final long serialVersionUID = 3057316449382450792L;

			@Override
			public void blur(BlurEvent event) {
				lastName.commit();
			}
		});
		company.addBlurListener(new BlurListener() {

			private static final long serialVersionUID = -3476586703756426895L;

			@Override
			public void blur(BlurEvent event) {
				company.commit();
			}
		});
		street.addBlurListener(new BlurListener() {

			private static final long serialVersionUID = 2283730320093321485L;

			@Override
			public void blur(BlurEvent event) {
				street.commit();
			}
		});
		zip.addBlurListener(new BlurListener() {

			private static final long serialVersionUID = -2980129999899315323L;

			@Override
			public void blur(BlurEvent event) {
				zip.commit();
			}
		});
		city.addBlurListener(new BlurListener() {

			private static final long serialVersionUID = -2844024902024369543L;

			@Override
			public void blur(BlurEvent event) {
				city.commit();
			}
		});
		country.addBlurListener(new BlurListener() {

			private static final long serialVersionUID = -2764030117687519231L;

			@Override
			public void blur(BlurEvent event) {
				country.commit();
			}
		});
		email.addBlurListener(new BlurListener() {

			private static final long serialVersionUID = -3099015603279964137L;

			@Override
			public void blur(BlurEvent event) {
				email.commit();
			}
		});
		accPassword.addBlurListener(new BlurListener() {

			private static final long serialVersionUID = -4891845815310678334L;

			@SuppressWarnings("deprecation")
			@Override
			public void blur(BlurEvent event) {
				accPassword.commit();
				if (!accPassword.getValue().equals("") && !passwordValidation.getValue().equals("")
						&& !accPassword.getValue().equals(passwordValidation.getValue())) {
					new Notification(null, "Passw�rter stimmen nicht �berein. Bitte �berpr�fen", Notification.TYPE_WARNING_MESSAGE, true)
							.show(Page.getCurrent());
				}
			}
		});
		passwordValidation.addBlurListener(new BlurListener() {

			private static final long serialVersionUID = 1085755708049547968L;

			@SuppressWarnings("deprecation")
			@Override
			public void blur(BlurEvent event) {
				passwordValidation.commit();
				if (!accPassword.getValue().equals("") && !passwordValidation.getValue().equals("")
						&& !accPassword.getValue().equals(passwordValidation.getValue())) {
					new Notification(null, "Passw�rter stimmen nicht �berein. Bitte �berpr�fen.", Notification.TYPE_WARNING_MESSAGE, true)
							.show(Page.getCurrent());
				}
			}
		});

		horizontalLayoutName.addComponent(firstName);
		horizontalLayoutName.addComponent(lastName);
		horizontalLayoutCity.addComponent(zip);
		horizontalLayoutCity.addComponent(city);
		horizontalLayoutPassword.addComponent(accPassword);
		horizontalLayoutPassword.addComponent(passwordValidation);

		horizontalLayoutButtons.addComponent(cancleButton);
		horizontalLayoutButtons.addComponent(createAccountButton);

		layout.addComponent(sex);
		layout.addComponent(horizontalLayoutName);
		layout.addComponent(company);
		layout.addComponent(street);
		layout.addComponent(horizontalLayoutCity);
		layout.addComponent(country);
		layout.addComponent(email);
		layout.addComponent(horizontalLayoutPassword);
		layout.addComponent(horizontalLayoutButtons);

		firstName.addValueChangeListener(new ValidationListener(firstName, "length"));
		lastName.addValueChangeListener(new ValidationListener(lastName, "length"));
		// street.addValueChangeListener(new ValidationListener(street, "length"));
		// zip.addValueChangeListener(new ValidationListener(zip, "length"));
		// city.addValueChangeListener(new ValidationListener(city, "length"));
		// country.addValueChangeListener(new ValidationListener(country, "length"));
		email.addValueChangeListener(new ValidationListener(email, "email"));
		// accPassword.addValueChangeListener(new ValidationListener(accPassword, "legth"));
		// passwordValidation.addValueChangeListener(new ValidationListener(passwordValidation,
		// "legth"));

		// Felder die �ber die volle L�nge gehen sollen
		company.addStyleName("full-length");
		street.addStyleName("full-length");
		email.addStyleName("full-length");

		horizontalLayoutButtons.addStyleName("float-right");

		createAccountButton.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 1625153151933503771L;

			@Override
			public void buttonClick(ClickEvent event) {
				if (firstName.getValue().isEmpty()) {
					firstName.addStyleName("invalidInput");
				}
				if (lastName.getValue().isEmpty()) {
					lastName.addStyleName("invalidInput");
				}
				// if (street.getValue().isEmpty()) {
				// street.addStyleName("invalidInput");
				// }
				// if (zip.getValue().isEmpty()) {
				// zip.addStyleName("invalidInput");
				// }
				// if (city.getValue().isEmpty()) {
				// city.addStyleName("invalidInput");
				// }
				// if (country.getValue().isEmpty()) {
				// country.addStyleName("invalidInput");
				// }
				if (email.getValue().isEmpty()) {
					email.addStyleName("invalidInput");
				}
				if (accPassword.getValue().isEmpty()) {
					accPassword.addStyleName("invalidInput");
				}
				if (passwordValidation.getValue().isEmpty()) {
					passwordValidation.addStyleName("invalidInput");
				}

				if (firstName.getStyleName().contains("invalidInput") || lastName.getStyleName().contains("invalidInput")
						|| street.getStyleName().contains("invalidInput") || zip.getStyleName().contains("invalidInput")
						|| city.getStyleName().contains("invalidInput") || country.getStyleName().contains("invalidInput")
						|| email.getStyleName().contains("invalidInput")) {

					ConfirmDialog.show(getUI(),
							"Bitte alle Rot makierten Felder �berpr�fen. INFO: Felder m�ssen mindestens 2 Zeichen lang sein und die Email muss \"text@host.de\" entsprechen",
							new ConfirmDialog.Listener() {

								private static final long serialVersionUID = 5560351889537451893L;

								@Override
								public void onClose(ConfirmDialog dialog) {
								}

							});
				} else if (accPassword.getValue().equals("") && passwordValidation.getValue().equals("")
						|| !accPassword.getValue().equals(passwordValidation.getValue())) {
					ConfirmDialog.show(getUI(),
							"Passwort nicht angegeben oder stimmt nicht mit der Passwortbest�tigung �berein. Bitte �berpr�fen und erneut versuchen",
							new ConfirmDialog.Listener() {

								private static final long serialVersionUID = 5560351889537451893L;

								@Override
								public void onClose(ConfirmDialog dialog) {
									accPassword.focus();
								}

							});
				} else if (SQLUtil.isEmailTaken(((MainView) UI.getCurrent()).getFactory(), userAccountBean.getEmail())) {
					ConfirmDialog.show(getUI(), "Die Email: " + userAccountBean.getEmail() + " wird bereits verwendet. bitte w�hlen sie eine andere.",
							new ConfirmDialog.Listener() {

								private static final long serialVersionUID = 5560351889537451893L;

								@Override
								public void onClose(ConfirmDialog dialog) {
									email.focus();
								}

							});
				} else {
					SQLUtil.save(((MainView) UI.getCurrent()).getFactory(), userAccountBean);
					new Notification(null, "Account erfolgreich angelegt.", Type.TRAY_NOTIFICATION, true).show(Page.getCurrent());
					close();
				}

			}
		});
		cancleButton.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 3205976675647223342L;

			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});
		userAccountBinder.setItemDataSource(userAccountItem);
		userAccountBinder.bindMemberFields(this);

		setDraggable(false);
		setModal(true);
		setResizable(false);
		center();
		setContent(layout);

	}

	public UserAccount getUserAccountBean() {
		return userAccountBean;
	}

	public void setUserAccountBean(UserAccount userAccountBean) {
		this.userAccountBean = userAccountBean;
	}

}
