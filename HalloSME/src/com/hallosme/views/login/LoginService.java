package com.hallosme.views.login;

import java.io.File;

import org.vaadin.teemu.VaadinIcons;

import com.hallosme.components.deleteKontextButton;
import com.hallosme.pojos.Nutzungskontext;
import com.hallosme.pojos.OrgEnviroment;
import com.hallosme.pojos.PhysEnviroment;
import com.hallosme.pojos.TechEnviroment;
import com.hallosme.pojos.UserAccount;
import com.hallosme.pojos.UserType;
import com.hallosme.pojos.WorkTask;
import com.hallosme.pojos.WorkTool;
import com.hallosme.utils.pdf.GeneratePDFButton;
import com.hallosme.views.MainView;
import com.hallosme.views.nutzungskontext.OrgEnviromentView;
import com.hallosme.views.nutzungskontext.PhysEnviromentView;
import com.hallosme.views.nutzungskontext.TechEnviromentView;
import com.hallosme.views.nutzungskontext.UserTypeView;
import com.hallosme.views.nutzungskontext.WorkTaskView;
import com.hallosme.views.nutzungskontext.WorkToolView;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.FileResource;
import com.vaadin.server.Resource;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

public class LoginService {

	/**
	 * Generiert das Accordion zur Navigation zwischen den einzelnen Nutzungskontexten
	 * 
	 * @param account
	 *            Der einzulogende Benutzer f�r den das Navigation accordion erstellt werden soll
	 */
	public static void createAccordion(UserAccount account) {
		Navigator navigator = ((MainView) UI.getCurrent()).getNavigator();

		for (Nutzungskontext kontext : account.getNutzungkontext()) {
			// List<Component> componets = new ArrayList<Component>();
			VerticalLayout accordionTab = new VerticalLayout();
			String name = kontext.getName();
			OrgEnviromentView orgEnviromentView = new OrgEnviromentView();
			TechEnviromentView techEnviromentView = new TechEnviromentView();
			PhysEnviromentView physEnviromentView = new PhysEnviromentView();
			UserTypeView userTypeView = new UserTypeView();
			WorkTaskView workTaskView = new WorkTaskView();
			WorkToolView workToolView = new WorkToolView();

			accordionTab.addStyleName("accordionTab");

			userTypeView.getUserTypeBinder().discard();
			// usertypeBar
			HorizontalLayout userBarLayout = new HorizontalLayout();
			userBarLayout.addStyleName("navigationPoint");
			final ProgressBar userBar = userTypeView.getBar();
			userBar.setCaption("Benutzer Typ");
			userBar.setWidth("298");
			userBarLayout.addComponent(userBar);
			userTypeView.getEditedComponentsLabel().setValue(userTypeView.getEditedComponents() + " / " + userTypeView.getComponents());
			userBarLayout.addComponent(userTypeView.getEditedComponentsLabel());
			// clicklistener auf das layout
			userBarLayout.addLayoutClickListener(new LayoutClickListener() {
				private static final long serialVersionUID = -1822685632006055695L;

				@Override
				public void layoutClick(LayoutClickEvent event) {
					navigator.navigateTo("userType" + name.replace(" ", ""));
				}
			});
			accordionTab.addComponent(userBarLayout);

			// worktaskBar
			HorizontalLayout taskBarLayout = new HorizontalLayout();
			taskBarLayout.addStyleName("navigationPoint");
			final ProgressBar taskBar = workTaskView.getBar();
			taskBar.setCaption("Arbeitsaufgabe");
			taskBar.setWidth("298");
			taskBarLayout.addComponent(taskBar);
			workTaskView.getEditedComponentsLabel().setValue(workTaskView.getEditedComponents() + " / " + workTaskView.getComponents());
			taskBarLayout.addComponent(workTaskView.getEditedComponentsLabel());
			// clicklistener auf das layout
			taskBarLayout.addLayoutClickListener(new LayoutClickListener() {
				private static final long serialVersionUID = -4393097727718305495L;

				@Override
				public void layoutClick(LayoutClickEvent event) {
					navigator.navigateTo("workTask" + name.replace(" ", ""));
				}
			});
			accordionTab.addComponent(taskBarLayout);

			// worktoolBar
			HorizontalLayout toolBarLayout = new HorizontalLayout();
			toolBarLayout.addStyleName("navigationPoint");
			final ProgressBar toolBar = workToolView.getBar();
			toolBar.setCaption("Arbeitsmittel");
			toolBar.setWidth("298");
			toolBarLayout.addComponent(toolBar);
			workToolView.getEditedComponentsLabel().setValue(workToolView.getEditedComponents() + " / " + workToolView.getComponents());
			toolBarLayout.addComponent(workToolView.getEditedComponentsLabel());
			// clicklistener auf das layout
			toolBarLayout.addLayoutClickListener(new LayoutClickListener() {
				private static final long serialVersionUID = 5575291592424591580L;

				@Override
				public void layoutClick(LayoutClickEvent event) {
					navigator.navigateTo("workTool" + name.replace(" ", ""));
				}
			});
			accordionTab.addComponent(toolBarLayout);

			// orgEnviBar
			HorizontalLayout orgEnviBarLayout = new HorizontalLayout();
			orgEnviBarLayout.addStyleName("navigationPoint");
			final ProgressBar orgEnviBar = orgEnviromentView.getBar();
			orgEnviBar.setCaption("Organisatorische Umgebung");
			orgEnviBar.setWidth("298");
			orgEnviBarLayout.addComponent(orgEnviBar);
			orgEnviromentView.getEditedComponentsLabel()
					.setValue(orgEnviromentView.getEditedComponents() + " / " + orgEnviromentView.getComponents());
			orgEnviBarLayout.addComponent(orgEnviromentView.getEditedComponentsLabel());
			// clicklistener auf das layout
			orgEnviBarLayout.addLayoutClickListener(new LayoutClickListener() {
				private static final long serialVersionUID = -2667100714141794351L;

				@Override
				public void layoutClick(LayoutClickEvent event) {
					navigator.navigateTo("orgEnvi" + name.replace(" ", ""));
				}
			});
			accordionTab.addComponent(orgEnviBarLayout);

			// techEnviBar
			HorizontalLayout techEnviBarLayout = new HorizontalLayout();
			techEnviBarLayout.addStyleName("navigationPoint");
			final ProgressBar techEnviBar = techEnviromentView.getBar();
			techEnviBar.setCaption("Technische Umgebung");
			techEnviBar.setWidth("298");
			techEnviBarLayout.addComponent(techEnviBar);
			techEnviromentView.getEditedComponentsLabel()
					.setValue(techEnviromentView.getEditedComponents() + " / " + techEnviromentView.getComponents());
			techEnviBarLayout.addComponent(techEnviromentView.getEditedComponentsLabel());
			// clicklistener auf das layout
			techEnviBarLayout.addLayoutClickListener(new LayoutClickListener() {
				private static final long serialVersionUID = 3868714600621368915L;

				@Override
				public void layoutClick(LayoutClickEvent event) {
					navigator.navigateTo("techEnvi" + name.replace(" ", ""));
				}
			});
			accordionTab.addComponent(techEnviBarLayout);

			// physEnviBar
			HorizontalLayout physEnviBarLayout = new HorizontalLayout();
			physEnviBarLayout.addStyleName("navigationPoint");
			final ProgressBar physEnviBar = physEnviromentView.getBar();
			physEnviBar.setCaption("Physische Umgebung");
			physEnviBar.setWidth("298");
			physEnviBarLayout.addComponent(physEnviBar);
			physEnviromentView.getEditedComponentsLabel()
					.setValue(physEnviromentView.getEditedComponents() + " / " + physEnviromentView.getComponents());
			physEnviBarLayout.addComponent(physEnviromentView.getEditedComponentsLabel());
			// clicklistener auf das layout
			physEnviBarLayout.addLayoutClickListener(new LayoutClickListener() {
				private static final long serialVersionUID = -3424745415468643571L;

				@Override
				public void layoutClick(LayoutClickEvent event) {
					navigator.navigateTo("physEnvi" + name.replace(" ", ""));
				}
			});
			accordionTab.addComponent(physEnviBarLayout);

			accordionTab.addComponent(new deleteKontextButton(accordionTab, name));

			/*
			 * Hier wird die PDF Generiert. Dazu werden 2 Buttons erstellt "generatePDFButton" ist
			 * in der Navigation sichtbar, downloadInvisibleButton wird mittles css siehe
			 * "Stylename" ausgebledet. Zudem wird nach dem physischen klick des Nutzers auf den
			 * "generatePDFButton" am Ende mittels Javascript ein neuer ButtonBlick auf den
			 * "downloadInvisibleButton" welcher die generierte PDF zum Download anbietet.
			 */
			String basepath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();
			Button generatePDFButton = new GeneratePDFButton().generateDownloadPDFButton(kontext);
			generatePDFButton.setIcon(VaadinIcons.FILE_TEXT_O);
			final Button downloadInvisibleButton = new Button();
			downloadInvisibleButton.setId("DownloadButtonId");
			downloadInvisibleButton.addStyleName("InvisibleButton");
			Resource res = new FileResource(new File(basepath + "/" + kontext.getName().replace(" ", "_") + ".pdf"));
			FileDownloader fd = new FileDownloader(res);
			fd.extend(downloadInvisibleButton);
			accordionTab.addComponent(generatePDFButton);
			accordionTab.addComponent(downloadInvisibleButton);

			navigator.addView("userType" + name.replace(" ", ""), userTypeView);
			navigator.addView("workTask" + name.replace(" ", ""), workTaskView);
			navigator.addView("workTool" + name.replace(" ", ""), workToolView);
			navigator.addView("orgEnvi" + name.replace(" ", ""), orgEnviromentView);
			navigator.addView("physEnvi" + name.replace(" ", ""), physEnviromentView);
			navigator.addView("techEnvi" + name.replace(" ", ""), techEnviromentView);

			// componets.add(orgEnviromentView);
			// componets.add(techEnviromentView);
			// componets.add(physEnviromentView);
			// componets.add(userTypeView);
			// componets.add(workTaskView);
			// componets.add(workToolView);

			// setzen des Session attributes auf init = true damit der barChangeListener wei�,
			// dass
			// er die Changes an dieser Stelle ignorien soll und nichts weiter unternimmt
			UI.getCurrent().getSession().setAttribute("init", new Boolean(true));
			// �bernehmen der Werte aus der Datenbank in die einzelenen Views
			loadUserType(userTypeView, kontext.getUserType());
			loadWorkTask(workTaskView, kontext.getWorkTask());
			loadWorkTool(workToolView, kontext.getWorkTool());
			loadOrgEnviroment(orgEnviromentView, kontext.getOrgEnviroment());
			loadPhysEnviroment(physEnviromentView, kontext.getPhysEnviroment());
			loadTechEnviroment(techEnviromentView, kontext.getTechEnviroment());

			// Damit der barChangeListener wei�, dass er alle seine Events richtig abarbeiten
			// kann
			UI.getCurrent().getSession().setAttribute("init", new Boolean(false));

			kontext.setUserType(userTypeView.getUserTypeBean());
			kontext.setWorkTask(workTaskView.getWorkTaskBean());
			kontext.setWorkTool(workToolView.getWorkToolBean());
			kontext.setOrgEnviroment(orgEnviromentView.getOrgEnviromentBean());
			kontext.setPhysEnviroment(physEnviromentView.getPhysEnviromentBean());
			kontext.setTechEnviroment(techEnviromentView.getTechEnviromentBean());

			// ((MainView) UI.getCurrent()).getKontextMap().put("Kontext" +
			// name, componets);
			((MainView) UI.getCurrent()).getAccordion().addTab(accordionTab, name, null);
		}
		navigator.navigateTo("userType" + account.getNutzungkontext().iterator().next().getName().replace(" ", ""));
		System.out.println("accordion fertig");
		// return accordion;
	}

	@SuppressWarnings("unchecked")
	public static void loadUserType(UserTypeView userTypeView, UserType userType) {
		BeanItem<UserType> beanItem = (BeanItem<UserType>) userTypeView.getUserTypeBinder().getItemDataSource();
		beanItem.getItemProperty("userType").setValue(userType.getUserType());
		beanItem.getItemProperty("expMobAss").setValue(userType.getExpMobAss());
		beanItem.getItemProperty("expWorkTask").setValue(userType.getExpWorkTask());
		beanItem.getItemProperty("expOrg").setValue(userType.getExpOrg());
		beanItem.getItemProperty("uebungsgrad").setValue(userType.getUebungsgrad());
		beanItem.getItemProperty("mouseUsed").setValue(userType.getMouseUsed());
		beanItem.getItemProperty("skillMouse").setValue(userType.getSkillMouse());
		beanItem.getItemProperty("keyboardUsed").setValue(userType.getKeyboardUsed());
		beanItem.getItemProperty("skillKeyboard").setValue(userType.getSkillKeyboard());
		beanItem.getItemProperty("touchscreenUsed").setValue(userType.getTouchscreenUsed());
		beanItem.getItemProperty("skillTouchscreen").setValue(userType.getSkillTouchscreen());
		beanItem.getItemProperty("quallificationField1").setValue(userType.getQuallificationField1());
		beanItem.getItemProperty("quallificationFieldStake1").setValue(userType.getQuallificationFieldStake1());
		beanItem.getItemProperty("quallificationField2").setValue(userType.getQuallificationField2());
		beanItem.getItemProperty("quallificationFieldStake2").setValue(userType.getQuallificationFieldStake2());
		beanItem.getItemProperty("quallificationField3").setValue(userType.getQuallificationField3());
		beanItem.getItemProperty("quallificationFieldStake3").setValue(userType.getQuallificationFieldStake3());
		beanItem.getItemProperty("language1").setValue(userType.getLanguage1());
		beanItem.getItemProperty("languageLevel1").setValue(userType.getLanguageLevel1());
		beanItem.getItemProperty("language2").setValue(userType.getLanguage2());
		beanItem.getItemProperty("languageLevel2").setValue(userType.getLanguageLevel2());
		beanItem.getItemProperty("language3").setValue(userType.getLanguage3());
		beanItem.getItemProperty("languageLevel3").setValue(userType.getLanguageLevel3());
		beanItem.getItemProperty("generalKnowledge").setValue(userType.getGeneralKnowledge());
		beanItem.getItemProperty("age").setValue(userType.getAge());
		beanItem.getItemProperty("sex").setValue(userType.getSex());
		beanItem.getItemProperty("mentalSkill").setValue(userType.getMentalSkill());
		beanItem.getItemProperty("attitude").setValue(userType.getAttitude());
		beanItem.getItemProperty("id").setValue(userType.getId());
		System.out.println("INIT USERTYPEBEAN READY");
	}

	@SuppressWarnings("unchecked")
	public static void loadWorkTask(WorkTaskView workTaskView, WorkTask workTask) {
		BeanItem<WorkTask> beanItem = (BeanItem<WorkTask>) workTaskView.getWorkTaskBinder().getItemDataSource();
		beanItem.getItemProperty("taskCategory1").setValue(workTask.getTaskCategory1());
		beanItem.getItemProperty("taskCategory2").setValue(workTask.getTaskCategory2());
		beanItem.getItemProperty("taskCategory3").setValue(workTask.getTaskCategory3());
		beanItem.getItemProperty("workTaskName").setValue(workTask.getWorkTaskName());
		beanItem.getItemProperty("taskFreqValue").setValue(workTask.getTaskFreqValue());
		beanItem.getItemProperty("taskFreqEntity").setValue(workTask.getTaskFreqEntity());
		beanItem.getItemProperty("taskDuration").setValue(workTask.getTaskDuration());
		beanItem.getItemProperty("taskDurationEntity").setValue(workTask.getTaskDurationEntity());
		beanItem.getItemProperty("incident").setValue(workTask.getIncident());
		beanItem.getItemProperty("incidentDuration").setValue(workTask.getIncidentDuration());
		beanItem.getItemProperty("incidentFreq").setValue(workTask.getIncidentFreq());
		beanItem.getItemProperty("scoupe").setValue(workTask.getScoupe());
		beanItem.getItemProperty("processRatio").setValue(workTask.getProcessRatio());
		beanItem.getItemProperty("physMentalReq").setValue(workTask.getPhysMentalReq());
		beanItem.getItemProperty("taskDependency").setValue(workTask.getTaskDependency());
		beanItem.getItemProperty("taskResult").setValue(workTask.getTaskResult());
		beanItem.getItemProperty("taskErrorImpact").setValue(workTask.getTaskErrorImpact());
		beanItem.getItemProperty("taskDanger").setValue(workTask.getTaskDanger());
		beanItem.getItemProperty("id").setValue(workTask.getId());
		System.out.println("INIT WORKTASK READY");
	}

	@SuppressWarnings("unchecked")
	public static void loadWorkTool(WorkToolView workToolView, WorkTool workTool) {
		BeanItem<WorkTool> beanItem = (BeanItem<WorkTool>) workToolView.getWorkToolBinder().getItemDataSource();
		beanItem.getItemProperty("productName").setValue(workTool.getProductName());
		beanItem.getItemProperty("productScoupe1").setValue(workTool.getProductScoupe1());
		beanItem.getItemProperty("productScoupe2").setValue(workTool.getProductScoupe2());
		beanItem.getItemProperty("productScoupe3").setValue(workTool.getProductScoupe3());
		beanItem.getItemProperty("importantFunctions").setValue(workTool.getImportantFunctions());
		beanItem.getItemProperty("hardwareScreenSize").setValue(workTool.getHardwareScreenSize());
		beanItem.getItemProperty("touchscreenNeed").setValue(workTool.isTouchscreenNeed());
		beanItem.getItemProperty("mouseNeed").setValue(workTool.isMouseNeed());
		beanItem.getItemProperty("keyboardNeed").setValue(workTool.isKeyboardNeed());
		beanItem.getItemProperty("softwareCategory").setValue(workTool.getSoftwareCategory());
		beanItem.getItemProperty("extServiceNeed").setValue(workTool.getExtServiceNeed());
		beanItem.getItemProperty("extServiceCategorys").setValue(workTool.getExtServiceCategorys());
		beanItem.getItemProperty("misc").setValue(workTool.getMisc());
		beanItem.getItemProperty("id").setValue(workTool.getId());
		System.out.println("INIT WORKTOOL READY");
	}

	@SuppressWarnings("unchecked")
	public static void loadOrgEnviroment(OrgEnviromentView orgEnviromentView, OrgEnviroment orgEnviroment) {
		BeanItem<OrgEnviroment> beanItem = (BeanItem<OrgEnviroment>) orgEnviromentView.getOrgEnviromentBinder().getItemDataSource();
		beanItem.getItemProperty("workHours").setValue(orgEnviroment.getWorkHours());
		beanItem.getItemProperty("groupwork").setValue(orgEnviroment.getGroupwork());
		beanItem.getItemProperty("groupworkSize").setValue(orgEnviroment.getGroupworkSize());
		beanItem.getItemProperty("goalOrg").setValue(orgEnviroment.getGoalOrg());
		beanItem.getItemProperty("assistence").setValue(orgEnviroment.getAssistence());
		beanItem.getItemProperty("assistenceCategory1").setValue(orgEnviroment.getAssistenceCategory1());
		beanItem.getItemProperty("assistenceCategory2").setValue(orgEnviroment.getAssistenceCategory2());
		beanItem.getItemProperty("assistenceCategory3").setValue(orgEnviroment.getAssistenceCategory3());
		beanItem.getItemProperty("breaks").setValue(orgEnviroment.getBreaks());
		beanItem.getItemProperty("manageStructure").setValue(orgEnviroment.getManageStructure());
		beanItem.getItemProperty("communicationStructure").setValue(orgEnviroment.getCommunicationStructure());
		beanItem.getItemProperty("regulationComputer1").setValue(orgEnviroment.getRegulationComputer1());
		beanItem.getItemProperty("regulationComputer2").setValue(orgEnviroment.getRegulationComputer2());
		beanItem.getItemProperty("regulationComputer3").setValue(orgEnviroment.getRegulationComputer3());
		beanItem.getItemProperty("orgGoals").setValue(orgEnviroment.getOrgGoals());
		beanItem.getItemProperty("workMix1").setValue(orgEnviroment.getWorkMix1());
		beanItem.getItemProperty("workMix2").setValue(orgEnviroment.getWorkMix2());
		beanItem.getItemProperty("workMix3").setValue(orgEnviroment.getWorkMix3());
		beanItem.getItemProperty("performaceMessure").setValue(orgEnviroment.getPerformaceMessure());
		beanItem.getItemProperty("performaceMessureCategory").setValue(orgEnviroment.getPerformaceMessureCategory());
		beanItem.getItemProperty("resultResponseCat").setValue(orgEnviroment.getResultResponseCat());
		beanItem.getItemProperty("resultResponsePeriode").setValue(orgEnviroment.getResultResponsePeriode());
		beanItem.getItemProperty("workSpeed").setValue(orgEnviroment.getWorkSpeed());
		beanItem.getItemProperty("autonomousWork").setValue(orgEnviroment.getAutonomousWork());
		beanItem.getItemProperty("freedomChoice").setValue(orgEnviroment.getFreedomChoice());
		beanItem.getItemProperty("id").setValue(orgEnviroment.getId());
		System.out.println("INIT ORGENVI READY");
	}

	@SuppressWarnings("unchecked")
	public static void loadPhysEnviroment(PhysEnviromentView physEnviromentView, PhysEnviroment physEnviroment) {
		BeanItem<PhysEnviroment> beanItem = (BeanItem<PhysEnviroment>) physEnviromentView.getPhysEnviromentBinder().getItemDataSource();
		beanItem.getItemProperty("atmosConditionAir").setValue(physEnviroment.getAtmosConditionAir());
		beanItem.getItemProperty("atmosConditionSpeed").setValue(physEnviroment.getAtmosConditionSpeed());
		beanItem.getItemProperty("atmosConditionHumidity").setValue(physEnviroment.getAtmosConditionHumidity());
		beanItem.getItemProperty("acousticCondition").setValue(physEnviroment.getAcousticCondition());
		beanItem.getItemProperty("thermalConditionLow").setValue(physEnviroment.getThermalConditionLow());
		beanItem.getItemProperty("thermalConditionHigh").setValue(physEnviroment.getThermalConditionHigh());
		beanItem.getItemProperty("lightConditionLow").setValue(physEnviroment.getLightConditionLow());
		beanItem.getItemProperty("lightConditionHigh").setValue(physEnviroment.getLightConditionHigh());
		beanItem.getItemProperty("enviStability").setValue(physEnviroment.getEnviStability());
		beanItem.getItemProperty("workspaceSize").setValue(physEnviroment.getWorkspaceSize());
		beanItem.getItemProperty("workspaceInv").setValue(physEnviroment.getWorkspaceInv());
		beanItem.getItemProperty("postureStaying").setValue(physEnviroment.getPostureStaying());
		beanItem.getItemProperty("postureSitting").setValue(physEnviroment.getPostureSitting());
		beanItem.getItemProperty("postureLying").setValue(physEnviroment.getPostureLying());
		beanItem.getItemProperty("postureKneeling").setValue(physEnviroment.getPostureKneeling());
		beanItem.getItemProperty("workspaceMoveSpace").setValue(physEnviroment.getWorkspaceMoveSpace());
		beanItem.getItemProperty("workspaceAtHand").setValue(physEnviroment.getWorkspaceAtHand());
		beanItem.getItemProperty("healtHezard").setValue(physEnviroment.getHealtHezard());
		beanItem.getItemProperty("healtHezardCategory").setValue(physEnviroment.getHealtHezardCategory());
		beanItem.getItemProperty("safty").setValue(physEnviroment.getSafty());
		beanItem.getItemProperty("saftyLimitation").setValue(physEnviroment.getSaftyLimitation());
		beanItem.getItemProperty("id").setValue(physEnviroment.getId());
		System.out.println("INIT PHYSENVI READY");

	}

	@SuppressWarnings("unchecked")
	public static void loadTechEnviroment(TechEnviromentView techEnviromentView, TechEnviroment techEnviroment) {
		BeanItem<TechEnviroment> beanItem = (BeanItem<TechEnviroment>) techEnviromentView.getTechEnviromentBinder().getItemDataSource();
		beanItem.getItemProperty("hardware1").setValue(techEnviroment.getHardware1());
		beanItem.getItemProperty("hardware2").setValue(techEnviroment.getHardware2());
		beanItem.getItemProperty("hardware3").setValue(techEnviroment.getHardware3());
		beanItem.getItemProperty("software1").setValue(techEnviroment.getSoftware1());
		beanItem.getItemProperty("software2").setValue(techEnviroment.getSoftware2());
		beanItem.getItemProperty("software3").setValue(techEnviroment.getSoftware3());
		beanItem.getItemProperty("id").setValue(techEnviroment.getId());
		System.out.println("INIT TECHENVI READY");
	}

}
