package com.hallosme.views.login;

import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.teemu.VaadinIcons;

import com.hallosme.components.CreateKontextWindow;
import com.hallosme.pojos.UserAccount;
import com.hallosme.utils.SQLUtil;
import com.hallosme.views.MainView;
import com.vaadin.server.Page;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

/**
 * @author Waldemar Schiffner
 *
 *         Login Fenster
 *
 */
@SuppressWarnings("serial")
public class LoginWindow extends Window {

	VerticalLayout layout;
	TextField emailField = new TextField("Email");
	PasswordField acc_passwordField = new PasswordField("Password");
	Button loginButton = new Button("Login");
	Button registerButton = new Button("Register");
	UserAccount account;

	public LoginWindow() {
		loginButton.setIcon(VaadinIcons.SIGN_IN);
		registerButton.setIcon(VaadinIcons.USER_CARD);
		setCaption("Login/Regestrieren");
		addStyleName("loginWindow");
		layout = new VerticalLayout();
		emailField = new TextField("Email");
		emailField.setInputPrompt("Email");
		acc_passwordField = new PasswordField("Password");
		acc_passwordField.setInputPrompt("Password");
		loginButton = new Button("Login");
		registerButton = new Button("Regestrieren");
		layout.addComponent(emailField);
		layout.addComponent(acc_passwordField);
		HorizontalLayout buttonsLayout = new HorizontalLayout();

		buttonsLayout.addStyleName("float-right");
		buttonsLayout.addComponent(registerButton);
		buttonsLayout.addComponent(loginButton);
		layout.addComponent(buttonsLayout);
		loginButton.addClickListener(new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				// pr�fe ob die eingegebenen Werte stimmen und ob daf�r ein Account existiert.
				if (!emailField.getValue().toString().isEmpty() && !acc_passwordField.getValue().toString().isEmpty()) {

					account = SQLUtil.login(((MainView) UI.getCurrent()).getFactory(), emailField.getValue(), acc_passwordField.getValue());
					if (account == null) {
						ConfirmDialog.show(getUI(), "Email und Passwort stimmen nicht �berein.", new ConfirmDialog.Listener() {

							private static final long serialVersionUID = 5560351889537451893L;

							@Override
							public void onClose(ConfirmDialog dialog) {
								emailField.focus();
							}

						});
					} else {
						getSession().setAttribute("user", account);

					}
				} else {
					new Notification(null, "Bitte gib zuerst Email und Passwort ein um dich einzuloggen.", Type.WARNING_MESSAGE, true)
							.show(Page.getCurrent());
				}
				if (account != null && !account.getNutzungkontext().isEmpty()) {
					// ((MainView)
					// UI.getCurrent()).setAccordion(LoginService.createAccordion(account.getNutzungkontext()));
					// ((MainView)
					// UI.getCurrent()).getHorizontalLayout().addComponent(LoginService.createAccordion(account.getNutzungkontext()));
					if (((MainView) UI.getCurrent()).getAccordion().getTab(0) != null) {
						ConfirmDialog.show(getUI(), "Achtung", "Alle bisherigen Eingaben gehen verloren. Wollen sie wirklich fortfahren?",
								"Fortfahren", "Abbrechen", new ConfirmDialog.Listener() {

									public void onClose(ConfirmDialog dialog) {
										if (dialog.isConfirmed()) {
											// Confirmed to continue
											((MainView) UI.getCurrent()).getAccordion().removeAllComponents();
											LoginService.createAccordion(account);
											cleanView();
											close();
										} else {
											// User did not confirm
											// feedback(dialog.isConfirmed());
										}
									}
								});

					} else {
						LoginService.createAccordion(account);
						cleanView();
						close();
					}
				} else if (account != null) {
					((MainView) UI.getCurrent()).addWindow(new CreateKontextWindow());
					cleanView();
					close();
				}
			}
		});

		registerButton.addClickListener(new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				close();
				UI.getCurrent().addWindow(new RegistryWindow());
			}
		});
		setContent(layout);
		setDraggable(false);
		setModal(true);
		setResizable(false);
		emailField.focus();
		center();
	}

	/**
	 * setzte den LoginButton unsichtbar, den SaveButton und das Accordion sichtbar
	 */
	public void cleanView() {
		((MainView) getUI()).getSaveButton().setVisible(true);
		((MainView) getUI()).getLoginButton().setVisible(false);
		((MainView) getUI()).getCreateKontextButton().setVisible(true);
		((MainView) getUI()).getAccordion().setVisible(true);

		// for (Nutzungskontext kontext : kontexte) {
		// String name = kontext.getName();
		// ((MainView) getUI()).getNavigator().removeView("userType" +
		// name.replace(" ", ""));
		// ((MainView) getUI()).getNavigator().removeView("workTask" +
		// name.replace(" ", ""));
		// ((MainView) getUI()).getNavigator().removeView("workTool" +
		// name.replace(" ", ""));
		// ((MainView) getUI()).getNavigator().removeView("orgEnvi" +
		// name.replace(" ", ""));
		// ((MainView) getUI()).getNavigator().removeView("techEnvi" +
		// name.replace(" ", ""));
		// ((MainView) getUI()).getNavigator().removeView("physEnvi" +
		// name.replace(" ", ""));
		// }
	}

}
