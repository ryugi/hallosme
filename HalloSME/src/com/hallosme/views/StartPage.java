package com.hallosme.views;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class StartPage extends VerticalLayout implements View {

	private static final long serialVersionUID = -2693085705585185421L;

	String willkommen = "Willkommen";
	String text1 = "Bei dieser Anwendung handelt es sich um eine Applikation zur Erfassung eines Nutzungskonetxt f�r Anwendungssysteme. Der Nutzungskontext spiegelt den Rahmenbereich wieder in dem die Anwendung verwendet wird";
	String text2 = "Sie k�nnen hier ihre eigenen Nutzungskontexte f�r verschiedene Applikationen erfassen, speichern und sich diese als PDF Dokument runterladen.";
	// String text3 = "";
	// String text4 = "";
	// String text5 = "";
	// String text6 = "";
	// String text7 = "";
	// String text8 = "";
	// String text9 = "";
	// String text10 = "";
	// String text11 = "";
	// String text12 = "";
	// String text13 = "";
	// String text14 = "";
	// String text15 = "";

	Label text1Label;
	Label text2Label;
	Label text3Label;
	// Label text4Label;
	// Label text5Label;
	// Label text6Label;
	// Label text7Label;
	// Label text8Label;
	// Label text9Label;
	// Label text10Label;
	// Label text11Label;
	// Label text12Label;
	// Label text13Label;
	// Label text14Label;
	// Label text15Label;

	public StartPage() {
		super();
		setWidth("700px");
		text1Label = new Label(willkommen);
		text2Label = new Label(text1);
		text3Label = new Label(text2);

		addComponent(text1Label);
		addComponent(text2Label);
		addComponent(text3Label);

		text1Label.addStyleName("startLabel");
		text1Label.addStyleName("startLabelHeadline");
		text2Label.addStyleName("startLabel");
		text3Label.addStyleName("startLabel");
		// text4Label.addStyleName("startLabel");
		// text5Label.addStyleName("startLabel");
		// text6Label.addStyleName("startLabel");
		// text7Label.addStyleName("startLabel");
		// text8Label.addStyleName("startLabel");
		// text9Label.addStyleName("startLabel");
		// text10Label.addStyleName("startLabel");
		// text11Label.addStyleName("startLabel");
		// text12Label.addStyleName("startLabel");
		// text13Label.addStyleName("startLabel");
		// text14Label.addStyleName("startLabel");
		// text15Label.addStyleName("startLabel");

	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub

	}

}
