package com.hallosme.pojos;

import java.io.Serializable;

public class PhysEnviroment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6814098127063269635L;
	private int id;
	private int atmosConditionAir;
	private int atmosConditionSpeed;
	private int atmosConditionHumidity;
	private String acousticCondition;
	private int thermalConditionLow;
	private int thermalConditionHigh;
	private int lightConditionLow;
	private int lightConditionHigh;
	private String enviStability;
	private int workspaceSize;
	private String workspaceInv = "";
	private int postureStaying;
	private int postureSitting;
	private int postureLying;
	private int postureKneeling;
	private int workspaceMoveSpace;
	private int workspaceAtHand;
	private String healtHezard;
	private String healtHezardCategory;
	private String safty;
	private String saftyLimitation;

	public PhysEnviroment() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PhysEnviroment(int atmosConditionAir, int atmosConditionSpeed, int atmosConditionHumidity, String acousticCondition,
			int thermalConditionLow, int thermalConditionHigh, int lightConditionLow, int lightConditionHigh, String enviStability, int workspaceSize,
			String workspaceInv, int postureStaying, int postureSitting, int postureLying, int postureKneeling, int workspaceMoveSpace,
			int workspaceAtHand, String healtHezard, String healtHezardCategory, String safty, String saftyLimitation) {
		super();
		this.atmosConditionAir = atmosConditionAir;
		this.atmosConditionSpeed = atmosConditionSpeed;
		this.atmosConditionHumidity = atmosConditionHumidity;
		this.acousticCondition = acousticCondition;
		this.thermalConditionLow = thermalConditionLow;
		this.thermalConditionHigh = thermalConditionHigh;
		this.lightConditionLow = lightConditionLow;
		this.lightConditionHigh = lightConditionHigh;
		this.enviStability = enviStability;
		this.workspaceSize = workspaceSize;
		this.workspaceInv = workspaceInv;
		this.postureStaying = postureStaying;
		this.postureSitting = postureSitting;
		this.postureLying = postureLying;
		this.postureKneeling = postureKneeling;
		this.workspaceMoveSpace = workspaceMoveSpace;
		this.workspaceAtHand = workspaceAtHand;
		this.healtHezard = healtHezard;
		this.healtHezardCategory = healtHezardCategory;
		this.safty = safty;
		this.saftyLimitation = saftyLimitation;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAtmosConditionAir() {
		return atmosConditionAir;
	}

	public void setAtmosConditionAir(int atmosConditionAir) {
		this.atmosConditionAir = atmosConditionAir;
	}

	public int getAtmosConditionSpeed() {
		return atmosConditionSpeed;
	}

	public void setAtmosConditionSpeed(int atmosConditionSpeed) {
		this.atmosConditionSpeed = atmosConditionSpeed;
	}

	public int getAtmosConditionHumidity() {
		return atmosConditionHumidity;
	}

	public void setAtmosConditionHumidity(int atmosConditionHumidity) {
		this.atmosConditionHumidity = atmosConditionHumidity;
	}

	public String getAcousticCondition() {
		return acousticCondition;
	}

	public void setAcousticCondition(String acousticCondition) {
		this.acousticCondition = acousticCondition;
	}

	public int getThermalConditionLow() {
		return thermalConditionLow;
	}

	public void setThermalConditionLow(int thermalConditionLow) {
		this.thermalConditionLow = thermalConditionLow;
	}

	public int getThermalConditionHigh() {
		return thermalConditionHigh;
	}

	public void setThermalConditionHigh(int thermalConditionHigh) {
		this.thermalConditionHigh = thermalConditionHigh;
	}

	public int getLightConditionLow() {
		return lightConditionLow;
	}

	public void setLightConditionLow(int lightConditionLow) {
		this.lightConditionLow = lightConditionLow;
	}

	public int getLightConditionHigh() {
		return lightConditionHigh;
	}

	public void setLightConditionHigh(int lightConditionHigh) {
		this.lightConditionHigh = lightConditionHigh;
	}

	public String getEnviStability() {
		return enviStability;
	}

	public void setEnviStability(String enviStability) {
		this.enviStability = enviStability;
	}

	public int getWorkspaceSize() {
		return workspaceSize;
	}

	public void setWorkspaceSize(int workspaceSize) {
		this.workspaceSize = workspaceSize;
	}

	public String getWorkspaceInv() {
		return workspaceInv;
	}

	public void setWorkspaceInv(String workspaceInv) {
		this.workspaceInv = workspaceInv;
	}

	public int getPostureStaying() {
		return postureStaying;
	}

	public void setPostureStaying(int postureStaying) {
		this.postureStaying = postureStaying;
	}

	public int getPostureSitting() {
		return postureSitting;
	}

	public void setPostureSitting(int postureSitting) {
		this.postureSitting = postureSitting;
	}

	public int getPostureLying() {
		return postureLying;
	}

	public void setPostureLying(int postureLying) {
		this.postureLying = postureLying;
	}

	public int getPostureKneeling() {
		return postureKneeling;
	}

	public void setPostureKneeling(int postureKneeling) {
		this.postureKneeling = postureKneeling;
	}

	public int getWorkspaceMoveSpace() {
		return workspaceMoveSpace;
	}

	public void setWorkspaceMoveSpace(int workspaceMoveSpace) {
		this.workspaceMoveSpace = workspaceMoveSpace;
	}

	public int getWorkspaceAtHand() {
		return workspaceAtHand;
	}

	public void setWorkspaceAtHand(int workspaceAtHand) {
		this.workspaceAtHand = workspaceAtHand;
	}

	public String getHealtHezard() {
		return healtHezard;
	}

	public void setHealtHezard(String healtHezard) {
		this.healtHezard = healtHezard;
	}

	public String getHealtHezardCategory() {
		return healtHezardCategory;
	}

	public void setHealtHezardCategory(String healtHezardCategory) {
		this.healtHezardCategory = healtHezardCategory;
	}

	public String getSafty() {
		return safty;
	}

	public void setSafty(String safty) {
		this.safty = safty;
	}

	public String getSaftyLimitation() {
		return saftyLimitation;
	}

	public void setSaftyLimitation(String saftyLimitation) {
		this.saftyLimitation = saftyLimitation;
	}

}
