package com.hallosme.pojos;

import java.io.Serializable;

public class OrgEnviroment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8330679322694441571L;
	private int id;
	private int workHours;
	private String groupwork;
	private int groupworkSize;
	private String goalOrg = "";
	private String assistence;
	private String assistenceCategory1;
	private String assistenceCategory2;
	private String assistenceCategory3;
	private int breaks;
	private String manageStructure;
	private String communicationStructure;
	private String regulationComputer1;
	private String regulationComputer2;
	private String regulationComputer3;
	private String orgGoals = "";
	private String workMix1;
	private String workMix2;
	private String workMix3;
	private String performaceMessure;
	private String performaceMessureCategory;
	private String resultResponseCat;
	private String resultResponsePeriode;
	private String workSpeed;
	private String autonomousWork;
	private String freedomChoice;

	public OrgEnviroment() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OrgEnviroment(int workHours, String groupwork, int groupworkSize, String goalOrg, String assistence, String assistenceCategory1,
			String assistenceCategory2, String assistenceCategory3, int breaks, String manageStructure, String communicationStructure,
			String regulationComputer1, String regulationComputer2, String regulationComputer3, String orgGoals, String workMix1, String workMix2,
			String workMix3, String performaceMessure, String performaceMessureCategory, String resultResponseCat, String resultResponsePeriode,
			String workSpeed, String autonomousWork, String freedomChoice) {
		super();
		this.workHours = workHours;
		this.groupwork = groupwork;
		this.groupworkSize = groupworkSize;
		this.goalOrg = goalOrg;
		this.assistence = assistence;
		this.assistenceCategory1 = assistenceCategory1;
		this.assistenceCategory2 = assistenceCategory2;
		this.assistenceCategory3 = assistenceCategory3;
		this.breaks = breaks;
		this.manageStructure = manageStructure;
		this.communicationStructure = communicationStructure;
		this.regulationComputer1 = regulationComputer1;
		this.regulationComputer2 = regulationComputer2;
		this.regulationComputer3 = regulationComputer3;
		this.orgGoals = orgGoals;
		this.workMix1 = workMix1;
		this.workMix2 = workMix2;
		this.workMix3 = workMix3;
		this.performaceMessure = performaceMessure;
		this.performaceMessureCategory = performaceMessureCategory;
		this.resultResponseCat = resultResponseCat;
		this.resultResponsePeriode = resultResponsePeriode;
		this.workSpeed = workSpeed;
		this.autonomousWork = autonomousWork;
		this.freedomChoice = freedomChoice;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getWorkHours() {
		return workHours;
	}

	public void setWorkHours(int workHours) {
		this.workHours = workHours;
	}

	public String getGroupwork() {
		return groupwork;
	}

	public void setGroupwork(String groupwork) {
		this.groupwork = groupwork;
	}

	public int getGroupworkSize() {
		return groupworkSize;
	}

	public void setGroupworkSize(int groupworkSize) {
		this.groupworkSize = groupworkSize;
	}

	public String getGoalOrg() {
		return goalOrg;
	}

	public void setGoalOrg(String goalOrg) {
		this.goalOrg = goalOrg;
	}

	public String getAssistence() {
		return assistence;
	}

	public void setAssistence(String assistence) {
		this.assistence = assistence;
	}

	public String getAssistenceCategory1() {
		return assistenceCategory1;
	}

	public void setAssistenceCategory1(String assistenceCategory1) {
		this.assistenceCategory1 = assistenceCategory1;
	}

	public String getAssistenceCategory2() {
		return assistenceCategory2;
	}

	public void setAssistenceCategory2(String assistenceCategory2) {
		this.assistenceCategory2 = assistenceCategory2;
	}

	public String getAssistenceCategory3() {
		return assistenceCategory3;
	}

	public void setAssistenceCategory3(String assistenceCategory3) {
		this.assistenceCategory3 = assistenceCategory3;
	}

	public int getBreaks() {
		return breaks;
	}

	public void setBreaks(int breaks) {
		this.breaks = breaks;
	}

	public String getManageStructure() {
		return manageStructure;
	}

	public void setManageStructure(String manageStructure) {
		this.manageStructure = manageStructure;
	}

	public String getCommunicationStructure() {
		return communicationStructure;
	}

	public void setCommunicationStructure(String communicationStructure) {
		this.communicationStructure = communicationStructure;
	}

	public String getRegulationComputer1() {
		return regulationComputer1;
	}

	public void setRegulationComputer1(String regulationComputer1) {
		this.regulationComputer1 = regulationComputer1;
	}

	public String getRegulationComputer2() {
		return regulationComputer2;
	}

	public void setRegulationComputer2(String regulationComputer2) {
		this.regulationComputer2 = regulationComputer2;
	}

	public String getRegulationComputer3() {
		return regulationComputer3;
	}

	public void setRegulationComputer3(String regulationComputer3) {
		this.regulationComputer3 = regulationComputer3;
	}

	public String getOrgGoals() {
		return orgGoals;
	}

	public void setOrgGoals(String orgGoals) {
		this.orgGoals = orgGoals;
	}

	public String getWorkMix1() {
		return workMix1;
	}

	public void setWorkMix1(String workMix1) {
		this.workMix1 = workMix1;
	}

	public String getWorkMix2() {
		return workMix2;
	}

	public void setWorkMix2(String workMix2) {
		this.workMix2 = workMix2;
	}

	public String getWorkMix3() {
		return workMix3;
	}

	public void setWorkMix3(String workMix3) {
		this.workMix3 = workMix3;
	}

	public String getPerformaceMessure() {
		return performaceMessure;
	}

	public void setPerformaceMessure(String performaceMessure) {
		this.performaceMessure = performaceMessure;
	}

	public String getPerformaceMessureCategory() {
		return performaceMessureCategory;
	}

	public void setPerformaceMessureCategory(String performaceMessureCategory) {
		this.performaceMessureCategory = performaceMessureCategory;
	}

	public String getResultResponseCat() {
		return resultResponseCat;
	}

	public void setResultResponseCat(String resultResponseCat) {
		this.resultResponseCat = resultResponseCat;
	}

	public String getResultResponsePeriode() {
		return resultResponsePeriode;
	}

	public void setResultResponsePeriode(String resultResponsePeriode) {
		this.resultResponsePeriode = resultResponsePeriode;
	}

	public String getWorkSpeed() {
		return workSpeed;
	}

	public void setWorkSpeed(String workSpeed) {
		this.workSpeed = workSpeed;
	}

	public String getAutonomousWork() {
		return autonomousWork;
	}

	public void setAutonomousWork(String autonomousWork) {
		this.autonomousWork = autonomousWork;
	}

	public String getFreedomChoice() {
		return freedomChoice;
	}

	public void setFreedomChoice(String freedomChoice) {
		this.freedomChoice = freedomChoice;
	}

}
