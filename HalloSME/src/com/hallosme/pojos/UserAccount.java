package com.hallosme.pojos;

import java.io.Serializable;
import java.util.Set;

public class UserAccount implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5612642778182333879L;
	private int id;
	private String email = "";
	private String accPassword = "";
	private String sex = "";
	private String firstName = "";
	private String lastName = "";
	private String street = "";
	private int zip;
	private String city = "";
	private String country = "";
	private String company = "";
	private Set<Nutzungskontext> nutzungkontext;

	public UserAccount() {
	}

	public UserAccount(String email, String accPassword, String sex, String firstName, String lastName, String street, int zip, String city,
			String country, String company, Set<Nutzungskontext> nutzungkontext) {
		super();
		this.email = email;
		this.accPassword = accPassword;
		this.sex = sex;
		this.firstName = firstName;
		this.lastName = lastName;
		this.street = street;
		this.zip = zip;
		this.city = city;
		this.country = country;
		this.company = company;
		this.nutzungkontext = nutzungkontext;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAccPassword() {
		return accPassword;
	}

	public void setAccPassword(String accPassword) {
		this.accPassword = accPassword;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public int getZip() {
		return zip;
	}

	public void setZip(int zip) {
		this.zip = zip;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public Set<Nutzungskontext> getNutzungkontext() {
		return nutzungkontext;
	}

	public void setNutzungkontext(Set<Nutzungskontext> nutzungkontext) {
		this.nutzungkontext = nutzungkontext;
	}

}
