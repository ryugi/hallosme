package com.hallosme.pojos;

import java.io.Serializable;

public class WorkTask implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6681241977988857092L;
	private int id;
	private String taskCategory1;
	private String taskCategory2;
	private String taskCategory3;
	private String workTaskName = "";
	private int taskFreqValue;
	private String taskFreqEntity;
	private int taskDuration;
	private String taskDurationEntity;
	// JA/NEIN
	private String incident;
	private int incidentDuration;
	private int incidentFreq;
	// JA/NEIN
	private String scoupe;
	private int processRatio;
	private String physMentalReq = "";
	private String taskDependency = "";
	private String taskResult = "";
	private String taskErrorImpact = "";
	private String taskDanger;

	public WorkTask() {
		super();
		// TODO Auto-generated constructor stub
	}

	public WorkTask(String taskCategory1, String taskCategory2, String taskCategory3, String workTaskName, int taskFreqValue, String taskFreqEntity,
			int taskDuration, String taskDurationEntity, String incident, int incidentDuration, int incidentFreq, String scoupe, int processRatio,
			String physMentalReq, String taskDependency, String taskResult, String taskErrorImpact, String taskDanger) {
		super();
		this.taskCategory1 = taskCategory1;
		this.taskCategory2 = taskCategory2;
		this.taskCategory3 = taskCategory3;
		this.workTaskName = workTaskName;
		this.taskFreqValue = taskFreqValue;
		this.taskFreqEntity = taskFreqEntity;
		this.taskDuration = taskDuration;
		this.taskDurationEntity = taskDurationEntity;
		this.incident = incident;
		this.incidentDuration = incidentDuration;
		this.incidentFreq = incidentFreq;
		this.scoupe = scoupe;
		this.processRatio = processRatio;
		this.physMentalReq = physMentalReq;
		this.taskDependency = taskDependency;
		this.taskResult = taskResult;
		this.taskErrorImpact = taskErrorImpact;
		this.taskDanger = taskDanger;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTaskCategory1() {
		return taskCategory1;
	}

	public void setTaskCategory1(String taskCategory1) {
		this.taskCategory1 = taskCategory1;
	}

	public String getTaskCategory2() {
		return taskCategory2;
	}

	public void setTaskCategory2(String taskCategory2) {
		this.taskCategory2 = taskCategory2;
	}

	public String getTaskCategory3() {
		return taskCategory3;
	}

	public void setTaskCategory3(String taskCategory3) {
		this.taskCategory3 = taskCategory3;
	}

	public String getWorkTaskName() {
		return workTaskName;
	}

	public void setWorkTaskName(String workTaskName) {
		this.workTaskName = workTaskName;
	}

	public int getTaskFreqValue() {
		return taskFreqValue;
	}

	public void setTaskFreqValue(int taskFreqValue) {
		this.taskFreqValue = taskFreqValue;
	}

	public String getTaskFreqEntity() {
		return taskFreqEntity;
	}

	public void setTaskFreqEntity(String taskFreqEntity) {
		this.taskFreqEntity = taskFreqEntity;
	}

	public int getTaskDuration() {
		return taskDuration;
	}

	public void setTaskDuration(int taskDuration) {
		this.taskDuration = taskDuration;
	}

	public String getTaskDurationEntity() {
		return taskDurationEntity;
	}

	public void setTaskDurationEntity(String taskDurationEntity) {
		this.taskDurationEntity = taskDurationEntity;
	}

	public String getIncident() {
		return incident;
	}

	public void setIncident(String incident) {
		this.incident = incident;
	}

	public int getIncidentDuration() {
		return incidentDuration;
	}

	public void setIncidentDuration(int incidentDuration) {
		this.incidentDuration = incidentDuration;
	}

	public int getIncidentFreq() {
		return incidentFreq;
	}

	public void setIncidentFreq(int incidentFreq) {
		this.incidentFreq = incidentFreq;
	}

	public String getScoupe() {
		return scoupe;
	}

	public void setScoupe(String scoupe) {
		this.scoupe = scoupe;
	}

	public int getProcessRatio() {
		return processRatio;
	}

	public void setProcessRatio(int processRatio) {
		this.processRatio = processRatio;
	}

	public String getPhysMentalReq() {
		return physMentalReq;
	}

	public void setPhysMentalReq(String physMentalReq) {
		this.physMentalReq = physMentalReq;
	}

	public String getTaskDependency() {
		return taskDependency;
	}

	public void setTaskDependency(String taskDependency) {
		this.taskDependency = taskDependency;
	}

	public String getTaskResult() {
		return taskResult;
	}

	public void setTaskResult(String taskResult) {
		this.taskResult = taskResult;
	}

	public String getTaskErrorImpact() {
		return taskErrorImpact;
	}

	public void setTaskErrorImpact(String taskErrorImpact) {
		this.taskErrorImpact = taskErrorImpact;
	}

	public String getTaskDanger() {
		return taskDanger;
	}

	public void setTaskDanger(String taskDanger) {
		this.taskDanger = taskDanger;
	}

}
