package com.hallosme.pojos;

import java.io.Serializable;

public class UserType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5640106738441073246L;
	private int id;
	private String userType;
	private int expMobAss;
	private int expWorkTask;
	private int expOrg;
	private String uebungsgrad;
	private boolean mouseUsed;
	private String skillMouse;
	private boolean keyboardUsed;
	private int skillKeyboard;
	private boolean touchscreenUsed;
	private String skillTouchscreen;
	private String quallificationField1;
	private int quallificationFieldStake1;
	private String quallificationField2;
	private int quallificationFieldStake2;
	private String quallificationField3;
	private int quallificationFieldStake3;
	private String language1;
	private String languageLevel1;
	private String language2;
	private String languageLevel2;
	private String language3;
	private String languageLevel3;
	private String generalKnowledge = "";
	private String age;
	private String sex;
	private String mentalSkill;
	private String attitude = "";

	public UserType() {
	}

	public UserType(String userType, int expMobAss, int expWorkTask, int expOrg, String uebungsgrad, boolean mouseUsed, String skillMouse,
			boolean keyboardUsed, int skillKeyboard, boolean touchscreenUsed, String skillTouchscreen, String quallificationField1,
			int quallificationFieldStake1, String quallificationField2, int quallificationFieldStake2, String quallificationField3,
			int quallificationFieldStake3, String language1, String languageLevel1, String language2, String languageLevel2, String language3,
			String languageLevel3, String generalKnowledge, String age, String sex, String mentalSkill, String attitude) {
		super();
		this.userType = userType;
		this.expMobAss = expMobAss;
		this.expWorkTask = expWorkTask;
		this.expOrg = expOrg;
		this.uebungsgrad = uebungsgrad;
		this.mouseUsed = mouseUsed;
		this.skillMouse = skillMouse;
		this.keyboardUsed = keyboardUsed;
		this.skillKeyboard = skillKeyboard;
		this.touchscreenUsed = touchscreenUsed;
		this.skillTouchscreen = skillTouchscreen;
		this.quallificationField1 = quallificationField1;
		this.quallificationFieldStake1 = quallificationFieldStake1;
		this.quallificationField2 = quallificationField2;
		this.quallificationFieldStake2 = quallificationFieldStake2;
		this.quallificationField3 = quallificationField3;
		this.quallificationFieldStake3 = quallificationFieldStake3;
		this.language1 = language1;
		this.languageLevel1 = languageLevel1;
		this.language2 = language2;
		this.languageLevel2 = languageLevel2;
		this.language3 = language3;
		this.languageLevel3 = languageLevel3;
		this.generalKnowledge = generalKnowledge;
		this.age = age;
		this.sex = sex;
		this.mentalSkill = mentalSkill;
		this.attitude = attitude;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getExpMobAss() {
		return expMobAss;
	}

	public void setExpMobAss(int expMobAss) {
		this.expMobAss = expMobAss;
	}

	public int getExpWorkTask() {
		return expWorkTask;
	}

	public void setExpWorkTask(int expWorkTask) {
		this.expWorkTask = expWorkTask;
	}

	public int getExpOrg() {
		return expOrg;
	}

	public void setExpOrg(int expOrg) {
		this.expOrg = expOrg;
	}

	public String getUebungsgrad() {
		return uebungsgrad;
	}

	public void setUebungsgrad(String uebungsgrad) {
		this.uebungsgrad = uebungsgrad;
	}

	public boolean getMouseUsed() {
		return mouseUsed;
	}

	public void setMouseUsed(boolean mouseUsed) {
		this.mouseUsed = mouseUsed;
	}

	public String getSkillMouse() {
		return skillMouse;
	}

	public void setSkillMouse(String skillMouse) {
		this.skillMouse = skillMouse;
	}

	public boolean getKeyboardUsed() {
		return keyboardUsed;
	}

	public void setKeyboardUsed(boolean keyboardUsed) {
		this.keyboardUsed = keyboardUsed;
	}

	public int getSkillKeyboard() {
		return skillKeyboard;
	}

	public void setSkillKeyboard(int skillKeyboard) {
		this.skillKeyboard = skillKeyboard;
	}

	public boolean getTouchscreenUsed() {
		return touchscreenUsed;
	}

	public void setTouchscreenUsed(boolean touchscreenUsed) {
		this.touchscreenUsed = touchscreenUsed;
	}

	public String getSkillTouchscreen() {
		return skillTouchscreen;
	}

	public void setSkillTouchscreen(String skillTouchscreen) {
		this.skillTouchscreen = skillTouchscreen;
	}

	public String getQuallificationField1() {
		return quallificationField1;
	}

	public void setQuallificationField1(String quallificationField1) {
		this.quallificationField1 = quallificationField1;
	}

	public int getQuallificationFieldStake1() {
		return quallificationFieldStake1;
	}

	public void setQuallificationFieldStake1(int quallificationFieldStake1) {
		this.quallificationFieldStake1 = quallificationFieldStake1;
	}

	public String getQuallificationField2() {
		return quallificationField2;
	}

	public void setQuallificationField2(String quallificationField2) {
		this.quallificationField2 = quallificationField2;
	}

	public int getQuallificationFieldStake2() {
		return quallificationFieldStake2;
	}

	public void setQuallificationFieldStake2(int quallificationFieldStake2) {
		this.quallificationFieldStake2 = quallificationFieldStake2;
	}

	public String getQuallificationField3() {
		return quallificationField3;
	}

	public void setQuallificationField3(String quallificationField3) {
		this.quallificationField3 = quallificationField3;
	}

	public int getQuallificationFieldStake3() {
		return quallificationFieldStake3;
	}

	public void setQuallificationFieldStake3(int quallificationFieldStake3) {
		this.quallificationFieldStake3 = quallificationFieldStake3;
	}

	public String getLanguage1() {
		return language1;
	}

	public void setLanguage1(String language1) {
		this.language1 = language1;
	}

	public String getLanguageLevel1() {
		return languageLevel1;
	}

	public void setLanguageLevel1(String languageLevel1) {
		this.languageLevel1 = languageLevel1;
	}

	public String getLanguage2() {
		return language2;
	}

	public void setLanguage2(String language2) {
		this.language2 = language2;
	}

	public String getLanguageLevel2() {
		return languageLevel2;
	}

	public void setLanguageLevel2(String languageLevel2) {
		this.languageLevel2 = languageLevel2;
	}

	public String getLanguage3() {
		return language3;
	}

	public void setLanguage3(String language3) {
		this.language3 = language3;
	}

	public String getLanguageLevel3() {
		return languageLevel3;
	}

	public void setLanguageLevel3(String languageLevel3) {
		this.languageLevel3 = languageLevel3;
	}

	public String getGeneralKnowledge() {
		return generalKnowledge;
	}

	public void setGeneralKnowledge(String generalKnowledge) {
		this.generalKnowledge = generalKnowledge;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getMentalSkill() {
		return mentalSkill;
	}

	public void setMentalSkill(String mentalSkill) {
		this.mentalSkill = mentalSkill;
	}

	public String getAttitude() {
		return attitude;
	}

	public void setAttitude(String attitude) {
		this.attitude = attitude;
	}

}
