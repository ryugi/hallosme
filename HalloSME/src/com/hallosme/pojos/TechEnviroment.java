package com.hallosme.pojos;

import java.io.Serializable;

public class TechEnviroment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1376596226221006780L;
	private int id;
	private String hardware1;
	private String hardware2;
	private String hardware3;
	private String software1;
	private String software2;
	private String software3;

	public TechEnviroment() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TechEnviroment(String hardware1, String hardware2, String hardware3, String software1, String software2, String software3) {
		super();
		this.hardware1 = hardware1;
		this.hardware2 = hardware2;
		this.hardware3 = hardware3;
		this.software1 = software1;
		this.software2 = software2;
		this.software3 = software3;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHardware1() {
		return hardware1;
	}

	public void setHardware1(String hardware1) {
		this.hardware1 = hardware1;
	}

	public String getHardware2() {
		return hardware2;
	}

	public void setHardware2(String hardware2) {
		this.hardware2 = hardware2;
	}

	public String getHardware3() {
		return hardware3;
	}

	public void setHardware3(String hardware3) {
		this.hardware3 = hardware3;
	}

	public String getSoftware1() {
		return software1;
	}

	public void setSoftware1(String software1) {
		this.software1 = software1;
	}

	public String getSoftware2() {
		return software2;
	}

	public void setSoftware2(String software2) {
		this.software2 = software2;
	}

	public String getSoftware3() {
		return software3;
	}

	public void setSoftware3(String software3) {
		this.software3 = software3;
	}

}
