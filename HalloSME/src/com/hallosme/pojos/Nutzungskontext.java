package com.hallosme.pojos;

import java.io.Serializable;

public class Nutzungskontext implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4549558906524847715L;
	private int id;
	private String name;
	// private UserAccount userAccount;
	private UserType userType;
	private WorkTask workTask;
	private WorkTool workTool;
	private OrgEnviroment orgEnviroment;
	private TechEnviroment techEnviroment;
	private PhysEnviroment physEnviroment;

	public Nutzungskontext() {
	}

	public Nutzungskontext(String name, UserType usertype, WorkTask worktask, WorkTool workTool, OrgEnviroment orgEnviroment,
			TechEnviroment techEnviroment, PhysEnviroment physEnviroment) {
		super();
		this.name = name;
		this.userType = usertype;
		this.workTask = worktask;
		this.workTool = workTool;
		this.orgEnviroment = orgEnviroment;
		this.techEnviroment = techEnviroment;
		this.physEnviroment = physEnviroment;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType usertype) {
		this.userType = usertype;
	}

	public WorkTask getWorkTask() {
		return workTask;
	}

	public void setWorkTask(WorkTask worktask) {
		this.workTask = worktask;
	}

	public WorkTool getWorkTool() {
		return workTool;
	}

	public void setWorkTool(WorkTool workTool) {
		this.workTool = workTool;
	}

	public OrgEnviroment getOrgEnviroment() {
		return orgEnviroment;
	}

	public void setOrgEnviroment(OrgEnviroment orgEnviroment) {
		this.orgEnviroment = orgEnviroment;
	}

	public TechEnviroment getTechEnviroment() {
		return techEnviroment;
	}

	public void setTechEnviroment(TechEnviroment techEnviroment) {
		this.techEnviroment = techEnviroment;
	}

	public PhysEnviroment getPhysEnviroment() {
		return physEnviroment;
	}

	public void setPhysEnviroment(PhysEnviroment physEnviroment) {
		this.physEnviroment = physEnviroment;
	}

	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!this.getClass().equals(obj.getClass()))
			return false;

		Nutzungskontext obj2 = (Nutzungskontext) obj;
		if ((this.id == obj2.getId()) && this.name.equals(obj2.getName()) && this.userType.equals(obj2.getUserType())
				&& this.workTask.equals(obj2.getWorkTask()) && this.workTool.equals(obj2.getWorkTool())
				&& this.orgEnviroment.equals(obj2.getOrgEnviroment()) && this.techEnviroment.equals(obj2.getTechEnviroment())
				&& this.physEnviroment.equals(obj2.getPhysEnviroment())) {
			return true;
		}
		return false;
	}

	public int hashCode() {
		int tmp = 0;
		tmp = (id + name).hashCode();
		return tmp;
	}
}
