package com.hallosme.pojos;

import java.io.Serializable;

public class WorkTool implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2530593250017787251L;
	private int id;
	private String productName;
	private String productScoupe1;
	private String productScoupe2;
	private String productScoupe3;
	private String importantFunctions = "";
	private int hardwareScreenSize;
	private boolean touchscreenNeed;
	private boolean mouseNeed;
	private boolean keyboardNeed;
	private String softwareCategory;
	private String extServiceNeed;
	private String extServiceCategorys = "";
	private String misc = "";

	public WorkTool() {
		super();
		// TODO Auto-generated constructor stub
	}

	public WorkTool(String productName, String productScoupe1, String productScoupe2, String productScoupe3, String importantFunctions,
			int hardwareScreenSize, boolean touchscreenNeed, boolean mouseNeed, boolean keyboardNeed, String softwareCategory, String extServiceNeed,
			String extServiceCategorys, String misc) {
		super();
		this.productName = productName;
		this.productScoupe1 = productScoupe1;
		this.productScoupe2 = productScoupe2;
		this.productScoupe3 = productScoupe3;
		this.importantFunctions = importantFunctions;
		this.hardwareScreenSize = hardwareScreenSize;
		this.touchscreenNeed = touchscreenNeed;
		this.mouseNeed = mouseNeed;
		this.keyboardNeed = keyboardNeed;
		this.softwareCategory = softwareCategory;
		this.extServiceNeed = extServiceNeed;
		this.extServiceCategorys = extServiceCategorys;
		this.misc = misc;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductScoupe1() {
		return productScoupe1;
	}

	public void setProductScoupe1(String productScoupe1) {
		this.productScoupe1 = productScoupe1;
	}

	public String getProductScoupe2() {
		return productScoupe2;
	}

	public void setProductScoupe2(String productScoupe2) {
		this.productScoupe2 = productScoupe2;
	}

	public String getProductScoupe3() {
		return productScoupe3;
	}

	public void setProductScoupe3(String productScoupe3) {
		this.productScoupe3 = productScoupe3;
	}

	public String getImportantFunctions() {
		return importantFunctions;
	}

	public void setImportantFunctions(String importantFunctions) {
		this.importantFunctions = importantFunctions;
	}

	public int getHardwareScreenSize() {
		return hardwareScreenSize;
	}

	public void setHardwareScreenSize(int hardwareScreenSize) {
		this.hardwareScreenSize = hardwareScreenSize;
	}

	public boolean isTouchscreenNeed() {
		return touchscreenNeed;
	}

	public void setTouchscreenNeed(boolean touchscreenNeed) {
		this.touchscreenNeed = touchscreenNeed;
	}

	public boolean isMouseNeed() {
		return mouseNeed;
	}

	public void setMouseNeed(boolean mouseNeed) {
		this.mouseNeed = mouseNeed;
	}

	public boolean isKeyboardNeed() {
		return keyboardNeed;
	}

	public void setKeyboardNeed(boolean keyboardNeed) {
		this.keyboardNeed = keyboardNeed;
	}

	public String getSoftwareCategory() {
		return softwareCategory;
	}

	public void setSoftwareCategory(String softwareCategory) {
		this.softwareCategory = softwareCategory;
	}

	public String getExtServiceNeed() {
		return extServiceNeed;
	}

	public void setExtServiceNeed(String extServiceNeed) {
		this.extServiceNeed = extServiceNeed;
	}

	public String getExtServiceCategorys() {
		return extServiceCategorys;
	}

	public void setExtServiceCategorys(String extServiceCategorys) {
		this.extServiceCategorys = extServiceCategorys;
	}

	public String getMisc() {
		return misc;
	}

	public void setMisc(String misc) {
		this.misc = misc;
	}

}
