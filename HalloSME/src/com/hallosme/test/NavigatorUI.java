package com.hallosme.test;

import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
@Theme("hallosme")
@PreserveOnRefresh
public class NavigatorUI extends UI {
	Navigator navigator;
	protected static final String MAINVIEW = "main";

	// @WebServlet(value = "/*", asyncSupported = true)
	// @VaadinServletConfiguration(productionMode = false, ui =
	// NavigatorUI.class)
	// public static class Servlet extends VaadinServlet {
	// }

	@Override
	protected void init(VaadinRequest request) {
		getPage().setTitle("Navigation Example");
		// Create a navigator to control the views
		navigator = new Navigator(this, this);
		// Create and register the views
		// navigator.addView("", new StartView());
		navigator.addView(MAINVIEW, new MainViewTest());
	}

}