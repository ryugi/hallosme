package com.hallosme.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import com.hallosme.utils.xml.ContextProperty;

public class SaxParsingTest {

	public static void main(String[] args) {
		File inputFile = new File("./test/Benutzer_ML.xml");

		Map<String, ContextProperty> contextPropertys = new HashMap<String, ContextProperty>();
		List<String> itemList = new ArrayList<String>();
		ContextProperty contextProperty = new ContextProperty();
		String context = "";
		String nameID = "";
		// boolean isProperty = false;
		boolean isNameID = false;
		boolean isQuestion = false;
		boolean isItems1 = false;
		boolean isItems2 = false;
		boolean isItem = false;

		try {
			XMLInputFactory factory = XMLInputFactory.newInstance();
			XMLEventReader eventReader = factory
					.createXMLEventReader(new FileReader(
							"./test/Benutzer_ML.xml"));

			while (eventReader.hasNext()) {
				XMLEvent event = eventReader.nextEvent();
				switch (event.getEventType()) {
				case XMLStreamConstants.START_ELEMENT:
					StartElement startElement = event.asStartElement();
					if (startElement.getName().toString().equals("context")) {
						Iterator<?> it = startElement.getAttributes();
						context = it.next().toString();
					}
					// if (startElement.getName().toString().equals("property"))
					// {
					// isProperty = true;
					// }
					if (startElement.getName().toString().equals("nameID")) {
						isNameID = true;
					}
					if (startElement.getName().toString().equals("question")) {
						isQuestion = true;
					}
					if (startElement.getName().toString().equals("items1")) {
						isItems1 = true;
					}
					if (startElement.getName().toString().equals("items2")) {
						isItems2 = true;
					}
					if (startElement.getName().toString().equals("item")) {
						isItem = true;
					}
					System.out.println("Start Element: " + startElement);
					break;
				case XMLStreamConstants.CHARACTERS:
					Characters characters = event.asCharacters();
					if (isNameID) {
						contextProperty.setNameID(characters.getData());
						nameID = characters.getData();
					}
					if (isQuestion) {
						contextProperty.setQuestion(characters.getData());
					}
					if (isItem) {
						itemList.add(characters.getData());
					}

					System.out.println("Characters: " + characters);
					break;
				case XMLStreamConstants.END_ELEMENT:
					EndElement endElement = event.asEndElement();
					if (endElement.getName().toString().equals("context")) {

					}
					if (endElement.getName().toString().equals("property")) {
						// isProperty = false;
						contextPropertys.put(nameID, contextProperty);
						contextProperty = new ContextProperty();
					}
					if (endElement.getName().toString().equals("nameID")) {
						isNameID = false;
					}
					if (endElement.getName().toString().equals("question")) {
						isQuestion = false;
					}
					if (endElement.getName().toString().equals("items1")) {
						isItems1 = false;
						contextProperty.setItems1(itemList);
						itemList = new ArrayList<String>();
					}
					if (endElement.getName().toString().equals("items2")) {
						isItems2 = false;
						contextProperty.setItems2(itemList);
						itemList = new ArrayList<String>();
					}
					if (endElement.getName().toString().equals("item")) {
						isItem = false;
					}

					System.out.println("End Element: " + endElement);
					break;
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
		System.out.println("Fertig!!!!");

	}

}
