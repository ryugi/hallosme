package com.hallosme.utils;

import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

public class CreateComponent {

	/**
	 * @param nameID
	 *            ContextProperty Name
	 * @param type
	 *            name der Variable die man vom dem ContextProperty erhalten m�chte
	 * @param caption
	 *            Caption f�r die ComboBox
	 * @return generierte ComboBox mit dem Daten aus dem ContextProperty
	 */
	public static ComboBox createComboBox(String nameID, String type, String caption) {
		ComboBox comboBox = new ComboBox();
		if (caption != null) {
			comboBox.setCaption(caption);
		}
		comboBox.setInputPrompt("Beschreiung des Feldes");
		String[] mlText = KontextUIText.getLabel(nameID, type).split(", ");

		for (int i = 0; i < mlText.length; i++) {
			comboBox.addItem(mlText[i]);
		}
		return comboBox;
	}

	/**
	 * @param nameID
	 *            ContextProperty Name
	 * @param type
	 *            name der Variable die man vom dem ContextProperty erhalten m�chte
	 * @return generiertes TextField mit dem Daten aus dem ContextProperty
	 */
	public static TextField createTextField(String nameID, String type) {
		TextField textField = new TextField();
		textField.setMaxLength(100);
		String[] mlText = KontextUIText.getLabel(nameID, type).split(", ");

		for (int i = 0; i < mlText.length; i++) {
			textField.setCaption(mlText[i]);
		}
		return textField;

	}

	/**
	 * @param nameID
	 *            ContextProperty Name
	 * @param type
	 *            name der Variable die man vom dem ContextProperty erhalten m�chte
	 * @return generierte TextArea mit dem Daten aus dem ContextProperty
	 */
	public static TextArea createTextArea(String nameID, String type) {
		TextArea textArea = new TextArea();
		textArea.setMaxLength(1000);
		String[] mlText = KontextUIText.getLabel(nameID, type).split(", ");

		for (int i = 0; i < mlText.length; i++) {
			textArea.setInputPrompt(mlText[i]);
		}
		return textArea;
	}

	/**
	 * @param nameID
	 *            ContextProperty Name
	 * @param type
	 *            name der Variable die man vom dem ContextProperty erhalten m�chte
	 * @param index
	 *            index des ben�tigten Teilstrings
	 * @return generierte TextArea mit dem Daten aus dem ContextProperty
	 */
	public static CheckBox createCheckBox(String nameID, String type, int index) {
		CheckBox checkBox = new CheckBox();
		String[] mlText = KontextUIText.getLabel(nameID, type).split(", ");
		checkBox.setCaption(mlText[index]);
		return checkBox;
	}

	/**
	 * @param caption
	 *            Caption f�r die ComboBox
	 * @return generierte Ja/Nein ComboBox
	 */
	public static ComboBox createComboBoxYN(String caption) {
		ComboBox comboBox = new ComboBox();
		comboBox.setCaption(caption);
		comboBox.setInputPrompt("");

		String[] mlText = KontextUIText.getLabel("yesNo", "items1").split(", ");

		for (int i = 0; i < mlText.length; i++) {
			comboBox.addItem(mlText[i]);
		}
		return comboBox;
	}

}
