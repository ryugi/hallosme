package com.hallosme.utils;

import java.io.Serializable;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.google.gwt.dev.util.collect.HashSet;
import com.hallosme.pojos.Nutzungskontext;
import com.hallosme.pojos.OrgEnviroment;
import com.hallosme.pojos.PhysEnviroment;
import com.hallosme.pojos.TechEnviroment;
import com.hallosme.pojos.UserAccount;
import com.hallosme.pojos.UserType;
import com.hallosme.pojos.WorkTask;
import com.hallosme.pojos.WorkTool;

public class SQLUtil {

	// public static Object save(Object object, SessionFactory factory) {
	// Session session = factory.openSession();
	// Integer id = null;
	// // welcher Instance geh�rt das Objekt an
	// if (object instanceof UserType) {
	// id = ((UserType) object).getId();
	// }
	//
	// if (id == null) {
	// id = commit(object, factory);
	// } else {
	// return update(factory, object, id);
	// }
	// return session.get(object.getClass(), id);
	// }

	/**
	 * Speichern des Objektes in der Datenbank. Dazu pr�fen ob das Objekt bereits eine ID beistzt,
	 * ist dies der Fall wird das Objekt geupdated, ansonten neu Angelegt
	 * 
	 * @param factory
	 *            SessionFactory
	 * @param object
	 *            Object das persistiert werden soll
	 * @return id des persistiertn Objektes
	 */
	public static Integer save(SessionFactory factory, Object object) {
		Integer id = null;
		// welcher Instance geh�rt das Objekt an
		if (object instanceof UserType) {
			id = ((UserType) object).getId();
		} else if (object instanceof WorkTask) {
			id = ((WorkTask) object).getId();
		} else if (object instanceof WorkTool) {
			id = ((WorkTool) object).getId();
		} else if (object instanceof OrgEnviroment) {
			id = ((OrgEnviroment) object).getId();
		} else if (object instanceof TechEnviroment) {
			id = ((TechEnviroment) object).getId();
		} else if (object instanceof PhysEnviroment) {
			id = ((PhysEnviroment) object).getId();
		} else if (object instanceof Nutzungskontext) {
			id = ((Nutzungskontext) object).getId();
		} else if (object instanceof UserAccount) {
			id = ((UserAccount) object).getId();
		}

		if (id == 0) {
			id = commit(factory, object);
		} else {
			update(factory, object, id);
		}
		return id;
	}

	/**
	 * L�sche Objekt aus der Datenbank mittels der
	 * 
	 * @param factory
	 *            SessionFactory
	 * @param clazz
	 *            Klasse des zu l�schenden Objektes
	 * @param id
	 *            id des zu l�schenden Objektes
	 * @return true wenn erflogreich gel�schten, ansonten false
	 */
	public static boolean deleteById(SessionFactory factory, Class<?> clazz, Serializable id) {
		Session session = factory.openSession();
		Transaction tx = null;
		Object persistentInstance = session.load(clazz, id);
		if (persistentInstance != null) {
			try {
				tx = session.beginTransaction();
				session.delete(persistentInstance);
				tx.commit();
				return true;
			} catch (HibernateException e) {
				if (tx != null)
					tx.rollback();
				e.printStackTrace();
			} finally {
				session.close();
			}
		}
		return false;
	}

	/**
	 * Neu Anlegen des �bergebenen Objekts
	 * 
	 * @param factory
	 *            SessionFactory
	 * @param object
	 *            zu persistierende Objekt
	 * @return id des persistierten Objekts
	 */
	public static Integer commit(SessionFactory factory, Object object) {
		Session session = factory.openSession();
		Transaction tx = null;
		Integer ID = null;
		try {
			tx = session.beginTransaction();
			ID = (Integer) session.save(object);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return ID;
	}

	/**
	 * Updaten des �bergebenen Objektes.
	 * 
	 * @param factory
	 *            SessionFactory
	 * @param object
	 *            zu updatendes Objekt
	 * @param id
	 *            id des Objekts
	 * @return geupdatetes Objekt
	 */
	public static Object update(SessionFactory factory, Object object, Integer id) {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			// object = session.get(object.getClass(), id);
			session.update(object);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return object;
	}

	// public static Integer createAccount(SessionFactory factory, UserAccount userAccount) {
	// Session session = factory.openSession();
	// Transaction tx = null;
	// Integer ID = null;
	// try {
	// tx = session.beginTransaction();
	// ID = (Integer) session.save(userAccount);
	// tx.commit();
	// } catch (HibernateException e) {
	// if (tx != null)
	// tx.rollback();
	// e.printStackTrace();
	// } finally {
	// session.close();
	// }
	// return ID;
	// }

	// /**
	// * @param id
	// * @return
	// */
	// public static Object load(Integer id) {
	// SessionFactory factory = new Configuration().configure().buildSessionFactory();
	// Session session = factory.openSession();
	// Object object = null;
	// object = session.get(object.getClass(), id);
	// session.close();
	// return object;
	// }

	/**
	 * @param factory
	 *            SessionFactory
	 * @param email
	 *            Email des UserAccounts
	 * @param password
	 *            Passwort des UserAccounts
	 * @return UserAccount mit der jeweiligen Email und wenn das Passwort dazu passt, anonsten null
	 */
	public static UserAccount login(SessionFactory factory, String email, String password) {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			// String sql = "select * from user_account where EMAIL=" + email;
			// SQLQuery query = session.createSQLQuery(sql);
			// query.addEntity(UserAccount.class);
			// List userAccounts = query.list();

			UserAccount userAccount = (UserAccount) session.createQuery("select ua from UserAccount ua where ua.email = :email")
					.setString("email", email).uniqueResult();
			if (userAccount != null && userAccount.getAccPassword().equals(password)) {
				userAccount = (UserAccount) session.get(UserAccount.class, userAccount.getId());
				return userAccount;
			}

			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

		return null;
	}

	/**
	 * Pr�fen ob die mitgegebene Email bereits benutzt wird
	 * 
	 * @param factory
	 *            SessionFactory
	 * @param email
	 *            Email
	 * @return true wenn Email bereits vergeben, sonst false
	 */
	public static boolean isEmailTaken(SessionFactory factory, String email) {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			UserAccount userAccount = (UserAccount) session.createQuery("select ua from UserAccount ua where ua.email = :email")
					.setString("email", email).uniqueResult();
			if (userAccount != null) {
				return true;
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return false;
	}

	// test persist
	public static Integer saveKontext() {
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		Session session = factory.openSession();
		Transaction tx = null;
		Integer ID = null;
		try {
			tx = session.beginTransaction();

			UserAccount userAccount = new UserAccount();
			userAccount.setAccPassword("123");
			userAccount.setCity("city");
			userAccount.setCompany("company");
			userAccount.setCountry("country");
			userAccount.setEmail("email");
			userAccount.setFirstName("firstName");
			userAccount.setLastName("lastName");
			userAccount.setSex("sex");
			userAccount.setStreet("street");
			userAccount.setZip(22225);

			// ######################################################################
			// KONTEXT 1
			// ######################################################################

			Set<Nutzungskontext> nutzungskontextSet = new HashSet<Nutzungskontext>();

			Nutzungskontext nutzungskontext = new Nutzungskontext();
			UserType userType = new UserType("userType", 1, 2, 3, "uebungsgrad", true, "skillMouse", true, 22, true, "skillTouchscreen",
					"quallificationField1", 33, "quallificationField2", 44, "quallificationField3", 55, "language1", "languageLevel1", "language2",
					"languageLevel2", "language3", "languageLevel3", "generalKnowledge", "age", "sex", "mentalSkill", "attitude");
			session.save(userType);
			nutzungskontext.setUserType(userType);

			WorkTask workTask = new WorkTask("taskCategory1", "taskCategory2", "taskCategory3", "workTaskName", 2, "taskFreqEntity", 1,
					"taskDurationEntity", "Ja", 2, 2, "Ja", 2, "physMentalReq", "taskDependency", "taskResult", "taskErrorImpact", "taskDanger");
			session.save(workTask);
			nutzungskontext.setWorkTask(workTask);

			WorkTool workTool = new WorkTool("productName", "productScoupe1", "productScoupe2", "productScoupe3", "importantFunctions", 2, true, true,
					true, "softwareCategory", "Ja", "extServiceCategorys", "misc");
			session.save(workTool);
			nutzungskontext.setWorkTool(workTool);

			OrgEnviroment orgEnviroment = new OrgEnviroment(44, "Ja", 33, "goalOrg", "Ja", "assistenceCategory1", "assistenceCategory2",
					"assistenceCategory3", 2, "manageStructure", "communicationStructure", "regulationComputer1", "regulationComputer2",
					"regulationComputer3", "orgGoals", "workMix1", "workMix2", "workMix3", "Ja", "performaceMessureCategory", "resultResponseCat",
					"resultResponsePeriode", "workSpeed", "Ja", "freedomChoice");
			session.save(orgEnviroment);
			nutzungskontext.setOrgEnviroment(orgEnviroment);

			TechEnviroment techEnviroment = new TechEnviroment("hardware1", "hardware2", "hardware3", "software1", "software2", "software3");
			session.save(techEnviroment);
			nutzungskontext.setTechEnviroment(techEnviroment);

			PhysEnviroment physEnviroment = new PhysEnviroment(5, 6, 6, "acousticCondition", 3, 4, 5, 6, "enviStability", 4, "workspaceInv", 4, 4, 4,
					4, 3, 2, "Ja", "healtHezardCategory", "Ja", "saftyLimitation");
			session.save(physEnviroment);
			nutzungskontext.setPhysEnviroment(physEnviroment);
			nutzungskontext.setName("Test Name");

			nutzungskontextSet.add(nutzungskontext);
			userAccount.setNutzungkontext(nutzungskontextSet);
			// ID = (Integer) session.save(nutzungskontext);
			ID = (Integer) session.save(userAccount);

			tx.commit();
			System.out.println("Done");
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return ID;
	}
}
