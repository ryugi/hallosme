package com.hallosme.utils.pdf;

import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import com.hallosme.pojos.Nutzungskontext;
import com.vaadin.server.VaadinService;

public class PDFGenerator {
	private final PDFont fontNormal = PDType1Font.HELVETICA;
	private final PDFont fontBold = PDType1Font.HELVETICA_BOLD;
	private PDDocument document;
	private PDPageContentStream contentStream;
	private PDPage page;
	private int height; // Gesamth�he der PDF Seite, in Dots
	private int width; // Einzug links der PDF Seite, in Dots
	private final int fontSizeNormal = 12; // Schriftgr��e des PDF
	private final int fontSizeHeadline = 16; // Schriftgr��e des PDF

	/**
	 * Default Constructor
	 */
	public PDFGenerator() {

	}

	/**
	 * Generiere ein PDF Dokument f�r den Nutzungskontext kontext
	 * 
	 * @param kontext
	 *            Nutzungskontext f�r den ein PDF Dokument Erstellt werden soll.
	 */
	public void generatePDF(Nutzungskontext kontext) {
		setHeight(700); // h�he des Dokuments
		setWidth(75); // Einzug links
		try {
			document = new PDDocument();
			page = new PDPage();
			document.addPage(page);
			contentStream = new PDPageContentStream(document, page);

			generateNutzungskontext(kontext);

			contentStream.close();
			document.save(VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/" + kontext.getName().replace(" ", "_") + ".pdf");
			document.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * F�gt dem PDF Document eine neue Zeile hinzu mit dem content als Inhalt und lineBreakHeight
	 * als zus�tlichen abstand zur n�chsten Zeile
	 * 
	 * @param content
	 *            Inhalt der neu hinzuzuf�genden Zeile
	 * @param lineBreakHeight
	 *            zus�tzlicher Abstand zur n�chsten Zeile
	 * @param font
	 *            verwendete Font
	 * @param fontSize
	 *            Fontgr��e
	 */
	public void addLine(String content, int lineBreakHeight, PDFont font, int fontSize) {

		try {

			if (height < 100) {
				contentStream.close();
				setPage(new PDPage()); // erstelle eine neue Seite
				document.addPage(page); // hinzuf�gen der neuen Seite
				contentStream = new PDPageContentStream(document, page);
				setHeight(700); // neue Seite somit zur�cksetzten der H�he
				;
			}

			// wenn die l�nge des anzuzeigenden content die Zeilenl�nge von 460
			// �berschreitet
			if (!lineLengthConfrom(content)) {
				String[] contendWords = content.split(" ");
				StringBuilder stringBulider = new StringBuilder();

				for (int i = 0; i < contendWords.length; i++) {
					if (!lineLengthConfrom(stringBulider.toString().trim() + contendWords[i])) {
						addLine(stringBulider.toString().trim(), 0, font, fontSize);
						stringBulider = new StringBuilder();
					}
					stringBulider.append(contendWords[i] + " ");
				}
				addLine(stringBulider.toString().trim(), 5, font, fontSize);
			} else {
				contentStream.beginText();
				contentStream.newLineAtOffset(width, height);
				contentStream.setFont(font, fontSize);
				contentStream.showText(content);
				contentStream.endText();

				height = height - 14 - lineBreakHeight;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Pr�fe ob der content String in eine Zeile des PDF Dokumentes passt
	 * 
	 * @param content
	 * @return true, wenn content in eine Zeile passt, sonst false
	 * @throws IOException
	 */
	public boolean lineLengthConfrom(String content) throws IOException {
		if ((fontNormal.getStringWidth(content.substring(0, content.length())) / 1000 * fontSizeNormal) < 460) {
			return true;
		}
		return false;
	}

	/**
	 * Generieren der einzelnen Zeilen des PDF Dokuments f�r den Nutzungskontext kontext
	 * 
	 * @param kontext
	 *            Nutzungskontext aus dem die Zeilen generiert werden
	 */
	public void generateNutzungskontext(Nutzungskontext kontext) {

		// addLine(""+ kontext.getUserType()., 5);

		// UserType
		addLine("Benutzer", 5, fontBold, fontSizeHeadline);
		addLine("Benutzertyp: " + kontext.getUserType().getUserType(), 5, fontNormal, fontSizeNormal);
		addLine("Erfahrung mit dem mobilen Assistenzsystem: " + kontext.getUserType().getExpMobAss(), 5, fontNormal, fontSizeNormal);
		addLine("Erfahrung mit der auszuf�hrenden Aufgabe: " + kontext.getUserType().getExpWorkTask(), 5, fontNormal, fontSizeNormal);
		addLine("Durchschnittliche Zeit bereits im Unternehmen:" + kontext.getUserType().getExpOrg(), 5, fontNormal, fontSizeNormal);
		addLine("Aufwand zur einarbeitung:     " + kontext.getUserType().getUebungsgrad(), 5, fontNormal, fontSizeNormal);
		addLine("Maus Verwendet:     " + kontext.getUserType().getMouseUsed() + ".   Fertigkeit: " + kontext.getUserType().getSkillMouse(), 5,
				fontNormal, fontSizeNormal);
		addLine("Tastatur Verwendet:     " + kontext.getUserType().getKeyboardUsed() + ".   Fertigkeit: " + kontext.getUserType().getSkillKeyboard()
				+ " Anschl�ge pro Minute", 5, fontNormal, fontSizeNormal);
		addLine("Touchscreen Verwendet:     " + kontext.getUserType().getTouchscreenUsed() + ".   Fertigkeit: "
				+ kontext.getUserType().getSkillTouchscreen(), 5, fontNormal, fontSizeNormal);
		addLine("Qualifikation 1:     " + kontext.getUserType().getQuallificationField1() + ".   Pozentualer Anteil: "
				+ kontext.getUserType().getQuallificationFieldStake1(), 0, fontNormal, fontSizeNormal);
		addLine("Qualifikation 2:     " + kontext.getUserType().getQuallificationField2() + ".   Pozentualer Anteil: "
				+ kontext.getUserType().getQuallificationFieldStake2(), 0, fontNormal, fontSizeNormal);
		addLine("Qualifikation 3:     " + kontext.getUserType().getQuallificationField3() + ".   Pozentualer Anteil: "
				+ kontext.getUserType().getQuallificationFieldStake3(), 5, fontNormal, fontSizeNormal);
		addLine("Sprache 1:     " + kontext.getUserType().getLanguage1() + ".   Niveau: " + kontext.getUserType().getLanguageLevel1(), 0, fontNormal,
				fontSizeNormal);
		addLine("Sprache 2:     " + kontext.getUserType().getLanguage2() + ".   Niveau: " + kontext.getUserType().getLanguageLevel2(), 0, fontNormal,
				fontSizeNormal);
		addLine("Sprache 3:     " + kontext.getUserType().getLanguage3() + ".   Niveau: " + kontext.getUserType().getLanguageLevel3(), 5, fontNormal,
				fontSizeNormal);
		addLine("Allgemeine Kenntnisse:     " + kontext.getUserType().getGeneralKnowledge(), 5, fontNormal, fontSizeNormal);
		addLine("Alter:     " + kontext.getUserType().getAge(), 5, fontNormal, fontSizeNormal);
		addLine("Geschlecht:     " + kontext.getUserType().getSex(), 5, fontNormal, fontSizeNormal);
		addLine("Intellektueller Fertigkeitslevel:     " + kontext.getUserType().getMentalSkill(), 5, fontNormal, fontSizeNormal);
		addLine("Haltung gegebn�ber Job/Aufgabe/Produkt/IT/ Vorgesetzen: " + kontext.getUserType().getAttitude(), 30, fontNormal, fontSizeNormal);

		// WorkTask
		addLine("Arbeitsaufgabe", 5, fontBold, fontSizeHeadline);
		addLine("Kategorien der Arbeitsaufgabe: " + kontext.getWorkTask().getTaskCategory1() + " ," + kontext.getWorkTask().getTaskCategory2() + " ,"
				+ kontext.getWorkTask().getTaskCategory3(), 5, fontNormal, fontSizeNormal);
		addLine("Bezeichnung: " + kontext.getWorkTask().getWorkTaskName(), 5, fontNormal, fontSizeNormal);
		addLine("H�ufigkeit: " + kontext.getWorkTask().getTaskFreqValue() + " pro " + kontext.getWorkTask().getTaskFreqEntity(), 5, fontNormal,
				fontSizeNormal);
		addLine("Aufgabendauer: " + kontext.getWorkTask().getTaskDuration() + " pro " + kontext.getWorkTask().getTaskDurationEntity(), 5, fontNormal,
				fontSizeNormal);
		addLine("Unterbrechungen vorhanden: " + kontext.getWorkTask().getIncident() + ". Unterbrechungsdauer : "
				+ kontext.getWorkTask().getIncidentDuration() + "(min). Unterbrechungsh�ufigkeit: " + kontext.getWorkTask().getIncidentFreq()
				+ "(pro Stunde).", 5, fontNormal, fontSizeNormal);
		addLine("Vorgegebener Ablauf: " + kontext.getWorkTask().getScoupe() + ". Anteil an gesamt Ablauf:  "
				+ kontext.getWorkTask().getProcessRatio(), 5, fontNormal, fontSizeNormal);
		addLine("Physische/Mentale Anforderungen: " + kontext.getWorkTask().getPhysMentalReq(), 5, fontNormal, fontSizeNormal);
		addLine("Resourcen Abh�nigkeit: " + kontext.getWorkTask().getTaskDependency(), 5, fontNormal, fontSizeNormal);
		addLine("Ergebnisse der Aufgabe: " + kontext.getWorkTask().getTaskResult(), 5, fontNormal, fontSizeNormal);
		addLine("Fehlereinfluss" + kontext.getWorkTask().getTaskErrorImpact(), 5, fontNormal, fontSizeNormal);
		addLine("Art des gef�hrlichen Einflusses: " + kontext.getWorkTask().getTaskDanger(), 30, fontNormal, fontSizeNormal);

		// WorkTool
		addLine("Arbeitsmittel", 5, fontBold, fontSizeHeadline);
		addLine("Arbeitsmittel: " + kontext.getWorkTool().getProductName(), 5, fontNormal, fontSizeNormal);
		addLine("Anwendungsbereiche: " + kontext.getWorkTool().getProductScoupe1() + ", " + kontext.getWorkTool().getProductScoupe2() + ", "
				+ kontext.getWorkTool().getProductScoupe3(), 5, fontNormal, fontSizeNormal);
		addLine("Wichtige Funktionen: " + kontext.getWorkTool().getImportantFunctions(), 5, fontNormal, fontSizeNormal);
		addLine("Bildschirmgr��e (Zoll): " + kontext.getWorkTool().getHardwareScreenSize(), 5, fontNormal, fontSizeNormal);
		addLine("Touchscreen ben�tigt: " + kontext.getWorkTool().isTouchscreenNeed(), 5, fontNormal, fontSizeNormal);
		addLine("Maus ben�tigt: " + kontext.getWorkTool().isMouseNeed(), 5, fontNormal, fontSizeNormal);
		addLine("Tastatur ben�tigt: " + kontext.getWorkTool().isKeyboardNeed(), 5, fontNormal, fontSizeNormal);
		addLine("Art der verwendeten Software" + kontext.getWorkTool().getSoftwareCategory(), 5, fontNormal, fontSizeNormal);
		addLine("Externe Dienstleistungen ben�tigt: " + kontext.getWorkTool().getExtServiceNeed() + ".  Dienstleistungen: "
				+ kontext.getWorkTool().getExtServiceCategorys(), 5, fontNormal, fontSizeNormal);
		addLine("Weiteres: " + kontext.getWorkTool().getMisc(), 30, fontNormal, fontSizeNormal);

		// OrgEnvironment
		addLine("Organisatorische Umgebung", 5, fontBold, fontSizeHeadline);
		addLine("Durschnittliche Arbeitszeit: " + kontext.getOrgEnviroment().getWorkHours(), 5, fontNormal, fontSizeNormal);
		addLine("Gruppenarbeit gegeben: " + kontext.getOrgEnviroment().getGroupwork() + ".  Durchschnittliche Gruppengr��e: "
				+ kontext.getOrgEnviroment().getGroupworkSize(), 5, fontNormal, fontSizeNormal);
		addLine("Ziel der Organisationsstruktur: " + kontext.getOrgEnviroment().getGoalOrg(), 5, fontNormal, fontSizeNormal);
		addLine("Hilfestellung gegeben: " + kontext.getOrgEnviroment().getAssistence() + ".  Arten der Hilfestellung: "
				+ kontext.getOrgEnviroment().getAssistenceCategory1() + ", " + kontext.getOrgEnviroment().getAssistenceCategory2() + ", "
				+ kontext.getOrgEnviroment().getAssistenceCategory3() + ", ", 5, fontNormal, fontSizeNormal);
		addLine("Anzahl der Unterbrechungen (pro Stunde): " + kontext.getOrgEnviroment().getBreaks(), 5, fontNormal, fontSizeNormal);
		addLine("F�hrungsstruktur: " + kontext.getOrgEnviroment().getManageStructure(), 5, fontNormal, fontSizeNormal);
		addLine("Kommunikationsstruktur: " + kontext.getOrgEnviroment().getCommunicationStructure(), 5, fontNormal, fontSizeNormal);
		addLine("Unternehmensvorschriften: " + kontext.getOrgEnviroment().getRegulationComputer1() + ", "
				+ kontext.getOrgEnviroment().getRegulationComputer2() + ", " + kontext.getOrgEnviroment().getRegulationComputer3() + ", ", 5,
				fontNormal, fontSizeNormal);
		addLine("Unternehmerische Ziele: " + kontext.getOrgEnviroment().getOrgGoals(), 5, fontNormal, fontSizeNormal);
		addLine("Arbeitsformen: " + kontext.getOrgEnviroment().getWorkMix1() + ", " + kontext.getOrgEnviroment().getWorkMix2() + ", "
				+ kontext.getOrgEnviroment().getWorkMix3() + ", ", 5, fontNormal, fontSizeNormal);
		addLine("Leistungsmessung gegeben: " + kontext.getOrgEnviroment().getPerformaceMessure() + ". Art: "
				+ kontext.getOrgEnviroment().getPerformaceMessureCategory(), 5, fontNormal, fontSizeNormal);
		addLine("R�ckmeldung zur Leistungsmessung: " + kontext.getOrgEnviroment().getResultResponseCat() + ". Periodizit�t: "
				+ kontext.getOrgEnviroment().getResultResponsePeriode(), 5, fontNormal, fontSizeNormal);
		addLine("Arbeitstempo" + kontext.getOrgEnviroment().getWorkSpeed(), 5, fontNormal, fontSizeNormal);
		addLine("Arbeitsablauf: " + kontext.getOrgEnviroment().getAutonomousWork(), 5, fontNormal, fontSizeNormal);
		addLine("Freiheit der Arbeitsgestaltung: " + kontext.getOrgEnviroment().getFreedomChoice(), 30, fontNormal, fontSizeNormal);

		// PhysEnvironment
		addLine("Physische Umgebung", 5, fontBold, fontSizeHeadline);
		addLine("Atmosph�rische Bedingungen: ", 0, fontNormal, fontSizeNormal);
		addLine("Luftqualit�t: " + kontext.getPhysEnviroment().getAtmosConditionAir() + ".    Windgeschwindigkeit: "
				+ kontext.getPhysEnviroment().getAtmosConditionSpeed() + ".    Luftfeuchtigkeit"
				+ kontext.getPhysEnviroment().getAtmosConditionHumidity(), 5, fontNormal, fontSizeNormal);
		addLine("Umgebungslauts�rke: " + kontext.getPhysEnviroment().getAcousticCondition(), 5, fontNormal, fontSizeNormal);
		addLine("Thermische Bedingungen: ", 0, fontNormal, fontSizeNormal);
		addLine("Unterste Themperatur: " + kontext.getPhysEnviroment().getThermalConditionLow() + ".    Oberste Themperatur: "
				+ kontext.getPhysEnviroment().getThermalConditionHigh(), 5, fontNormal, fontSizeNormal);
		addLine("Visuelle Bedingungen: ", 0, fontNormal, fontSizeNormal);
		addLine("Unterste Helligkeit: " + kontext.getPhysEnviroment().getLightConditionLow() + ".    Oberste Helligkeit: "
				+ kontext.getPhysEnviroment().getLightConditionHigh(), 5, fontNormal, fontSizeNormal);
		addLine("Arbeitsplatzstabilit�t: " + kontext.getPhysEnviroment().getEnviStability(), 5, fontNormal, fontSizeNormal);
		addLine("Arbeitsplatzgr��e (m�): " + kontext.getPhysEnviroment().getWorkspaceSize(), 0, fontNormal, fontSizeNormal);
		addLine("Arbeitsplatzeinrichtung: " + kontext.getPhysEnviroment().getWorkspaceInv(), 5, fontNormal, fontSizeNormal);
		addLine("K�rperhaltungsverteilung (Prozent): Stehend: " + kontext.getPhysEnviroment().getPostureStaying() + ", Sitzend: "
				+ kontext.getPhysEnviroment().getPostureSitting() + ", Liegend: " + kontext.getPhysEnviroment().getPostureLying() + ", Kniend: "
				+ kontext.getPhysEnviroment().getPostureKneeling(), 5, fontNormal, fontSizeNormal);
		addLine("Gr��e des Bewegungsraums (m�): " + kontext.getPhysEnviroment().getWorkspaceMoveSpace(), 0, fontNormal, fontSizeNormal);
		addLine("Ben�tigte Gegenst�nde in Reichweite (Prozent): " + kontext.getPhysEnviroment().getWorkspaceAtHand(), 5, fontNormal, fontSizeNormal);
		addLine("Gesundheitsgef�hrdungen gegeben: " + kontext.getPhysEnviroment().getHealtHezard() + ".  Art der Gef�hrdung: "
				+ kontext.getPhysEnviroment().getHealtHezardCategory(), 5, fontNormal, fontSizeNormal);
		addLine("Schutzvorrichtungen ben�tigt: " + kontext.getPhysEnviroment().getSafty() + ".  Art der Schutzvorrichtung: "
				+ kontext.getPhysEnviroment().getSaftyLimitation(), 30, fontNormal, fontSizeNormal);

		// TechEnvironment
		addLine("Technische Umgebung", 5, fontBold, fontSizeHeadline);
		addLine("Verwendete elektornische Ger�te: " + kontext.getTechEnviroment().getHardware1() + ", " + kontext.getTechEnviroment().getHardware2()
				+ ", " + kontext.getTechEnviroment().getHardware3() + ", ", 5, fontNormal, fontSizeNormal);
		addLine("Verwendete Arten von Software: " + kontext.getTechEnviroment().getSoftware1() + ", " + kontext.getTechEnviroment().getSoftware2()
				+ ", " + kontext.getTechEnviroment().getSoftware3() + ", ", 5, fontNormal, fontSizeNormal);

	}

	public void setDocument(PDDocument document) {
		this.document = document;
	}

	public PDDocument getDocument() {
		return document;
	}

	public PDPageContentStream getContentStream() {
		return contentStream;
	}

	public void setContentStream(PDPageContentStream contentStream) {
		this.contentStream = contentStream;
	}

	public PDPage getPage() {
		return page;
	}

	public void setPage(PDPage page) {
		this.page = page;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}
}
