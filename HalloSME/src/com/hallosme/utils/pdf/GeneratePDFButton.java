package com.hallosme.utils.pdf;

import com.hallosme.pojos.Nutzungskontext;
import com.vaadin.server.Page;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class GeneratePDFButton {

	/**
	 * Generiere einen neunen Button mittles aus dem mitgegebenen Nutzungskontext eine PDF generiert
	 * und zum download angeboten wird
	 * 
	 * @param nutzungskontext
	 * @return new PDF DownloadButton
	 */
	public Button generateDownloadPDFButton(Nutzungskontext nutzungskontext) {
		Button downloadButton = new Button("Generiere PDF");
		downloadButton.addStyleName("full-length");
		downloadButton.addStyleName("space");
		downloadButton.addClickListener(new ClickListener() {

			private static final long serialVersionUID = -9205627844906355155L;

			@Override
			public void buttonClick(ClickEvent event) {
				PDFGenerator pdfGenerator = new PDFGenerator();
				pdfGenerator.generatePDF(nutzungskontext);
				Page.getCurrent().getJavaScript().execute("document.getElementById('DownloadButtonId').click();");
			}
		});

		return downloadButton;

	}

}
