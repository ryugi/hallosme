package com.hallosme.utils;

import java.util.HashMap;
import java.util.Map;

import com.hallosme.utils.xml.ContextProperty;
import com.hallosme.utils.xml.SaxParsingTest;

public class KontextUIText {

	static Map<String, ContextProperty> contextPropertys = new HashMap<String, ContextProperty>();

	/**
	 * generiere alle ContextPropertys aus den xml-Dateien die im Ordner WEB_INF/MLText enthaltnen
	 * sind
	 */
	public static void loadMLTexts() {
		contextPropertys = SaxParsingTest.getMLTexts();
	}

	/**
	 * @param nameID
	 *            betreffendes contextProperty das diese nameID enth�lt (siehe ML-texte xml)
	 * @param type
	 *            entsprechende Stelle innerhalb des xml files
	 * @return alle Strings die innerhalb dieses xml tags enthalten sind
	 */
	public static String getLabel(String nameID, String type) {

		ContextProperty contextProperty = contextPropertys.get(nameID);
		switch (type) {
		case "question":
			return contextProperty.getQuestion();
		case "inputPrompt":
			return contextProperty.getInputPrompt();
		case "items1":
			return contextProperty.getItems1().toString().replace("[", "").replace("]", "");
		case "items2":
			return contextProperty.getItems2().toString().replace("[", "").replace("]", "");
		case "items3":
			return contextProperty.getItems3().toString().replace("[", "").replace("]", "");
		case "items4":
			return contextProperty.getItems4().toString().replace("[", "").replace("]", "");
		}
		// default, falls nichts gefunden wird.
		return "##NichtsGefunden: nameID=" + nameID + ", type=" + type;
	}
}
