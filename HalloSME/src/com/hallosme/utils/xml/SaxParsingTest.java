package com.hallosme.utils.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xml.sax.InputSource;

import com.vaadin.server.VaadinService;

public class SaxParsingTest {

	public static Map<String, ContextProperty> getMLTexts() {
		// File inputFile = new File("./test/MLTexts.xml");

		// Pfad der Applikation
		String basepath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();

		Map<String, ContextProperty> contextPropertys = new HashMap<String, ContextProperty>();
		List<String> itemList = new ArrayList<String>();
		ContextProperty contextProperty = new ContextProperty();
		String context = "";
		String nameID = "";
		// boolean isProperty = false;
		boolean isNameID = false;
		boolean isQuestion = false;
		boolean isInputPrompt = false;
		boolean isItems1 = false;
		boolean isItems2 = false;
		boolean isItems3 = false;
		boolean isItems4 = false;
		boolean isItem = false;

		try {

			File files = new File(basepath + "/WEB-INF/MLText");
			for (File file : files.listFiles()) {

				InputStream inputStream = new FileInputStream(file);
				Reader reader = new InputStreamReader(inputStream, "UTF-8");
				InputSource is = new InputSource(reader);
				XMLInputFactory factory = XMLInputFactory.newInstance();
				XMLEventReader eventReader = factory.createXMLEventReader(reader);

				// // Alternative wie das File gezogen werden kann
				// XMLEventReader eventReader = factory
				// .createXMLEventReader(new FileReader(VaadinService
				// .getCurrent().getBaseDirectory().getAbsolutePath()
				// + "/WEB-INF/MLText/Benutzer_ML.xml"));

				// get Path
				// VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();
				// new File("").getAbsoluteFile();
				// Paths.get("user.dir");

				while (eventReader.hasNext()) {
					XMLEvent event = eventReader.nextEvent();
					switch (event.getEventType()) {
					case XMLStreamConstants.START_ELEMENT:
						StartElement startElement = event.asStartElement();
						if (startElement.getName().toString().equals("context")) {
							Iterator<?> it = startElement.getAttributes();
							context = it.next().toString();
						}
						// if
						// (startElement.getName().toString().equals("property"))
						// {
						// isProperty = true;
						// }
						if (startElement.getName().toString().equals("nameID")) {
							isNameID = true;
						}
						if (startElement.getName().toString().equals("question")) {
							isQuestion = true;
						}
						if (startElement.getName().toString().equals("InputPrompt")) {
							isInputPrompt = true;
						}
						if (startElement.getName().toString().equals("items1")) {
							isItems1 = true;
						}
						if (startElement.getName().toString().equals("items2")) {
							isItems2 = true;
						}
						if (startElement.getName().toString().equals("items3")) {
							isItems3 = true;
						}
						if (startElement.getName().toString().equals("items4")) {
							isItems4 = true;
						}
						if (startElement.getName().toString().equals("item")) {
							isItem = true;
						}
						break;
					case XMLStreamConstants.CHARACTERS:
						Characters characters = event.asCharacters();
						if (isNameID) {
							contextProperty.setNameID(characters.getData());
							nameID = characters.getData();
						}
						if (isQuestion) {
							contextProperty.setQuestion(characters.getData());
						}
						if (isInputPrompt) {
							contextProperty.setInputPrompt(characters.getData());
						}
						if (isItem) {
							itemList.add(characters.getData());
						}

						break;
					case XMLStreamConstants.END_ELEMENT:
						EndElement endElement = event.asEndElement();
						if (endElement.getName().toString().equals("context")) {

						}
						if (endElement.getName().toString().equals("property")) {
							// isProperty = false;
							contextPropertys.put(nameID, contextProperty);
							contextProperty = new ContextProperty();
						}
						if (endElement.getName().toString().equals("nameID")) {
							isNameID = false;
						}
						if (endElement.getName().toString().equals("question")) {
							isQuestion = false;
						}
						if (endElement.getName().toString().equals("InputPrompt")) {
							isInputPrompt = false;
						}
						if (endElement.getName().toString().equals("items1")) {
							isItems1 = false;
							contextProperty.setItems1(itemList);
							itemList = new ArrayList<String>();
						}
						if (endElement.getName().toString().equals("items2")) {
							isItems2 = false;
							contextProperty.setItems2(itemList);
							itemList = new ArrayList<String>();
						}
						if (endElement.getName().toString().equals("items3")) {
							isItems3 = false;
							contextProperty.setItems3(itemList);
							itemList = new ArrayList<String>();
						}
						if (endElement.getName().toString().equals("items4")) {
							isItems4 = false;
							contextProperty.setItems4(itemList);
							itemList = new ArrayList<String>();
						}
						if (endElement.getName().toString().equals("item")) {
							isItem = false;
						}

						break;
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (XMLStreamException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		System.out.println("Laden der Texte fertig");
		return contextPropertys;

	}
}
