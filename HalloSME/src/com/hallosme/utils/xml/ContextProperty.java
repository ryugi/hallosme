package com.hallosme.utils.xml;

import java.util.ArrayList;
import java.util.List;

public class ContextProperty {

	private String nameID = null;
	private String question = null;
	private String inputPrompt = null;
	private List<String> items1 = new ArrayList<String>();
	private List<String> items2 = new ArrayList<String>();
	private List<String> items3 = new ArrayList<String>();
	private List<String> items4 = new ArrayList<String>();

	public String getNameID() {
		return nameID;
	}

	public void setNameID(String nameID) {
		this.nameID = nameID;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getInputPrompt() {
		return inputPrompt;
	}

	public void setInputPrompt(String inputPrompt) {
		this.inputPrompt = inputPrompt;
	}

	public List<String> getItems1() {
		return items1;
	}

	public void setItems1(List<String> items1) {
		this.items1 = items1;
	}

	public List<String> getItems2() {
		return items2;
	}

	public void setItems2(List<String> items2) {
		this.items2 = items2;
	}

	public List<String> getItems3() {
		return items3;
	}

	public void setItems3(List<String> items3) {
		this.items3 = items3;
	}

	public List<String> getItems4() {
		return items4;
	}

	public void setItems4(List<String> items4) {
		this.items4 = items4;
	}

}
