package com.hallosme.components.listener;

import com.hallosme.views.nutzungskontext.BasicKontextView;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;

/**
 * @author Waldemar Schiffner
 * 
 *         ValueChangeListener mittels Fortschrittsbalken den Fortschritt beim ausf�llen eines
 *         Kontextes verfolgen kann. Dazu werden den Feldern im Kontext ein BarChangeListener
 *         hinzugef�gt der bei �nderungen an der Komponente den Fortschrittsbalken entsprechend
 *         �ndert.
 * 
 *
 */
public class BarChangeListener implements ValueChangeListener {

	private static final long serialVersionUID = 6459347320845825061L;

	Component comp;
	ProgressBar bar;
	float compCount;
	float intervall;
	boolean defaultValue = true;
	int editedCompontens;
	Label editedCompontensLabel;
	BasicKontextView basicKontextView;

	public BarChangeListener(Component comp, int compCount, BasicKontextView basicKontextView) {
		intervall = calcInterval(compCount);
		this.comp = comp;
		this.bar = basicKontextView.getBar();
		this.compCount = compCount;
		this.basicKontextView = basicKontextView;
		this.editedCompontensLabel = basicKontextView.getEditedComponentsLabel();
	}

	@Override
	public void valueChange(ValueChangeEvent event) {
		if (event.getProperty().getClass().equals(ComboBox.class)) {
			if (((ComboBox) event.getProperty()).getValue() != null && !((ComboBox) event.getProperty()).getValue().equals("") && defaultValue) {
				bar.setValue(bar.getValue() + intervall);
				increaseEditedCompontens();
				defaultValue = false;
			} else if (((ComboBox) event.getProperty()).getValue() != null && ((ComboBox) event.getProperty()).getValue().equals("")) {
				if (UI.getCurrent().getSession().getAttribute("init") != null && (boolean) UI.getCurrent().getSession().getAttribute("init")) {
					return;
				} else {
					bar.setValue(bar.getValue() - intervall);
					decreaseEditedCompontens();
					defaultValue = true;
				}
			} else if (((ComboBox) event.getProperty()).getValue() == null) {
				if (UI.getCurrent().getSession().getAttribute("init") != null && (boolean) UI.getCurrent().getSession().getAttribute("init")) {
					return;
				} else {
					bar.setValue(bar.getValue() - intervall);
					decreaseEditedCompontens();
					defaultValue = true;
				}
			}
			changeBarStyle();
			System.out.println("Bar Value: " + bar.getValue() + " // Intervall: " + intervall);
			return;
		}
		if (event.getProperty().getClass().equals(TextField.class)) {
			// wert ge�ndert und vorher nicht auf default gewesen
			if (((TextField) event.getProperty()).getValue() != "" && !((TextField) event.getProperty()).getValue().equals("")
					&& !((TextField) event.getProperty()).getValue().equals("0") && !defaultValue) {
				return;
			}
			// wert ge�ndert und vorher default gewesen
			else if (!((TextField) event.getProperty()).getValue().equals("") && !((TextField) event.getProperty()).getValue().equals("0")
					&& defaultValue) {
				bar.setValue(bar.getValue() + intervall);
				increaseEditedCompontens();
				defaultValue = false;
			}
			// wert auf default ge�ndert
			else {
				if (UI.getCurrent().getSession().getAttribute("init") != null && (boolean) UI.getCurrent().getSession().getAttribute("init")) {
					return;
				} else {
					bar.setValue(bar.getValue() - intervall);
					decreaseEditedCompontens();
					defaultValue = true;
				}
			}
			changeBarStyle();
			System.out.println("Bar Value: " + bar.getValue() + " // Intervall: " + intervall);
			return;
		}
		if (event.getProperty().getClass().equals(TextArea.class)) {
			// wert ge�ndert und vorher nicht auf default gewesen
			if (((TextArea) event.getProperty()).getValue() != null && !((TextArea) event.getProperty()).getValue().equals("") && !defaultValue) {
				return;
			}
			// wert ge�ndert und vorher default gewesen
			else if (((TextArea) event.getProperty()).getValue() != null && !((TextArea) event.getProperty()).getValue().equals("") && defaultValue) {
				bar.setValue(bar.getValue() + intervall);
				increaseEditedCompontens();
				defaultValue = false;
			}
			// wert auf default ge�ndert
			else {
				if (UI.getCurrent().getSession().getAttribute("init") != null && (boolean) UI.getCurrent().getSession().getAttribute("init")) {
					return;
				} else {
					bar.setValue(bar.getValue() - intervall);
					decreaseEditedCompontens();
					defaultValue = true;
				}
			}
			changeBarStyle();
			System.out.println("Bar Value: " + bar.getValue() + " // Intervall: " + intervall);
			return;
		}
		if (event.getProperty().getClass().equals(CheckBox.class)) {
			if (((CheckBox) event.getProperty()).getValue()) {
				bar.setValue(bar.getValue() + intervall);
				increaseEditedCompontens();
			} else {
				if (UI.getCurrent().getSession().getAttribute("init") != null && (boolean) UI.getCurrent().getSession().getAttribute("init")) {
					return;
				} else {
					bar.setValue(bar.getValue() - intervall);
					decreaseEditedCompontens();
				}
			}
			changeBarStyle();
			System.out.println("Bar Value: " + bar.getValue() + " // Intervall: " + intervall);
			return;
		}
	}

	/**
	 * Erh�ht den Wert der Beschriftung des Fortschrittsbalken
	 */
	public void increaseEditedCompontens() {
		editedCompontens = basicKontextView.getEditedComponents() + 1;
		basicKontextView.setEditedComponents(editedCompontens);
		editedCompontensLabel.setValue(String.valueOf(editedCompontens) + " / " + (int) compCount);
	}

	/**
	 * verringert den Wert der Beschriftung des Fortschrittsbalken
	 */
	public void decreaseEditedCompontens() {
		editedCompontens = basicKontextView.getEditedComponents() - 1;
		basicKontextView.setEditedComponents(editedCompontens);
		editedCompontensLabel.setValue(String.valueOf(editedCompontens) + " / " + (int) compCount);
	}

	/**
	 * �ndert den Style des Fortschrittsbalken enprechend seines Values
	 */
	public void changeBarStyle() {
		if (bar.getValue() < 0.3) {
			bar.removeStyleName("orangeBar");
			bar.addStyleName("redBar");
		}
		if (bar.getValue() >= 0.3) {
			bar.removeStyleName("redBar");
			bar.removeStyleName("greenBar");
			bar.addStyleName("orangeBar");
		}
		if (bar.getValue() > 0.75) {
			bar.removeStyleName("orangeBar");
			bar.addStyleName("greenBar");
		}
	}

	/**
	 * Berechnet den Intervall in denen der Fortschrittsbalken erh�ht/verringert wird
	 * 
	 * @param count
	 *            Anzahl der gesamt Komponenten
	 * @return Intervall
	 */
	public float calcInterval(int count) {
		intervall = 1f / count;
		return intervall;

	}

}
