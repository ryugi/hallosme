package com.hallosme.components.listener;

import org.apache.commons.lang3.math.NumberUtils;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

public class VisibilityChangeListener implements ValueChangeListener {

	/**
	 * Setzen der Sichtbarkeit der Komonenten bei ValueChanges
	 */
	private static final long serialVersionUID = -7483682505339162323L;

	Component comp1;
	Component comp2;
	Component comp3;
	Component comp4;

	/**
	 * 
	 * 
	 * @param comp1
	 * @param comp2
	 * @param comp3
	 */
	public VisibilityChangeListener(Component comp1, Component comp2, Component comp3, Component comp4) {
		this.comp1 = comp1;
		this.comp2 = comp2;
		this.comp3 = comp3;
		this.comp4 = comp4;
	}

	@Override
	public void valueChange(ValueChangeEvent event) {
		if (comp1.getClass().equals(ComboBox.class)) {
			if (((ComboBox) comp1).getValue() != null && !((ComboBox) comp1).getValue().equals("") && !((ComboBox) comp1).getValue().equals("Nein")) {
				comp2.removeStyleName("hidden");
				comp2.setVisible(true);
			} else {
				comp2.addStyleName("hidden");
				comp2.setVisible(false);
				if (comp2.getClass().equals(ComboBox.class)) {
					// ((ComboBox) comp2).clear();
					((ComboBox) comp2).select(null);
					((ComboBox) comp2).getValue();
				} else if (comp2.getClass().equals(TextArea.class)) {
					((TextArea) comp2).clear();
				} else if (comp2.getClass().equals(TextField.class)) {
					if (!NumberUtils.isNumber(((TextField) comp2).getValue())) {
						((TextField) comp2).clear();
					}
				}
				if (comp3 != null) {
					comp3.addStyleName("hidden");
					comp3.setVisible(false);
					if (comp3.getClass().equals(ComboBox.class)) {
						// ((ComboBox) comp3).setValue("");
						// ((ComboBox) comp3).clear();
						((ComboBox) comp3).select(null);
					}
				}
				if (comp4 != null) {
					comp4.addStyleName("hidden");
					comp4.setVisible(false);
					if (comp4.getClass().equals(ComboBox.class)) {
						// ((ComboBox) comp4).setValue("");
						// ((ComboBox) comp4).clear();
						((ComboBox) comp4).select(null);
					}
				}
			}
		}
		if (comp1.getClass().equals(TextField.class)) {
			if (((TextField) comp1).getValue() != null && !((TextField) comp1).getValue().equals("0") && !((TextField) comp1).getValue().equals("")) {
				comp2.removeStyleName("hidden");
				comp2.setVisible(true);
			} else {
				if (comp2.getClass().equals(ComboBox.class)) {
					// ((ComboBox) comp2).clear();
					((ComboBox) comp2).setValue("");
				} else if (comp2.getClass().equals(TextArea.class)) {
					((TextArea) comp2).clear();
				} else if (comp2.getClass().equals(TextField.class)) {
					if (!NumberUtils.isNumber(((TextField) comp2).getValue())) {
						((TextField) comp2).clear();
					}
				}
			}
		}
		if (comp1.getClass().equals(TextArea.class)) {
			if (((TextArea) comp1).getValue() != null) {
			} else {
			}
		}
		if (comp1.getClass().equals(CheckBox.class)) {
			if (((CheckBox) comp1).getValue() != null) {
			} else {
			}
		}

	}
}
