package com.hallosme.components;

import com.hallosme.views.MainView;
import com.hallosme.views.navigation.SideNavigationService;
import com.vaadin.server.Page;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

/**
 * @author Waldemar Schiffner
 * 
 *         Fenster zur erstellung eines neunen Nutzungskontexts
 *
 */
public class CreateKontextWindow extends Window {

	private static final long serialVersionUID = -4485435419344180799L;

	public CreateKontextWindow() {
		super();
		addStyleName("createKontextWindow");
		VerticalLayout layout = new VerticalLayout();
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		TextField kontextName = new TextField("Kontext Name");
		kontextName.setInputPrompt("Kontext Name");
		Button createKontextButton = new Button("Erstellen");
		Button closeButton = new Button("Schlie�en");
		createKontextButton.addClickListener(new ClickListener() {
			private static final long serialVersionUID = -6552393380889286529L;

			@Override
			public void buttonClick(ClickEvent event) {
				if (kontextName.getValue().isEmpty()) {
					new Notification(null, "Gib bitte zuerst einen Namen f�r den neuen Kontext an.", Notification.TYPE_WARNING_MESSAGE, true)
							.show(Page.getCurrent());
				} else {
					((MainView) getUI()).getAccordion().setVisible(true);
					SideNavigationService.createNavigationTab(getSession(), kontextName.getValue().toString());
					close();
				}

			}
		});
		closeButton.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 6669305824418043081L;

			@Override
			public void buttonClick(ClickEvent event) {
				close();

			}
		});
		kontextName.setSizeFull();
		createKontextButton.setSizeFull();
		closeButton.setSizeFull();
		layout.addComponent(kontextName);
		layout.addComponent(horizontalLayout);
		horizontalLayout.addComponent(closeButton);
		horizontalLayout.addComponent(createKontextButton);

		horizontalLayout.addStyleName("float-right");

		// Eigentschaften des Fensters
		setCaption("Neuen Nutzungkontext erstellen");
		setPositionX(200);
		setPositionY(50);
		setModal(true);
		setClosable(false);
		setResizable(false);
		setContent(layout);
	}

}
