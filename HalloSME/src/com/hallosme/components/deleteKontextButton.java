package com.hallosme.components;

import java.io.Serializable;

import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.teemu.VaadinIcons;

import com.hallosme.pojos.Nutzungskontext;
import com.hallosme.pojos.UserAccount;
import com.hallosme.utils.SQLUtil;
import com.hallosme.views.MainView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;

public class deleteKontextButton extends Button {

	private static final long serialVersionUID = -5848549602742349994L;

	/**
	 * Entfernt den Nutzungskontext Tab "component" aus den Navigations Accordion
	 * 
	 * @param component
	 *            Nutzungskontext Tab
	 * @param name
	 *            Name des Nutzungskontexts
	 */
	public deleteKontextButton(Component component, String name) {
		super();
		setCaption("Kontext entfernen");
		addStyleName("full-length");
		addStyleName("space");
		setIcon(VaadinIcons.MINUS_CIRCLE_O);
		addClickListener(new ClickListener() {

			private static final long serialVersionUID = -540617131288517152L;

			@Override
			public void buttonClick(ClickEvent event) {
				ConfirmDialog.show(getUI(), "Achtung", "Wollen Sie den Nutzungskontext: " + name + " wirklich l�schen?", "Fortfahren", "Abbrechen",
						new ConfirmDialog.Listener() {

							private static final long serialVersionUID = 1969385392235822617L;

							public void onClose(ConfirmDialog dialog) {
								if (dialog.isConfirmed()) {
									((MainView) UI.getCurrent()).getAccordion().removeComponent(component);
									if (UI.getCurrent().getSession().getAttribute("user") != null) {
										UserAccount account = ((UserAccount) UI.getCurrent().getSession().getAttribute("user"));
										for (Nutzungskontext nutzungskontext : account.getNutzungkontext()) {
											if (nutzungskontext.getName().equals(name) && nutzungskontext.getId() != 0) {
												deleteKontext(nutzungskontext.getId());
											}
										}
									}
								} else {
									// do nothing
								}
							}
						});

			}
		});
	}

	/**
	 * Entferne den Nutzungskontext mit der id aus der Datenbank
	 * 
	 * @param id
	 *            id des Nutzungskontexts
	 */
	private void deleteKontext(Serializable id) {
		SQLUtil.deleteById(((MainView) UI.getCurrent()).getFactory(), Nutzungskontext.class, id);
	}

}
