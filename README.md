# README #

### Dies ist ein Projekt zum Erfassen eines Nutzungskontext in Hinblick auf mobile Assistenzsysteme ###


### Setup ###

Für den Setup der Entwicklungsumgebung sind folgende Punkte nötig.

Vorraussetzungen:

* eine MySQL Datenbank (das SQL Script für die Tabellen der Datenbank findet sich unter "HalloSMe / HalloSME / SQL / DB create skript V2.sql")
* Eclispe IDE
* das Eclipse Plugin Vaadin (über den Eclispe Marketplace zu erhalten)
* ein Git Plugin zum pullen des Projektes
* Apache Tomcat installation (Aufsetzten eines Servers innerhalb von Eclispe auf dem die Anwendung laufen wird)




Nach dem Pull des Projektes: 

* muss nur noch in der "HalloSMe / HalloSME / src / hibernate.cfg.xml" folgende anpassungen gemacht werden 
hibernate.connection.url = URL der Datenbank gegen die man sich verbinden möchte
hibernate.connection.username = der User mit dem amn sich verbindet
hibernate.connection.password = passwort des User mit dem man sich verbidnet

Danach muss die Applikation nur noch gestartet werden, der zuvor angelegte Tomcat server ausgewählt werden. 

Wurde alles korrekt durchgeführt, sollte die Anwendung mittels einem Browser über "http://localhost:8080/HalloSME/" aufrufbar sein